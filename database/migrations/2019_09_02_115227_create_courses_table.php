<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('category');
            $table->string('tag')->nullable();
            $table->string('method');
            $table->string('duration');
            $table->string('capacity');
            $table->string('quota');
            $table->string('level');
            $table->text('description');
            $table->string('schedule');
            $table->string('facility')->nullable();
            $table->string('terms')->nullable();
            $table->string('start_date');
            $table->decimal('price', 13, 2);
            $table->string('status');
            $table->integer('merchant_id');
            $table->string('thumbnail');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
