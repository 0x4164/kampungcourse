@extends('inc.layout')
@section('content')
<div id=content class=main-container>
    <div class="container site-content">
        <div class=container>
            <div class="vc_row wpb_row vc_row-fluid">
                <div class="wpb_column vc_column_container vc_col-sm-4">
                    <div class=vc_column-inner>
                        <div class=wpb_wrapper>
                            <div class="wpb_single_image wpb_content_element vc_align_left">
                                <figure class="wpb_wrapper vc_figure">
                                    <div class="vc_single_image-wrapper   vc_box_border_grey"><img width=1066 height=1600 src={{asset('assets/wp-content/uploads/tim.jpg')}} class="vc_single_image-img attachment-full" alt="Team KampungCourse"></div>
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container vc_col-sm-8">
                    <div class=vc_column-inner>
                        <div class=wpb_wrapper>
                            <h2 style="text-align: left" class="vc_custom_heading">Tentang Kami</h2>
                            <div class="wpb_text_column wpb_content_element ">
                                <div class=wpb_wrapper>
                                    <blockquote>
                                        <p>
                                            Platform untuk menghubungkan para pencari kursus dan penyedia kursus dengan sistem pembayaran yang Aman & Terpercaya melalui rekening perusahaan Kampung Course Indonesia.</p>
                                    </blockquote>
                                    <h4>Latar Belakang</h4>
                                    <p>Perusahaan Kampung Course Indonesia merupakan perusahaan legal yang resmi telah berdiri di Indonesia, lebih tepatnya di Kampung Inggris, Desa Tulungrejo, Pare, Kediri, Jawa Timur.</p>
                                    <h4>Tujuan Kampung Course</h4>
                                    <li>Membangun kepercayaan dalam sistem pembayaran antara peserta kursus dan penyedia kursus (lembaga kursus).</li>
                                    <li>Mempermudah para pemilik lembaga kursus untuk melakukan promosi secara online</li>
                                    <li>Mempermudah para pencari kursus untuk mendapatkan program & lembaga kursus sesuai dengan kebutuhannya.</li>
                                    <h4>Penghargaan</h4>
                                    <p>Kampung Course memiliki banyak prestasi dan penghargaan dari berbagai pihak swasta dan pemerintah Indonesia.</p>
                                    <p>Selain itu, Kampung Course telah di dukung oleh berbagai Perusahaan Swasta dan Pemerintah, serta komunitas yang berkaitan dengan bidang bisnis Kampung Course.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="vc_row wpb_row vc_row-fluid">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class=vc_column-inner>
                        <div class=wpb_wrapper>
                            <h2 style="text-align: left" class="vc_custom_heading vc_custom_1517755684938">Our Management Team</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="vc_row wpb_row vc_row-fluid">
                <div class="wpb_column vc_column_container vc_col-sm-4">
                    <div class="vc_column-inner vc_custom_1517755872596">
                        <div class=wpb_wrapper>
                            <div class="wpb_single_image wpb_content_element vc_align_center">
                                <figure class="wpb_wrapper vc_figure">
                                    <div class="vc_single_image-wrapper   vc_box_border_grey"><img class="vc_single_image-img " src={{asset('assets/wp-content/uploads/tim1.jpg')}} width=200 height=200 alt=Daniel-Liejardi title=Daniel-Liejardi></div>
                                </figure>
                            </div>
                            <div class="wpb_text_column wpb_content_element ">
                                <div class=wpb_wrapper>
                                    <h4 style="text-align: center;">Danang Pamungkas</h4>
                                </div>
                            </div>
                            <div class="wpb_text_column wpb_content_element  vc_custom_1517755785086">
                                <div class=wpb_wrapper>
                                    <p style="text-align: center;"><em>CEO &amp; Founder</em></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container vc_col-sm-4">
                    <div class="vc_column-inner vc_custom_1517755881505">
                        <div class=wpb_wrapper>
                            <div class="wpb_single_image wpb_content_element vc_align_center">
                                <figure class="wpb_wrapper vc_figure">
                                    <div class="vc_single_image-wrapper   vc_box_border_grey"><img class="vc_single_image-img " src={{asset('assets/wp-content/uploads/tim2.jpg')}} width=200 height=200 alt=Jourdan-Kamal title=Jourdan-Kamal></div>
                                </figure>
                            </div>
                            <div class="wpb_text_column wpb_content_element ">
                                <div class=wpb_wrapper>
                                    <h4 style="text-align: center;">Jimy Candra Gunawan</h4>
                                </div>
                            </div>
                            <div class="wpb_text_column wpb_content_element  vc_custom_1517755794072">
                                <div class=wpb_wrapper>
                                    <p style="text-align: center;"><em>COO &amp; Co-Founder</em></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container vc_col-sm-4">
                    <div class="vc_column-inner vc_custom_1517755888272">
                        <div class=wpb_wrapper>
                            <div class="wpb_single_image wpb_content_element vc_align_center">
                                <figure class="wpb_wrapper vc_figure">
                                    <div class="vc_single_image-wrapper   vc_box_border_grey"><img class="vc_single_image-img " src={{asset('assets/wp-content/uploads/tim3.jpg')}} width=200 height=200 alt="Dwina M Putri" title=Dwina-M-Putri></div>
                                </figure>
                            </div>
                            <div class="wpb_text_column wpb_content_element ">
                                <div class=wpb_wrapper>
                                    <h4 style="text-align: center;">M. Indre Wanof</h4>
                                </div>
                            </div>
                            <div class="wpb_text_column wpb_content_element  vc_custom_1517755802595">
                                <div class=wpb_wrapper>
                                    <p style="text-align: center;"><em>CMO &amp; Co-Founder</em></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection