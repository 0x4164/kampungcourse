@extends('inc.layout')
@section('content')


<div id="content" class="main-container">
    <div class="woocommerce">
        <div id="customer-account">
            <div class="container">
                <div class="customer-account-wrapper">
                    <nav class="woocommerce-MyAccount-navigation">
                        <ul>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--dashboard"><a href="#">Dashboard</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--orders"><a href="#">Omzet</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards"><a href="#">Data Peserta</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards is-active"><a href="#">Kelola Kursus</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--edit-account"><a href="#">Akun Lembaga</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--customer-logout"><a href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                              document.getElementById('logout-form').submit();">Logout</a></li>
                        </ul>
                    </nav>
                    <div class="woocommerce-MyAccount-content">
                            <div id=woocommerce-content role=main>
                                    <div id=directory-search class=main-search-wrapper>
                                        <div class=main-search>
                                            <label class="visible-sm visible-xs text-center">Advanced Search</label>
                                            <form id=workshop-filters method=get enctype=multipart/form-data>
                                                <input type=hidden name=s value>
                                                <div class=search-option>
                                                    <div class=form-group>
                                                        <button >Tambah Kursus</button></div>
                                                        
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    
                                    <section class="archive-box products">
                                        <div class="row workshops-container">
                                            <div class="col-md-3 col-sm-6">
                                                <div class="post course">
                                                    <div class=content-wrapper>
                                                        <a href=culinary/beverage/coffee/learn-the-art-of-expresso-brewing/index.html class=img-fit-wrapper>
                                                            <img width=279 height=250 src="data:image/gif;base64,R0lGODlhAQABAIAAAMLCwgAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" data-lazy-type=image data-src=https://cdn.maubelajarapa.com/wp-content/uploads/2018/10/31103821/pexels-photo-529922-950x850.jpeg class="lazy lazy-hidden img-lazy post-img wp-post-image"
                                                                alt="sensory coffee jakarta"><noscript><img
                        width=279 height=250 src=../../cdn.maubelajarapa.com/wp-content/uploads/2018/10/31103821/pexels-photo-529922-950x850.jpg class="post-img wp-post-image" alt="sensory coffee jakarta"></noscript> </a>
                                                        <div class=post-content>
                                                            <span class=course-category title=Coffee>Bahasa Inggris</span>
                                                            <a href=culinary/beverage/coffee/learn-the-art-of-expresso-brewing/index.html class=post-title>
                                                                <strong>Kursus:</strong> Learn The Art of language</a>
                                                            <div class=course-info>
                                                                <strong class=course-price>
                        <span
                        class="woocommerce-Price-amount amount"><span
                        class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;400.000</span>		</strong>
                                                                <strong class=time-exp>
                        14 hours left	</strong></div>
                                                        </div>
                                                        <div class=course-footer>
                                                            <div class=course-footer-heading>
                                                                <a href=../educator/halocoffee/index.html class=course-vendor>
                                                                    <img src=../../cdn.maubelajarapa.com/wp-content/uploads/2018/10/15151516/b37d5a1d-941b-eaec-3123-10b1b9c875ee_logo.jpg alt="Halo Coffee" class=vendor-img>
                                                                    <span>Halo English</span>
                                                                    <strong>Jakarta Utara</strong>
                                                                </a>
                                                                <div class=course-date>
                                                                    <span class=month>Aug</span>
                                                                    <strong class=date>27</strong></div>
                                                                <a href=# class=show-other-dates>View Date (1)</a></div>
                                                            <div class=course-other-dates>
                                                                <form class=cart method=post enctype=multipart/form-data>
                                                                    <ul class=date-list>
                                                                        <li>
                                                                            <label>
                        <input
                        type=radio name=schedule_id value=12085>27 Aug, 2019
                        <i
                        class=price>
                        <span
                        class="woocommerce-Price-amount amount"><span
                        class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;400.000</span>		</i>
                        </label></li>
                                                                    </ul>
                                                                    <div class=button-box>
                                                                        <button type=submit name=add-to-cart value=167308 class="single_add_to_cart_button btn btn-primary" disabled=disabled>Attend</button>
                                                                        <a href=# class="btn btn-default btn-cancel">Cancel</a></div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6">
                                                <div class="post course">
                                                    <div class=content-wrapper>
                                                        <a href=personal-development/writing-menulis/learn-how-to-be-a-professional-graphologist-the-fundamentals-deeper-graphologist/index.html class=img-fit-wrapper>
                                                            <img width=167 height=250 src="data:image/gif;base64,R0lGODlhAQABAIAAAMLCwgAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" data-lazy-type=image data-src=https://cdn.maubelajarapa.com/wp-content/uploads/2019/08/06104453/tyler-nix-plsS9Ve23AI-unsplash-e1565063108571.jpg
                                                                class="lazy lazy-hidden img-lazy post-img wp-post-image" alt><noscript><img
                        width=167 height=250 src=../../cdn.maubelajarapa.com/wp-content/uploads/2019/08/06104453/tyler-nix-plsS9Ve23AI-unsplash-e1565063108571.jpg class="post-img wp-post-image" alt></noscript> </a>
                                                        <div class=post-content>
                                                            <span class=course-category title="Writing (Menulis)">Bahasa Inggris</span>
                                                            <a href=personal-development/writing-menulis/learn-how-to-be-a-professional-graphologist-the-fundamentals-deeper-graphologist/index.html class=post-title>
                                                                <strong>Kursus:</strong> (2 Sessions) Learn How To Be A Professional Ambassador: The Fundamentals &amp; Deeper Ambassador </a>
                                                            <div class=course-info>
                                                                <strong class=course-price>
                        <span
                        class="woocommerce-Price-amount amount"><span
                        class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;5.000.000</span>		</strong>
                                                                <strong class=time-exp>
                        14 hours left	</strong></div>
                                                        </div>
                                                        <div class=course-footer>
                                                            <div class=course-footer-heading>
                                                                <a href=../educator/brightenvironmentlearningacademy/index.html class=course-vendor>
                                                                    <img src=../../cdn.maubelajarapa.com/wp-content/uploads/2019/07/30172125/c14af2a8-2b70-aa6b-31ee-16f478e5cbf9_logo5.jpg alt="Bright Environment Learning Academy" class=vendor-img>
                                                                    <span>Bright Environment Learning Academy</span>
                                                                    <strong>Jakarta Selatan</strong>
                                                                </a>
                                                                <div class=course-date>
                                                                    <span class=month>Aug</span>
                                                                    <strong class=date>27</strong></div>
                                                                <a href=# class=show-other-dates>View Date (1)</a></div>
                                                            <div class=course-other-dates>
                                                                <form class=cart method=post enctype=multipart/form-data>
                                                                    <ul class=date-list>
                                                                        <li>
                                                                            <label>
                        <input
                        type=radio name=schedule_id value=12219  disabled=disabled>27 Aug, 2019 (Closed)
                        <i
                        class=price>
                        <span
                        class="woocommerce-Price-amount amount"><span
                        class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;5.000.000</span>		</i>
                        </label></li>
                                                                    </ul>
                                                                    <div class=button-box>
                                                                        <button type=submit name=add-to-cart value=198045 class="single_add_to_cart_button btn btn-primary" disabled=disabled>Attend</button>
                                                                        <a href=# class="btn btn-default btn-cancel">Cancel</a></div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6">
                                                <div class="post course">
                                                    <div class=content-wrapper>
                                                        <a href=fashion/sewing-menjahit/learn-how-to-make-japanese-knot-bag-using-hand-sewing-technique/index.html class=img-fit-wrapper>
                                                            <img width=242 height=250 src="data:image/gif;base64,R0lGODlhAQABAIAAAMLCwgAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" data-lazy-type=image data-src=https://cdn.maubelajarapa.com/wp-content/uploads/2019/08/12123935/cropped-Slide17.jpg class="lazy lazy-hidden img-lazy post-img wp-post-image"
                                                                alt data-lazy-srcset="https://cdn.maubelajarapa.com/wp-content/uploads/2019/08/12123935/cropped-Slide17.jpg 871w, https://cdn.maubelajarapa.com/wp-content/uploads/2019/08/12123935/cropped-Slide17-436x450.jpg 436w"
                                                                data-lazy-sizes="(max-width: 242px) 100vw, 242px"><noscript><img
                        width=242 height=250 src=../../cdn.maubelajarapa.com/wp-content/uploads/2019/08/12123935/cropped-Slide17.jpg class="post-img wp-post-image" alt srcset="https://cdn.maubelajarapa.com/wp-content/uploads/2019/08/12123935/cropped-Slide17.jpg 871w, https://cdn.maubelajarapa.com/wp-content/uploads/2019/08/12123935/cropped-Slide17-436x450.jpg 436w" sizes="(max-width: 242px) 100vw, 242px"></noscript>                                            </a>
                                                        <div class=post-content>
                                                            <span class=course-category title="Sewing (Menjahit)">Bahasa Mandarin</span>
                                                            <a href=fashion/sewing-menjahit/learn-how-to-make-japanese-knot-bag-using-hand-sewing-technique/index.html class=post-title>
                                                                <strong>Kursus:</strong> Learn How To Make Essay Using Yin Yang Technique </a>
                                                            <div class=course-info>
                                                                <strong class=course-price>
                        <span
                        class="woocommerce-Price-amount amount"><span
                        class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;300.000</span>		</strong>
                                                                <strong class=time-exp>
                        14 hours left	</strong></div>
                                                        </div>
                                                        <div class=course-footer>
                                                            <div class=course-footer-heading>
                                                                <a href=../educator/byubyelisabethtita/index.html class=course-vendor>
                                                                    <img src=../../cdn.maubelajarapa.com/wp-content/uploads/2019/08/09080110/353c8576-1a4f-b64e-b160-36188c148aa9_logo.jpg alt="Byu by Elisabeth TITA" class=vendor-img>
                                                                    <span>Byu by Elisabeth TITA</span>
                                                                    <strong>Jakarta Selatan</strong>
                                                                </a>
                                                                <div class=course-date>
                                                                    <span class=month>Aug</span>
                                                                    <strong class=date>27</strong></div>
                                                                <a href=# class=show-other-dates>View Date (1)</a></div>
                                                            <div class=course-other-dates>
                                                                <form class=cart method=post enctype=multipart/form-data>
                                                                    <ul class=date-list>
                                                                        <li>
                                                                            <label>
                        <input
                        type=radio name=schedule_id value=12347  disabled=disabled>27 Aug, 2019 (Closed)
                        <i
                        class=price>
                        <span
                        class="woocommerce-Price-amount amount"><span
                        class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;300.000</span>		</i>
                        </label></li>
                                                                    </ul>
                                                                    <div class=button-box>
                                                                        <button type=submit name=add-to-cart value=198915 class="single_add_to_cart_button btn btn-primary" disabled=disabled>Attend</button>
                                                                        <a href=# class="btn btn-default btn-cancel">Cancel</a></div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6">
                                                <div class="post course">
                                                    <div class=content-wrapper>
                                                        <a href=business/startup/learn-tax-obligation-for-startup/index.html class=img-fit-wrapper>
                                                            <img width=362 height=250 src="data:image/gif;base64,R0lGODlhAQABAIAAAMLCwgAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" data-lazy-type=image data-src=https://cdn.maubelajarapa.com/wp-content/uploads/2019/08/08154254/kelly-sikkema-G_91H-3qZOA-unsplash-e1565253817199.jpg
                                                                class="lazy lazy-hidden img-lazy post-img wp-post-image" alt><noscript><img
                        width=362 height=250 src=../../cdn.maubelajarapa.com/wp-content/uploads/2019/08/08154254/kelly-sikkema-G_91H-3qZOA-unsplash-e1565253817199.jpg class="post-img wp-post-image" alt></noscript> </a>
                                                        <div class=post-content>
                                                            <span class=course-category title="Startup, Tax">Bahasa Arab</span>
                                                            <a href=business/startup/learn-tax-obligation-for-startup/index.html class=post-title>
                                                                <strong>Kursus:</strong> Belajar Arab Pegon </a>
                                                            <div class=course-info>
                                                                <strong class=course-price>
                        <span
                        class="woocommerce-Price-amount amount"><span
                        class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;850.000</span>		</strong>
                                                                <strong class=time-exp>
                        14 hours left	</strong></div>
                                                        </div>
                                                        <div class=course-footer>
                                                            <div class=course-footer-heading>
                                                                <a href=../educator/taxvisory/index.html class=course-vendor>
                                                                    <img src=../../cdn.maubelajarapa.com/wp-content/uploads/2019/08/07113803/29198305-4cc4-4043-be0c-0b3cf2fbc3ac_logo.jpg alt=TAXVISORY class=vendor-img>
                                                                    <span>TAXVISORY</span>
                                                                    <strong>Jakarta Barat</strong>
                                                                </a>
                                                                <div class=course-date>
                                                                    <span class=month>Aug</span>
                                                                    <strong class=date>27</strong></div>
                                                                <a href=# class=show-other-dates>View Date (1)</a></div>
                                                            <div class=course-other-dates>
                                                                <form class=cart method=post enctype=multipart/form-data>
                                                                    <ul class=date-list>
                                                                        <li>
                                                                            <label>
                        <input
                        type=radio name=schedule_id value=12326  disabled=disabled>27 Aug, 2019 (Closed)
                        <i
                        class=price>
                        <span
                        class="woocommerce-Price-amount amount"><span
                        class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;850.000</span>		</i>
                        </label></li>
                                                                    </ul>
                                                                    <div class=button-box>
                                                                        <button type=submit name=add-to-cart value=198422 class="single_add_to_cart_button btn btn-primary" disabled=disabled>Attend</button>
                                                                        <a href=# class="btn btn-default btn-cancel">Cancel</a></div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6">
                                                <div class="post course">
                                                    <div class=content-wrapper>
                                                        <a href=media/photography/learn-about-basic-technique-of-photography/index.html class=img-fit-wrapper>
                                                            <img width=242 height=250 src="data:image/gif;base64,R0lGODlhAQABAIAAAMLCwgAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" data-lazy-type=image data-src=https://cdn.maubelajarapa.com/wp-content/uploads/2019/03/18140803/basic-photography-DP.jpg class="lazy lazy-hidden img-lazy post-img wp-post-image"
                                                                alt><noscript><img
                        width=242 height=250 src=../../cdn.maubelajarapa.com/wp-content/uploads/2019/03/18140803/basic-photography-DP.jpg class="post-img wp-post-image" alt></noscript> </a>
                                                        <div class=post-content>
                                                            <span class=course-category title=Photography>Photography</span>
                                                            <a href=media/photography/learn-about-basic-technique-of-photography/index.html class=post-title>
                                                                <strong>Kursus:</strong> Learn About Basic Technique Of Photography </a>
                                                            <div class=course-info>
                                                                <strong class=course-price>
                        <span
                        class="woocommerce-Price-amount amount"><span
                        class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;300.000</span>		</strong>
                                                                <strong class=time-exp>
                        14 hours left	</strong></div>
                                                        </div>
                                                        <div class=course-footer>
                                                            <div class=course-footer-heading>
                                                                <a href=../educator/olifevisual/index.html class=course-vendor>
                                                                    <img src=../../cdn.maubelajarapa.com/wp-content/uploads/2019/07/31164141/cropped-new-olife-logo-300x300.jpg alt="Olife Visual" class=vendor-img>
                                                                    <span>Olife Visual</span>
                                                                    <strong>Jakarta Utara</strong>
                                                                </a>
                                                                <div class=star-rating><span style=width:100%>Rated <strong
                        class=rating>5.00</strong> out of 5</span></div>
                                                                <div class=course-date>
                                                                    <span class=month>Aug</span>
                                                                    <strong class=date>27</strong></div>
                                                                <a href=# class=show-other-dates>View All Dates (3)</a></div>
                                                            <div class=course-other-dates>
                                                                <form class=cart method=post enctype=multipart/form-data>
                                                                    <ul class=date-list>
                                                                        <li>
                                                                            <label>
                        <input
                        type=radio name=schedule_id value=11913  disabled=disabled>27 Aug, 2019 (Closed)
                        <i
                        class=price>
                        <span
                        class="woocommerce-Price-amount amount"><span
                        class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;300.000</span>		</i>
                        </label></li>
                                                                        <li>
                                                                            <label>
                        <input
                        type=radio name=schedule_id value=11918  disabled=disabled>28 Aug, 2019 (Closed)
                        <i
                        class=price>
                        <span
                        class="woocommerce-Price-amount amount"><span
                        class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;300.000</span>		</i>
                        </label></li>
                                                                        <li>
                                                                            <label>
                        <input
                        type=radio name=schedule_id value=11914  disabled=disabled>31 Aug, 2019 (Closed)
                        <i
                        class=price>
                        <span
                        class="woocommerce-Price-amount amount"><span
                        class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;300.000</span>		</i>
                        </label></li>
                                                                    </ul>
                                                                    <div class=button-box>
                                                                        <button type=submit name=add-to-cart value=183440 class="single_add_to_cart_button btn btn-primary" disabled=disabled>Attend</button>
                                                                        <a href=# class="btn btn-default btn-cancel">Cancel</a></div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6">
                                                <div class="post course">
                                                    <div class=content-wrapper>
                                                        <a href=fashion/learn-the-basic-of-leather-crafting/index.html class=img-fit-wrapper>
                                                            <img width=348 height=250 src="data:image/gif;base64,R0lGODlhAQABAIAAAMLCwgAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" data-lazy-type=image data-src=https://cdn.maubelajarapa.com/wp-content/uploads/2018/01/09125250/Leatherians-Beginner-Class1.jpg class="lazy lazy-hidden img-lazy post-img wp-post-image"
                                                                alt=Leatherians-Beginner-Class1 data-lazy-srcset="https://cdn.maubelajarapa.com/wp-content/uploads/2018/01/09125250/Leatherians-Beginner-Class1.jpg 950w, https://cdn.maubelajarapa.com/wp-content/uploads/2018/01/09125250/Leatherians-Beginner-Class1-450x323.jpg 450w, https://cdn.maubelajarapa.com/wp-content/uploads/2018/01/09125250/Leatherians-Beginner-Class1-768x551.jpg 768w"
                                                                data-lazy-sizes="(max-width: 348px) 100vw, 348px"><noscript><img
                        width=348 height=250 src=../../cdn.maubelajarapa.com/wp-content/uploads/2018/01/09125250/Leatherians-Beginner-Class1.jpg class="post-img wp-post-image" alt=Leatherians-Beginner-Class1 srcset="https://cdn.maubelajarapa.com/wp-content/uploads/2018/01/09125250/Leatherians-Beginner-Class1.jpg 950w, https://cdn.maubelajarapa.com/wp-content/uploads/2018/01/09125250/Leatherians-Beginner-Class1-450x323.jpg 450w, https://cdn.maubelajarapa.com/wp-content/uploads/2018/01/09125250/Leatherians-Beginner-Class1-768x551.jpg 768w" sizes="(max-width: 348px) 100vw, 348px"></noscript>                                            </a>
                                                        <div class=post-content>
                                                            <span class=course-category title="Fashion, Leathercraft">Fashion, Leathercraft</span>
                                                            <a href=fashion/learn-the-basic-of-leather-crafting/index.html class=post-title>
                                                                <strong>Kursus:</strong> (2 Sessions) Learn the Basic of Leather Crafting </a>
                                                            <div class=course-info>
                                                                <strong class=course-price>
                        <span
                        class="woocommerce-Price-amount amount"><span
                        class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;1.100.000</span>		</strong>
                                                                <strong class=time-exp>
                        14 hours left	</strong></div>
                                                        </div>
                                                        <div class=course-footer>
                                                            <div class=course-footer-heading>
                                                                <a href=../educator/leatherians/index.html class=course-vendor>
                                                                    <img src=../../cdn.maubelajarapa.com/wp-content/uploads/2018/02/09083944/1001-300x300.jpg alt=Leatherians class=vendor-img>
                                                                    <span>Leatherians</span>
                                                                    <strong>Jakarta Utara</strong>
                                                                </a>
                                                                <div class=course-date>
                                                                    <span class=month>Aug</span>
                                                                    <strong class=date>27</strong></div>
                                                                <a href=# class=show-other-dates>View All Dates (11)</a></div>
                                                            <div class=course-other-dates>
                                                                <form class=cart method=post enctype=multipart/form-data>
                                                                    <ul class=date-list>
                                                                        <li>
                                                                            <label>
                        <input
                        type=radio name=schedule_id value=12691  disabled=disabled>27 Aug, 2019 (Closed)
                        <i
                        class=price>
                        <span
                        class="woocommerce-Price-amount amount"><span
                        class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;1.100.000</span>		</i>
                        </label></li>
                                                                        <li>
                                                                            <label>
                        <input
                        type=radio name=schedule_id value=12692>29 Aug, 2019
                        <i
                        class=price>
                        <span
                        class="woocommerce-Price-amount amount"><span
                        class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;1.100.000</span>		</i>
                        </label></li>
                                                                        <li>
                                                                            <label>
                        <input
                        type=radio name=schedule_id value=12693>31 Aug, 2019
                        <i
                        class=price>
                        <span
                        class="woocommerce-Price-amount amount"><span
                        class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;1.100.000</span>		</i>
                        </label></li>
                                                                        <li>
                                                                            <label>
                        <input
                        type=radio name=schedule_id value=12756>3 Sep, 2019
                        <i
                        class=price>
                        <span
                        class="woocommerce-Price-amount amount"><span
                        class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;1.100.000</span>		</i>
                        </label></li>
                                                                        <li>
                                                                            <label>
                        <input
                        type=radio name=schedule_id value=12757>5 Sep, 2019
                        <i
                        class=price>
                        <span
                        class="woocommerce-Price-amount amount"><span
                        class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;1.100.000</span>		</i>
                        </label></li>
                                                                        <li>
                                                                            <label>
                        <input
                        type=radio name=schedule_id value=12758>17 Sep, 2019
                        <i
                        class=price>
                        <span
                        class="woocommerce-Price-amount amount"><span
                        class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;1.100.000</span>		</i>
                        </label></li>
                                                                        <li>
                                                                            <label>
                        <input
                        type=radio name=schedule_id value=12759>19 Sep, 2019
                        <i
                        class=price>
                        <span
                        class="woocommerce-Price-amount amount"><span
                        class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;1.100.000</span>		</i>
                        </label></li>
                                                                        <li>
                                                                            <label>
                        <input
                        type=radio name=schedule_id value=12760>21 Sep, 2019
                        <i
                        class=price>
                        <span
                        class="woocommerce-Price-amount amount"><span
                        class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;1.100.000</span>		</i>
                        </label></li>
                                                                        <li>
                                                                            <label>
                        <input
                        type=radio name=schedule_id value=12761>24 Sep, 2019
                        <i
                        class=price>
                        <span
                        class="woocommerce-Price-amount amount"><span
                        class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;1.100.000</span>		</i>
                        </label></li>
                                                                        <li>
                                                                            <label>
                        <input
                        type=radio name=schedule_id value=12762>26 Sep, 2019
                        <i
                        class=price>
                        <span
                        class="woocommerce-Price-amount amount"><span
                        class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;1.100.000</span>		</i>
                        </label></li>
                                                                        <li>
                                                                            <label>
                        <input
                        type=radio name=schedule_id value=12763>28 Sep, 2019
                        <i
                        class=price>
                        <span
                        class="woocommerce-Price-amount amount"><span
                        class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;1.100.000</span>		</i>
                        </label></li>
                                                                    </ul>
                                                                    <div class=button-box>
                                                                        <button type=submit name=add-to-cart value=89669 class="single_add_to_cart_button btn btn-primary" disabled=disabled>Attend</button>
                                                                        <a href=# class="btn btn-default btn-cancel">Cancel</a></div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6">
                                                <div class="post course">
                                                    <div class=content-wrapper>
                                                        <a href=fashion/intermediate-level-leather-crafting/index.html class=img-fit-wrapper>
                                                            <img width=253 height=250 src="data:image/gif;base64,R0lGODlhAQABAIAAAMLCwgAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" data-lazy-type=image data-src=https://cdn.maubelajarapa.com/wp-content/uploads/2018/01/09125211/Leather-Craft-Intermediate.jpg class="lazy lazy-hidden img-lazy post-img wp-post-image"
                                                                alt="Leather Craft Intermediate" data-lazy-srcset="https://cdn.maubelajarapa.com/wp-content/uploads/2018/01/09125211/Leather-Craft-Intermediate.jpg 750w, https://cdn.maubelajarapa.com/wp-content/uploads/2018/01/09125211/Leather-Craft-Intermediate-450x445.jpg 450w"
                                                                data-lazy-sizes="(max-width: 253px) 100vw, 253px"><noscript><img
                        width=253 height=250 src=../../cdn.maubelajarapa.com/wp-content/uploads/2018/01/09125211/Leather-Craft-Intermediate.jpg class="post-img wp-post-image" alt="Leather Craft Intermediate" srcset="https://cdn.maubelajarapa.com/wp-content/uploads/2018/01/09125211/Leather-Craft-Intermediate.jpg 750w, https://cdn.maubelajarapa.com/wp-content/uploads/2018/01/09125211/Leather-Craft-Intermediate-450x445.jpg 450w" sizes="(max-width: 253px) 100vw, 253px"></noscript>                                            </a>
                                                        <div class=post-content>
                                                            <span class=course-category title="Fashion, Leathercraft">Fashion, Leathercraft</span>
                                                            <a href=fashion/intermediate-level-leather-crafting/index.html class=post-title>
                                                                <strong>Kursus:</strong> (4 Sessions) (Intermediate Level Leather Crafting) Learn How To Make Your Own Bag </a>
                                                            <div class=course-info>
                                                                <strong class=course-price>
                        <span
                        class="woocommerce-Price-amount amount"><span
                        class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;3.000.000</span>		</strong>
                                                                <strong class=time-exp>
                        14 hours left	</strong></div>
                                                        </div>
                                                        <div class=course-footer>
                                                            <div class=course-footer-heading>
                                                                <a href=../educator/leatherians/index.html class=course-vendor>
                                                                    <img src=../../cdn.maubelajarapa.com/wp-content/uploads/2018/02/09083944/1001-300x300.jpg alt=Leatherians class=vendor-img>
                                                                    <span>Leatherians</span>
                                                                    <strong>Jakarta Utara</strong>
                                                                </a>
                                                                <div class=course-date>
                                                                    <span class=month>Aug</span>
                                                                    <strong class=date>27</strong></div>
                                                                <a href=# class=show-other-dates>View All Dates (11)</a></div>
                                                            <div class=course-other-dates>
                                                                <form class=cart method=post enctype=multipart/form-data>
                                                                    <ul class=date-list>
                                                                        <li>
                                                                            <label>
                        <input
                        type=radio name=schedule_id value=12697  disabled=disabled>27 Aug, 2019 (Closed)
                        <i
                        class=price>
                        <span
                        class="woocommerce-Price-amount amount"><span
                        class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;3.000.000</span>		</i>
                        </label></li>
                                                                        <li>
                                                                            <label>
                        <input
                        type=radio name=schedule_id value=12698>29 Aug, 2019
                        <i
                        class=price>
                        <span
                        class="woocommerce-Price-amount amount"><span
                        class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;3.000.000</span>		</i>
                        </label></li>
                                                                        <li>
                                                                            <label>
                        <input
                        type=radio name=schedule_id value=12699>31 Aug, 2019
                        <i
                        class=price>
                        <span
                        class="woocommerce-Price-amount amount"><span
                        class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;3.000.000</span>		</i>
                        </label></li>
                                                                        <li>
                                                                            <label>
                        <input
                        type=radio name=schedule_id value=12764>3 Sep, 2019
                        <i
                        class=price>
                        <span
                        class="woocommerce-Price-amount amount"><span
                        class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;3.000.000</span>		</i>
                        </label></li>
                                                                        <li>
                                                                            <label>
                        <input
                        type=radio name=schedule_id value=12765>5 Sep, 2019
                        <i
                        class=price>
                        <span
                        class="woocommerce-Price-amount amount"><span
                        class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;3.000.000</span>		</i>
                        </label></li>
                                                                        <li>
                                                                            <label>
                        <input
                        type=radio name=schedule_id value=12766>17 Sep, 2019
                        <i
                        class=price>
                        <span
                        class="woocommerce-Price-amount amount"><span
                        class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;3.000.000</span>		</i>
                        </label></li>
                                                                        <li>
                                                                            <label>
                        <input
                        type=radio name=schedule_id value=12767>19 Sep, 2019
                        <i
                        class=price>
                        <span
                        class="woocommerce-Price-amount amount"><span
                        class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;3.000.000</span>		</i>
                        </label></li>
                                                                        <li>
                                                                            <label>
                        <input
                        type=radio name=schedule_id value=12768>21 Sep, 2019
                        <i
                        class=price>
                        <span
                        class="woocommerce-Price-amount amount"><span
                        class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;3.000.000</span>		</i>
                        </label></li>
                                                                        <li>
                                                                            <label>
                        <input
                        type=radio name=schedule_id value=12769>24 Sep, 2019
                        <i
                        class=price>
                        <span
                        class="woocommerce-Price-amount amount"><span
                        class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;3.000.000</span>		</i>
                        </label></li>
                                                                        <li>
                                                                            <label>
                        <input
                        type=radio name=schedule_id value=12770>26 Sep, 2019
                        <i
                        class=price>
                        <span
                        class="woocommerce-Price-amount amount"><span
                        class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;3.000.000</span>		</i>
                        </label></li>
                                                                        <li>
                                                                            <label>
                        <input
                        type=radio name=schedule_id value=12771>28 Sep, 2019
                        <i
                        class=price>
                        <span
                        class="woocommerce-Price-amount amount"><span
                        class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;3.000.000</span>		</i>
                        </label></li>
                                                                    </ul>
                                                                    <div class=button-box>
                                                                        <button type=submit name=add-to-cart value=89683 class="single_add_to_cart_button btn btn-primary" disabled=disabled>Attend</button>
                                                                        <a href=# class="btn btn-default btn-cancel">Cancel</a></div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6">
                                                <div class="post course">
                                                    <div class=content-wrapper>
                                                        <a href=culinary/baking/learn-how-to-make-lapis-double-choco-free-demo-lapis-decoration-with-chef-vinsencia/index.html class=img-fit-wrapper>
                                                            <img width=250 height=250 src="data:image/gif;base64,R0lGODlhAQABAIAAAMLCwgAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" data-lazy-type=image data-src=https://cdn.maubelajarapa.com/wp-content/uploads/2019/08/20202724/WhatsApp-Image-2019-08-20-at-20.09.48.jpeg
                                                                class="lazy lazy-hidden img-lazy post-img wp-post-image" alt="kelas buat lapis choco" data-lazy-srcset="https://cdn.maubelajarapa.com/wp-content/uploads/2019/08/20202724/WhatsApp-Image-2019-08-20-at-20.09.48.jpeg 768w, https://cdn.maubelajarapa.com/wp-content/uploads/2019/08/20202724/WhatsApp-Image-2019-08-20-at-20.09.48-300x300.jpeg 300w, https://cdn.maubelajarapa.com/wp-content/uploads/2019/08/20202724/WhatsApp-Image-2019-08-20-at-20.09.48-450x450.jpeg 450w, https://cdn.maubelajarapa.com/wp-content/uploads/2019/08/20202724/WhatsApp-Image-2019-08-20-at-20.09.48-180x180.jpeg 180w, https://cdn.maubelajarapa.com/wp-content/uploads/2019/08/20202724/WhatsApp-Image-2019-08-20-at-20.09.48-600x600.jpeg 600w"
                                                                data-lazy-sizes="(max-width: 250px) 100vw, 250px"><noscript><img
                        width=250 height=250 src=../../cdn.maubelajarapa.com/wp-content/uploads/2019/08/20202724/WhatsApp-Image-2019-08-20-at-20.09.48.jpg class="post-img wp-post-image" alt="kelas buat lapis choco" srcset="https://cdn.maubelajarapa.com/wp-content/uploads/2019/08/20202724/WhatsApp-Image-2019-08-20-at-20.09.48.jpeg 768w, https://cdn.maubelajarapa.com/wp-content/uploads/2019/08/20202724/WhatsApp-Image-2019-08-20-at-20.09.48-300x300.jpeg 300w, https://cdn.maubelajarapa.com/wp-content/uploads/2019/08/20202724/WhatsApp-Image-2019-08-20-at-20.09.48-450x450.jpeg 450w, https://cdn.maubelajarapa.com/wp-content/uploads/2019/08/20202724/WhatsApp-Image-2019-08-20-at-20.09.48-180x180.jpeg 180w, https://cdn.maubelajarapa.com/wp-content/uploads/2019/08/20202724/WhatsApp-Image-2019-08-20-at-20.09.48-600x600.jpeg 600w" sizes="(max-width: 250px) 100vw, 250px"></noscript>                                            </a>
                                                        <div class=post-content>
                                                            <span class=course-category title="Baking, Cooking Demo">Baking, Cooking Demo</span>
                                                            <a href=culinary/baking/learn-how-to-make-lapis-double-choco-free-demo-lapis-decoration-with-chef-vinsencia/index.html class=post-title>
                                                                <strong>Kursus:</strong> Learn How to Make Lapis Double Choco (Free Demo Lapis Decoration) with Chef Vinsencia </a>
                                                            <div class=course-info>
                                                                <strong class=course-price>
                        <span
                        class="woocommerce-Price-amount amount"><span
                        class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;1.000.000</span>		</strong>
                                                                <strong class=time-exp>
                        14 hours left	</strong></div>
                                                        </div>
                                                        <div class=course-footer>
                                                            <div class=course-footer-heading>
                                                                <a href=../educator/electroluxtasteandcarecenter/index.html class=course-vendor>
                                                                    <img src=../../cdn.maubelajarapa.com/wp-content/uploads/2019/08/12173547/3e2d3960-a7b3-bd1d-c1a8-c56fa38cff09_logo.jpg alt="Electrolux Taste and Care Center" class=vendor-img>
                                                                    <span>Electrolux Taste and Care Center</span>
                                                                    <strong>Jakarta Pusat</strong>
                                                                </a>
                                                                <div class=course-date>
                                                                    <span class=month>Aug</span>
                                                                    <strong class=date>27</strong></div>
                                                                <a href=# class=show-other-dates>View Date (1)</a></div>
                                                            <div class=course-other-dates>
                                                                <form class=cart method=post enctype=multipart/form-data>
                                                                    <ul class=date-list>
                                                                        <li>
                                                                            <label>
                        <input
                        type=radio name=schedule_id value=12809  disabled=disabled>27 Aug, 2019 (Closed)
                        <i
                        class=price>
                        <span
                        class="woocommerce-Price-amount amount"><span
                        class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;1.000.000</span>		</i>
                        </label></li>
                                                                    </ul>
                                                                    <div class=button-box>
                                                                        <button type=submit name=add-to-cart value=200331 class="single_add_to_cart_button btn btn-primary" disabled=disabled>Attend</button>
                                                                        <a href=# class="btn btn-default btn-cancel">Cancel</a></div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                    <div class="load-more-button-wrapper text-center">
                                        <img src=../wp-content/mu-plugins/mba-project/assets/images/loading.gif class="load-posts-loading hide" alt=Loading>
                                        <p><a class="mba-load-more button btn btn-primary" data-target=.workshops-container>Load more...</a></p>
                                    </div>
                                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    @endsection