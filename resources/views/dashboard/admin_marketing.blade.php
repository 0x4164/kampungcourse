@extends('inc.layout') @section('content')


<div id="content" class="main-container">
    <div class="woocommerce">
        <div id="customer-account">
            <div class="container">
                <div class="customer-account-wrapper">
                    <nav class="woocommerce-MyAccount-navigation">
                        <ul>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--dashboard"><a href="{{ route('adminpage') }}">Dashboard</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards"><a href="{{ route('admin_verify') }}">Verifikasi</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--orders"><a href="{{ route('admin_omzet') }}">Analisis Omzet & Peserta</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards"><a href="{{ route('admin_promotion') }}">Kelola Promosi</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards is-active"><a href="{{ route('admin_marketing') }}">Marketing</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards"><a href="{{ route('admin_blog') }}">Blog</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--customer-logout"><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                              document.getElementById('logout-form').submit();">Logout</a></li>
                        </ul>
                    </nav>
                    <div class="woocommerce-MyAccount-content">
                        <label>Promosi Email : </label><br>
                        <form action="/sendMail" method="POST" >
                        <select name="recipient">
                            <option value="lembaga">Kirim Ke Email Lembaga</option>
                            <option value="peserta">Kirim Ke Email Peserta</option>
                        </select><br><br>
                        <textarea name="message" id="editor"></textarea>
                        <br><button type="submit" class="btn">Kirim Promosi</button>
                        @csrf
                    </form>
                    <hr>
                    <form action="/sendNotification" method="POST" >
                        <br>
                        <label>Kirim Notifikasi : </label><br>
                        <select name="to">
                            <option value="lembaga">Kirim Ke Notifikasi Lembaga</option>
                            <option value="peserta">Kirim Ke Notifikasi Peserta</option>
                            <option value="semua">Kirim Ke Semua</option>
                        </select><br><br>
                        <label>Pesan : </label><br>
                        <textarea name="pesan" style="width:80%"></textarea><br>
                        <label>Link : </label><br>
                        <input type="url" name="link" style="width:80%"><br>
                        <br><button type="submit" class="btn">Kirim Notifikasi</button>
                        @csrf
                    </form>
                    <br>
                            @include('inc.message')
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdn.ckeditor.com/ckeditor5/12.4.0/classic/ckeditor.js"></script>

<script>
    //CKEDITOR.replace( 'editor' );
    ClassicEditor
        .create( document.querySelector( '#editor' ) )
        .catch( error => {
            console.error( error );
        } );
    //CKEDITOR.replace( 'editor1' );
</script>

@endsection