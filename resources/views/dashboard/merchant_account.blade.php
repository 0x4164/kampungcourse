@extends('inc.layout')
@section('content')


<div id="content" class="main-container">
    <div class="woocommerce">
        <div id="customer-account">
            <div class="container">
                <div class="customer-account-wrapper">
                    <nav class="woocommerce-MyAccount-navigation">
                        <ul>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--dashboard"><a href="{{ route('merchantpage') }}">Dashboard</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--orders"><a href="{{ route('merchant_omzet') }}">Omzet</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards"><a href="{{ route('merchant_participant') }}">Data Peserta</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards"><a href="{{ route('merchant_course') }}">Kelola Kursus</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--edit-account is-active"><a href="{{ route('merchant_account') }}">Akun Lembaga</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--customer-logout"><a href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                              document.getElementById('logout-form').submit();">Logout</a></li>
                        </ul>
                    </nav>
                    <div class="woocommerce-MyAccount-content">
                            @include('inc.message')
                            <br><br>
                            <div class=business-profile-box>
                                    <div class="profile-cover upload-wrapper form-group" data-identifier=cover data-return=id>
                                        <div class="upload-loading hidden">Uploading</div>
                                        <div class=upload-fields>
                                            <label for=profile_cover class=upload-btn>
                                                    <span class=label-content><i class=icon-camera></i> Foto Cover</span>
                                                </label>
                                            <input type=hidden class=file_id name=cover value>
                                            <input type=file id=profile_cover class="upload-preview upload-input"></div>
                                    </div>
                                    <div class="profile-logo upload-wrapper form-group" data-identifier=logo data-return=id>
                                        <div class="upload-loading hidden">Uploading</div>
                                        <div class=upload-fields>
                                            <label for=profile_logo class=upload-btn>
                                                    <span class=label-content><i class=icon-camera></i>
                                                        Logo/Foto</span>
                                                </label>
                                            <input type=hidden class="file_id validate" id=logo name=logo value>
                                            <input type=file id=profile_logo class="upload-preview upload-input"></div>
                                    </div>
                                </div>
                                <div class=row>
                                    <div class="col-md-8 col-md-push-2">
                                        <div class=form-group>
                                            <label>Nama Lembaga <small class=text-italic></small></label>
                                            <input type=text class=form-control name=shopname id=shopname></div>
                                        <div class=form-group>
                                            <label>Nomor Telepon <small class=text-italic></small></label>
                                            <input type=text class=form-control placeholder data-type=phone name=admin_phone id=admin_phone></div>
                                        <div class=form-group>
                                            <label>Tipe Lembaga</label>
                                            <select class="selectpicker form-control" data-live-search=true name=institution_type id=area_coverage>
                                                            <option value>Pilih Tipe Lembaga</option>
                                                            <option data-tokens=Amplas value=162>Lembaga</option>
                                                            <option data-tokens=Ancol value=88>Tutor</option>
                                                        </select></div>
                                        <div class=form-group>
                                            <label>Kategori</label>
                                            <select class="selectpicker form-control" data-live-search=true name=tipe id=area_coverage>
                                                                <option value>Pilih Kategori</option>
                                                                <option data-tokens=Amplas value=162>Bahasa Inggris</option>
                                                                <option data-tokens=Ancol value=88>Bahasa Mandarin</option>
                                                                <option data-tokens=Arab value=88>Bahasa Arab</option>
                                                            </select></div>
                                        <div class=form-group>
                                            <label>Deskripsi Profil <small class=text-italic>(Minimal 50 Kata, Maksimal 150 Words)</small></label><textarea class=form-control cols=30 rows=5 name=description id=description></textarea></div>
                                        <div class=form-group>
                                            <label>Maps <small class=text-italic></small></label>
                                            <input type=text class=form-control name=shopname id=shopname></div>
                                        <div class=form-group>
                                            <label>Foto & Video Galeri <small class=text-italic></small></label>
                                            <input type=file class=form-control name=galery[] multiple id=shopname></div>
                                        <br><a class="btn btn-default" href="{{route('merchant.update')}}">Update Profile</a>
                                    </div>
                                </div>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    @endsection