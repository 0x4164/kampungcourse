@extends('inc.layout') @section('content')

<style>
    @import url(//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css);
.rating { 
  border: none;
  float: left;
  margin:0px 0px 0px 28px;
}

.rating > input { display: none; } 
.rating > label:before { 
  margin-top: 2px;
  padding:0px 5px 0px 5px;
  font-size: 1.25em;
  font-family: FontAwesome;
  display: inline-block;
  content: "\f005";
}

.rating > .half:before { 
  content: "\f089";
  position: absolute;
}

.rating > label { 
	color: #fff; 
	float: right;
	margin:4px 1px 0px 0px;
	background-color:#D8D8D8;
	border-radius:15px;
  height:25px;
}

/***** CSS Magic to Highlight Stars on Hover *****/

.rating:not(:checked) > label:hover, /* hover current star */
.rating:not(:checked) > label:hover ~ label { 
	background-color:#7ED321 !important;
  cursor:pointer;
} /* hover previous stars in list */

.rating > input:checked + label:hover, /* hover current star when changing rating */
.rating > label:hover ~ input:checked ~ label, /* lighten current selection */
.rating > input:checked ~ label:hover ~ label { 
	background-color:#7ED321 !important;
  cursor:pointer;
} 
</style>

<div id="content" class="main-container">
    <div class="woocommerce">
        <div id="customer-account">
            <div class="container">
                <div class="customer-account-wrapper">
                    <nav class="woocommerce-MyAccount-navigation">
                        <ul>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--dashboard"><a href="{{ route('learner-dashboard') }}">Dashboard</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--orders"><a href="{{ route('invoice') }}">Invoice</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards is-active"><a href="{{ route('history') }}">Riwayat</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--edit-account"><a href="{{ route('edit.account') }}">Akun</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--customer-logout"><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                              document.getElementById('logout-form').submit();">Logout</a></li>
                        </ul>
                    </nav>
                    <div class="woocommerce-MyAccount-content">
                        <table class="woocommerce-orders-table woocommerce-MyAccount-orders shop_table shop_table_responsive my_account_orders account-orders-table">
                            <thead>
                                <tr>
                                    <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-number"><span class="nobr">Order</span></th>
                                    <th class="woocommerce-orders-table__header woocommerce-orders-table__header-workshop"><span class="nobr">Workshop</span></th>
                                    <th class="woocommerce-orders-table__header woocommerce-orders-table__header-start_date"><span class="nobr">Start Date</span></th>
                                    <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-date"><span class="nobr">Order Date</span></th>
                                    <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-status"><span class="nobr">Status</span></th>
                                    <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-total"><span class="nobr">Total</span></th>
                                    <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-actions"><span class="nobr">Actions</span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="woocommerce-orders-table__row woocommerce-orders-table__row--status-cancelled order">
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-number" data-title="Order">
                                        <a href="https://maubelajarapa.com/account/view-order/205628">
#205628	</a></td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-workshop" data-title="Workshop">
                                        Learn How To Measure &amp; Analyze Your Mobile App Success</td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-start_date" data-title="Start Date">
                                        September 18, 2019 19:00:00</td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-date" data-title="Order Date">
                                        <time datetime="2019-09-16T15:10:37+00:00">Sep 16, 2019</time></td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-status" data-title="Status">
                                        Cancelled</td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-total" data-title="Total">
                                        <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">Rp</span>&nbsp;1.500.000</span> for 1 item</td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-actions" data-title="Actions">
                                        <a onclick="document.getElementById('history').style.display = 'flex';" class="woocommerce-button button view">Lihat Detail</a><a onclick="document.getElementById('testimony').style.display = 'flex';" class="woocommerce-button button invoice">Testimoni</a></td>
                                </tr>
                                <tr class="woocommerce-orders-table__row woocommerce-orders-table__row--status-cancelled order">
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-number" data-title="Order">
                                        <a href="https://maubelajarapa.com/account/view-order/205627">
#205627	</a></td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-workshop" data-title="Workshop">
                                        Learn The Essentials Of Business Writing</td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-start_date" data-title="Start Date">
                                        September 18, 2019 08:30:00</td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-date" data-title="Order Date">
                                        <time datetime="2019-09-16T15:07:36+00:00">Sep 16, 2019</time></td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-status" data-title="Status">
                                        Cancelled</td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-total" data-title="Total">
                                        <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">Rp</span>&nbsp;2.500.000</span> for 1 item</td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-actions" data-title="Actions">
                                        <a onclick="document.getElementById('history').style.display = 'flex';" class="woocommerce-button button view">Lihat Detail</a><a onclick="document.getElementById('testimony').style.display = 'flex';" class="woocommerce-button button invoice">Testimoni</a></td>
                                </tr>
                                @if(count($invoices)>0)
                                    @foreach ($invoices as $invoice)
                                <tr class="woocommerce-orders-table__row woocommerce-orders-table__row--status-cancelled order">
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-number" data-title="Order">
                                        <a href="#">#{{$invoice->invoices[0]->id}}	</a></td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-workshop" data-title="Workshop">
                                            {{$invoice->title}}</td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-start_date" data-title="Start Date">
                                        {{$invoice->invoices[0]->start_date}}</td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-date" data-title="Order Date">
                                        <time datetime="2019-08-02T22:55:36+00:00">{{$invoice->invoices[0]->created_at}}</time></td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-status" data-title="Status">
                                        {{$invoice->invoices[0]->status}} </td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-total" data-title="Total">
                                        <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">Rp</span>&nbsp;{{$invoice->invoices[0]->total}}</span></td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-actions" data-title="Actions">
                                        <a onclick="document.getElementById('history{{$invoice->invoices[0]->id}}').style.display = 'flex';" class="woocommerce-button button view">Lihat Detail</a><a onclick="document.getElementById('testimony{{$invoice->invoices[0]->id}}').style.display = 'flex';" class="woocommerce-button button invoice">Testimoni</a></td>
                                </tr>
                                @endforeach
                                    @endif

                                    <tr class="woocommerce-orders-table__row woocommerce-orders-table__row--status-cancelled order">
                                        <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-number" data-title="Order">
                                            <a href="https://maubelajarapa.com/account/view-order/197861">
    #197861	</a></td>
                                        <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-workshop" data-title="Workshop">
                                            Transforming Your Family Business In The Digital Era</td>
                                        <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-start_date" data-title="Start Date">
                                            August 15, 2019 19:00:00</td>
                                        <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-date" data-title="Order Date">
                                            <time datetime="2019-08-02T22:55:36+00:00">Aug 2, 2019</time></td>
                                        <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-status" data-title="Status">
                                            Cancelled</td>
                                        <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-total" data-title="Total">
                                            <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">Rp</span>&nbsp;150.000</span> for 1 item</td>
                                        <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-actions" data-title="Actions">
                                            <a onclick="document.getElementById('history').style.display = 'flex';" class="woocommerce-button button view">Lihat Detail</a><a onclick="document.getElementById('testimony').style.display = 'flex';" class="woocommerce-button button invoice">Testimoni</a></td>
                                    </tr>
                            </tbody>
                        </table>
                        @include('inc.message')
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@if(count($invoices)>0)
                                    @foreach ($invoices as $invoice)
                                    
<div class="swal2-container swal2-center swal2-fade swal2-shown" id="testimony{{$invoice->invoices[0]->id}}" style="overflow-y: auto; display:none;">
    <div role="dialog" aria-modal="true" aria-labelledby="swal2-title" aria-describedby="swal2-content" class="swal2-popup swal2-modal swal2-show" tabindex="-1" aria-live="assertive" style="width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; display: flex;">
        <form id="testimonyForm{{$invoice->invoices[0]->id}}" method="post" action="submitTestimony">
            @csrf
            <input type="hidden" name="invoiceId" value="{{$invoice->invoices[0]->id}}">
        <ul class="swal2-progresssteps" style="display: none;"></ul>
        <div class="swal2-icon swal2-error" style="display: none;"><span class="swal2-x-mark"><span class="swal2-x-mark-line-left"></span><span class="swal2-x-mark-line-right"></span></span>
        </div>
        <div class="swal2-icon swal2-question" style="display: none;">?</div>
        <div class="swal2-icon swal2-warning" style="display: none;">!</div>
        <div class="swal2-icon swal2-info" style="display: none;">i</div>
        <div class="swal2-icon swal2-success" style="display: none;">
            <div class="swal2-success-circular-line-left" style="background: rgb(255, 255, 255) none repeat scroll 0% 0%;"></div><span class="swal2-success-line-tip"></span> <span class="swal2-success-line-long"></span>
            <div class="swal2-success-ring"></div>
            <div class="swal2-success-fix" style="background: rgb(255, 255, 255) none repeat scroll 0% 0%;"></div>
            <div class="swal2-success-circular-line-right" style="background: rgb(255, 255, 255) none repeat scroll 0% 0%;"></div>
        </div><img class="swal2-image" src="{{asset('assets/wp-content/themes/maubelajarapa/assets/images/KampungCourseLogo.png')}}" alt="" style="display: block;">
        <div class="swal2-contentwrapper">
            <h2 class="swal2-title" id="swal2-title"></h2>
            <div id="swal2-content" class="swal2-content" style="display: block;">Masukkan Testimoni Anda Dibawah, Kami akan menampilkannya pada testimoni kursus</div>
        </div>
        <h4>Rating</h4>
        <fieldset class="rating" style="width:60%;">
                <input type="radio" id="field{{$invoice->invoices[0]->id}}_star5" style="display:none" name="rating{{$invoice->invoices[0]->id}}" value="5" /><label class = "full" for="field{{$invoice->invoices[0]->id}}_star5"></label>
                
                <input type="radio" id="field{{$invoice->invoices[0]->id}}_star4" style="display:none" name="rating{{$invoice->invoices[0]->id}}" value="4" /><label class = "full" for="field{{$invoice->invoices[0]->id}}_star4"></label>
                
                <input type="radio" id="field{{$invoice->invoices[0]->id}}_star3" style="display:none" name="rating{{$invoice->invoices[0]->id}}" value="3" /><label class = "full" for="field{{$invoice->invoices[0]->id}}_star3"></label>
                
                <input type="radio" id="field{{$invoice->invoices[0]->id}}_star2" style="display:none" name="rating{{$invoice->invoices[0]->id}}" value="2" /><label class = "full" for="field{{$invoice->invoices[0]->id}}_star2"></label>
                
                <input type="radio" id="field{{$invoice->invoices[0]->id}}_star1" style="display:none" name="rating{{$invoice->invoices[0]->id}}" value="1" /><label class = "full" for="field{{$invoice->invoices[0]->id}}_star1"></label>
                
            </fieldset>
            <br><br>
            <h4>Testimoni</h4>
        <textarea name="testimony" class="swal2-input" rows="5" style="display: block; height:max-content;" placeholder="" type="text"></textarea>
        
        

        <input type="file" class="swal2-file" style="display: none;">
        <div class="swal2-range" style="display: none;"><output></output><input type="range"></div><select class="swal2-select" style="display: none;"></select>
        <div class="swal2-radio" style="display: none;"></div><label for="swal2-checkbox" class="swal2-checkbox" style="display: none;"><input type="checkbox"></label><textarea class="swal2-textarea" style="display: none;"></textarea>
        <div class="swal2-validationerror" id="swal2-validationerror" style="display: none;"></div>
        <div class="swal2-buttonswrapper" style="display: flex;"><a type="button" onclick="document.getElementById('testimonyForm{{$invoice->invoices[0]->id}}').submit();" class="swal2-confirm swal2-styled" aria-label="" style="background-color: rgb(48, 133, 214); border-left-color: rgb(48, 133, 214); border-right-color: rgb(48, 133, 214);">Submit</a>
            <button type="button" class="swal2-cancel swal2-styled" style="display: inline-block; background-color: rgb(170, 170, 170);" aria-label="" onclick="document.getElementById('testimony{{$invoice->invoices[0]->id}}').style.display = 'none';">Cancel</button>
        </div><button type="button" class="swal2-close" style="display: none;">×</button>
        </form></div>
</div>
@endforeach
                                    @endif
@if(count($invoices)>0)
                                    @foreach ($invoices as $invoice)
<div class="swal2-container swal2-center swal2-fade swal2-shown" id="history{{$invoice->invoices[0]->id}}" style="overflow-y: auto; display:none;">
    <div role="dialog" aria-modal="true" aria-labelledby="swal2-title" aria-describedby="swal2-content" class="swal2-popup swal2-modal swal2-show" tabindex="-1" aria-live="assertive" style="width: 1200px; height:80%; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; display: flex;">
        <ul class="swal2-progresssteps" style="display: none;"></ul>
        <div class="swal2-icon swal2-error" style="display: none;"><span class="swal2-x-mark"><span class="swal2-x-mark-line-left"></span><span class="swal2-x-mark-line-right"></span></span>
        </div>
        <div class="swal2-icon swal2-question" style="display: none;">?</div>
        <div class="swal2-icon swal2-warning" style="display: none;">!</div>
        <div class="swal2-icon swal2-info" style="display: none;">i</div>
        <div class="swal2-icon swal2-success" style="display: none;">
            <div class="swal2-success-circular-line-left" style="background: rgb(255, 255, 255) none repeat scroll 0% 0%;"></div><span class="swal2-success-line-tip"></span> <span class="swal2-success-line-long"></span>
            <div class="swal2-success-ring"></div>
            <div class="swal2-success-fix" style="background: rgb(255, 255, 255) none repeat scroll 0% 0%;"></div>
            <div class="swal2-success-circular-line-right" style="background: rgb(255, 255, 255) none repeat scroll 0% 0%;"></div>
        </div><img class="swal2-image" width="200px" src="{{asset('assets/wp-content/themes/maubelajarapa/assets/images/KampungCourseLogo.png')}}" alt="" style="display: block;">
        <div class="swal2-contentwrapper">
            <h2 class="swal2-title" id="swal2-title"></h2>
            <div id="swal2-content" class="swal2-content" style="display: block; text-align:center;">
                <table align="center">
                    <tr>
                        <th style="padding-bottom:2%">Order ID <div class="pull-right">&nbsp:</div></th>
                        <td align="left" style="padding-left:2%; padding-bottom:2%">{{$invoice->invoices[0]->id}}</td>
                    </tr>
                    <tr>
                        <th style="padding-bottom:2%">Kursus<div class="pull-right">&nbsp:</div></th>
                        <td align="left" style="padding-left:2%; padding-bottom:2%;">{{$invoice->title}}</td>
                    </tr>
                    <tr>
                        <th style="padding-bottom:2%">Kategori<div class="pull-right">&nbsp:</div></th>
                        <td align="left" style="padding-left:2%; padding-bottom:2%;">{{$invoice->category}}</td>
                    </tr>
                    <tr>
                        <th style="padding-bottom:2%">Metode<div class="pull-right">&nbsp:</div></th>
                        <td align="left" style="padding-left:2%; padding-bottom:2%;">{{$invoice->method}}</td>
                    </tr>
                    <tr>
                        <th style="padding-bottom:2%">Level<div class="pull-right">&nbsp:</div></th>
                        <td align="left" style="padding-left:2%; padding-bottom:2%;">{{$invoice->level}}</td>
                    </tr>
                    <tr>
                        <th style="padding-bottom:2%">Fasilitas<div class="pull-right">&nbsp:</div></th>
                        <td align="left" style="padding-left:2%; padding-bottom:2%;">{{$invoice->facility}}</td>
                    </tr>
                    <tr>
                        <th style="padding-bottom:2%">Tanggal Mulai <div class="pull-right">&nbsp:</div></th>
                        <td align="left" style="padding-left:2%; padding-bottom:2%;">{{$invoice->invoices[0]->start_date}}</td>
                    </tr>
                    <tr>
                        <th style="padding-bottom:2%">Tanggal Order <div class="pull-right">&nbsp:</div></th>
                        <td align="left" style="padding-left:2%; padding-bottom:2%;">{{$invoice->invoices[0]->created_at}}</td>
                    </tr>
                    <tr>
                        <th style="padding-bottom:2%">Status <div class="pull-right">&nbsp:</div></th>
                        <td align="left" style="padding-left:2%; padding-bottom:2%;">{{$invoice->invoices[0]->status}}</td>
                    </tr>
                    <tr>
                        <th style="padding-bottom:2%">Harga <div class="pull-right">&nbsp:</div></th>
                        <td align="left" style="padding-left:2%; padding-bottom:2%;">Rp {{$invoice->invoices[0]->total}}</td>
                    </tr>
                </table>
            </div>
        </div><input type="file" class="swal2-file" style="display: none;">
        <div class="swal2-range" style="display: none;"><output></output><input type="range"></div><select class="swal2-select" style="display: none;"></select>
        <div class="swal2-radio" style="display: none;"></div><label for="swal2-checkbox" class="swal2-checkbox" style="display: none;"><input type="checkbox"></label><textarea class="swal2-textarea" style="display: none;"></textarea>
        <div class="swal2-validationerror" id="swal2-validationerror" style="display: none;"></div>
        <div class="swal2-buttonswrapper" style="display: flex;"><button type="button" onclick="document.getElementById('history{{$invoice->invoices[0]->id}}').style.display = 'none';" class="swal2-confirm swal2-styled" aria-label="" style="background-color: rgb(48, 133, 214); border-left-color: rgb(48, 133, 214); border-right-color: rgb(48, 133, 214);">Close</button>
        </div><button type="button" class="swal2-close" style="display: none;">×</button></div>
</div>
@endforeach
                                    @endif
                                    <script>
                                            var $j = jQuery.noConflict();
                                            $j("label").click(function(){
    $j(this).parent().find("label").css({"background-color": "#D8D8D8"});
    $j(this).css({"background-color": "#7ED321"});
    $j(this).nextAll().css({"background-color": "#7ED321"});
  });
                                            </script>
@endsection

