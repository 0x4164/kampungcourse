@extends('inc.layout') @section('content')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" rel="stylesheet"/>
<div id="content" class="main-container">
    <div class="woocommerce">
        <div id="customer-account">
            <div class="container">
                <div class="customer-account-wrapper">
                    <nav class="woocommerce-MyAccount-navigation">
                        <ul>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--dashboard"><a href="{{ route('merchantpage') }}">Dashboard</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--orders"><a href="{{ route('merchant_omzet') }}">Omzet</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards"><a href="{{ route('merchant_participant') }}">Data Peserta</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards is-active"><a href="{{ route('merchant_course') }}">Kelola Kursus</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--edit-account"><a href="{{ route('merchant_account') }}">Akun Lembaga</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--customer-logout"><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                              document.getElementById('logout-form').submit();">Logout</a></li>
                        </ul>
                    </nav>
                    <div class="woocommerce-MyAccount-content">
                        <h4>Pilih jenis promosi untuk kursus {{$course->title}}</h4>
                        <a onclick="document.getElementById('slider').style.display='block'" class="btn btn-primary">Slider Utama</a><br>
                        <div id="slider" style="display:none">
                            <br>
                            <button type="button" onclick="document.getElementById('slider').style.display='none'" class="btn pull-right">x</button><br>
                            <form method="post" action="/promoteSlider" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label>Gambar Slider Utama : </label>
                                    <input type="hidden" name="courseId" value="{{$course->id}}">
                                    <input type="hidden" name="merchantId" value="{{auth('merchant')->id()}}">
                                    <input type="file" name="image">
                                </div>
                                <button type="submit">Submit</button>
                            </form>
                        </div>
                        <hr>
                        <a onclick="document.getElementById('flash').style.display='block'" class="btn btn-primary">Flash Sale</a><br>
                        <div id="flash" style="display:none">
                                <br>
                                <button type="button" onclick="document.getElementById('flash').style.display='none'" class="btn pull-right">x</button><br>
                                <form method="post" action="/promoteFlash">
                                    @csrf
                                    <div class="form-group">
                                        <label>Harga Flash Sale : </label>
                                        <input type="hidden" name="courseId" value="{{$course->id}}">
                                        <input type="hidden" name="merchantId" value="{{auth('merchant')->id()}}">
                                        <input type="number" name="price">
                                    </div>
                                    <button type="submit">Submit</button>
                                </form>
                            </div>
                        <hr>
                        <a onclick="document.getElementById('topForm').submit();" class="btn btn-primary">Top Kursus</a><br><hr>
                        <form method="post" id="topForm" style="display:none" action="/promoteTop">
                            @csrf
                                <input type="hidden" name="courseId" value="{{$course->id}}">
                                <input type="hidden" name="merchantId" value="{{auth('merchant')->id()}}">
                        </form>
                        <a onclick="document.getElementById('recommendedForm').submit();" class="btn btn-primary">Recommended</a><br><hr>
                        <form method="post" id="recommendedForm" style="display:none" action="/promoteRecommended">
                            @csrf
                                <input type="hidden" name="courseId" value="{{$course->id}}">
                                <input type="hidden" name="merchantId" value="{{auth('merchant')->id()}}">
                        </form>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection