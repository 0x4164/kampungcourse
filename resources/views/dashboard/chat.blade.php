@extends('inc.layout') @section('content')
<?php use \App\Http\Controllers\ChatController; ?>

<div id="content" class="main-container">
    <div class="woocommerce">
        <div id="customer-account">
            <div class="container">
                <div class="customer-account-wrapper">
                    <nav class="woocommerce-MyAccount-navigation">
                        <ul>
                            @if(auth()->check())
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--dashboard is-active"><a href="/account">Masuk Ke Dashboard</a></li>
                            @if(count($chatList)>0) 

                            @foreach ($chatList as $item)
                                <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--dashboard <?php if($item->status == 'unread'){echo 'is-active';}  ?>"><a href="/chatWith/{{$item->to}}"><?php $merchant = ChatController::getMerchantData($item->to); echo $merchant->name ?></a></li>
                            @endforeach

                            @endif
                            @endif

                            @if(auth('merchant')->check())
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--dashboard is-active"><a href="/merchant-dashboard">Masuk Ke Dashboard</a></li>
                            @if(count($chatList)>0) 

                            @foreach ($chatList as $item)
                                <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--dashboard <?php if($item->status2 == 'unread'){echo 'is-active';}  ?>"><a href="/chatFrom/{{$item->user_id}}"><?php $user = ChatController::getUserData($item->user_id); echo $user->name ?></a></li>
                            @endforeach

                            @endif
                            @endif
                            
                        </ul>
                    </nav>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection