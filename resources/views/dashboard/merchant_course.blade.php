@extends('inc.layout') @section('content')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" rel="stylesheet"/>
<div id="content" class="main-container">
    <div class="woocommerce">
        <div id="customer-account">
            <div class="container">
                <div class="customer-account-wrapper">
                    <nav class="woocommerce-MyAccount-navigation">
                        <ul>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--dashboard"><a href="{{ route('merchantpage') }}">Dashboard</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--orders"><a href="{{ route('merchant_omzet') }}">Omzet</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards"><a href="{{ route('merchant_participant') }}">Data Peserta</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards is-active"><a href="{{ route('merchant_course') }}">Kelola Kursus</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--edit-account"><a href="{{ route('merchant_account') }}">Akun Lembaga</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--customer-logout"><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                              document.getElementById('logout-form').submit();">Logout</a></li>
                        </ul>
                    </nav>
                    <div class="woocommerce-MyAccount-content">
                        @if(!count($courses)>0)
                        <h4>Belum Ada Kursus Yang Terdaftar</h4>
                        @endif
                        <br><button onclick="document.getElementById('regist').style.display='block'" class="btn">Tambah Kursus</button>
                        <br><br>
                        @include('inc.message')

                        {!! Form::open(['action' => 'CourseController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data', 'id' => 'regist', 'style' => 'display: none;']) !!}
                                <hr>
                                <button type="button" onclick="document.getElementById('regist').style.display='none'" class="btn pull-right">x</button><br>
                                <div class="form-group">
                                    {{Form::label('title', 'Judul Kursus :')}}
                                    {{Form::text('title', '', ['class' => 'form-control', 'placeholder' => 'Judul Kursus'])}}
                                </div>
                                <div class="form-group">
                                    {{Form::label('category', 'Kategori Kursus :')}}
                                    {{Form::select('category', ['Bahasa Inggris' => 'Bahasa Inggris', 'Bahasa Mandarin' => 'Bahasa Mandarin', 'Bahasa Arab' => 'Bahasa Arab'], [ 'class' => 'form-control'])}}
                                </div>
                                <div class="form-group">
                                    {{Form::label('tag', 'Tag Kursus :')}}
                                    {{Form::text('tag', '', ['class' => 'form-control', 'placeholder' => 'Tag Kursus'])}}
                                </div>
                                <div class="form-group">
                                    {{Form::label('metode', 'Metode Belajar :')}}
                                    {{Form::select('metode', ['Offline' => 'Offline', 'Online' => 'Online'], [ 'class' => 'form-control'])}}
                                </div>
                                <div class="form-group">
                                    {{Form::label('duration', 'Lama Belajar :')}}
                                    {{Form::select('duration', ['1 Minggu' => '1 Minggu', '2 Minggu' => '2 Minggu', '1 Tahun' => '1 Tahun'], [ 'class' => 'form-control'])}}
                                </div>
                                <div class="form-group">
                                    {{Form::label('total', 'Jumlah Peserta Perkelas :')}}
                                    {{Form::number('total', '', ['class' => 'form-control'])}}
                                </div>
                                <div class="form-group">
                                    {{Form::label('kuota', 'Kuota Tersisa :')}}
                                    {{Form::number('kuota', '', ['class' => 'form-control'])}}
                                </div>
                                <div class="form-group">
                                    {{Form::label('level', 'Level :')}}
                                    {{Form::select('level', ['Pemula' => 'Pemula', 'Sedang' => 'Sedang', 'Mahir' => 'Mahir'], [ 'class' => 'form-control'])}}
                                </div>
                                <div class="form-group">
                                    {{Form::label('penjelasan', 'Penjelasan Kursus :')}}
                                    {{Form::textarea('penjelasan', '', ['class' => 'form-control', 'placeholder' => 'Penjelasan Kursus', 'rows' => '5', 'cols' => '50'])}}
                                </div>
                                <div class="form-group">
                                    {{Form::label('jadwal', 'Jadwal & Materi Belajar :')}}
                                    {{Form::textarea('jadwal', '', ['class' => 'form-control', 'placeholder' => 'Jadwal & Materi Belajar', 'rows' => '5', 'cols' => '50'])}}
                                </div>
                                <div class="form-group">
                                    {{Form::label('fasilitas', 'Fasilitas :')}}
                                    {{Form::textarea('fasilitas', '', ['class' => 'form-control', 'placeholder' => 'Fasilitas', 'rows' => '5', 'cols' => '50'])}}
                                </div>
                                <div class="form-group">
                                    {{Form::label('persyaratan', 'Persyaratan :')}}
                                    {{Form::textarea('persyaratan', '', ['class' => 'form-control', 'placeholder' => 'Persyaratan', 'rows' => '5', 'cols' => '50'])}}
                                </div>
                                <div class="form-group">
                                    {{Form::label('start', 'Tanggal Mulai Belajar :')}}
                                    {!! Form::text('date', '', array('id' => 'datepicker')) !!}
                                    
                                </div>
                                <div class="form-group">
                                    {{Form::label('harga', 'Harga :')}}
                                    {{Form::number('harga', '', ['class' => 'form-control'])}}
                                </div>
                                <div class="form-group">
                                    {{Form::label('cover_image', 'Gambar Thumbnail :')}}
                                    {{Form::file('cover_image[]', ['multiple' => 'multiple'])}}
                                </div>
                                {{Form::submit('Submit', ['class'=>'btn btn-primary datepicker'])}}
                            {!! Form::close() !!}
                            <br>

                        @if(count($courses)>0) <hr><h4>Daftar Kursus : </h4> @foreach ($courses as $course)
                        

                    <div class="col-md-4 col-sm-6">
                        <div class="post course">
                            <div class=content-wrapper>
                                <a href="/course/{{$course->id}}" class=img-fit-wrapper>
                                    <?php $area = (array) json_decode($course->thumbnail, true);
                                        $image = 'noimage.png';
                                        foreach($area as $v)
                                        {
                                            $image = $v;
                                        } ?>
                                    <img width=279 height=250 src="<?php echo asset('/images') ?>/<?php echo $image?>" data-lazy-type=image data-src=https://cdn.maubelajarapa.com/wp-content/uploads/2018/10/31103821/pexels-photo-529922-950x850.jpeg class="lazy lazy-hidden img-lazy post-img wp-post-image"
                                        alt="sensory coffee jakarta"><noscript><img width=279 height=250 src="<?php echo asset('/images') ?>/<?php echo $image?>" class="post-img wp-post-image" alt="sensory coffee jakarta"></noscript>                                    </a>
                                <div class=post-content>
                                    <span class=course-category title=Coffee>{{$course->category}}</span>
                                    <a href="/course/{{$course->id}}" class=post-title>
                                        <strong>Kursus:</strong> {{$course->title}}</a>
                                    <div class=course-info>
                                        <strong class=course-price>
                                                <span class="woocommerce-Price-amount amount">
                                                    <span class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;{{$course->price}}</span>		</strong>
                                        <strong class=time-exp> {{$course->duration}}	</strong></div>
                                </div>
                                <div class=course-footer>
                                    <div class=course-footer-heading>
                                        <a class=course-vendor>
                                            <img src="<?php echo asset('/images') ?>/<?php echo $course->merchant->profileImage?>" alt="{{$course->merchant->name}}" class=vendor-img>
                                            <span>{{$course->merchant->name}}</span>
                                            <strong>{{$course->merchant->address}}</strong>
                                        </a>
                                        <div class=course-date>
                                            <span class=month>Aug</span>
                                            <strong class=date>27</strong></div>
                                        <a href=# class=show-other-dates>Action</a></div>
                                    <div class=course-other-dates>
                                            <ul class=date-list>
                                            <li><a href="{{route('showParticipant')}}">Tampilkan Peserta</a></li>
                                                <li><a href="/course/{{$course->id}}/edit">Edit</a></li>
                                                <li><a onclick="if(confirm('Apakah Anda Yakin Akan Menghapus Kursus Ini ?')) {
                                                    window.location.href = '/deleteCourse/{{$course->id}}'
                                                }">Hapus</a></li>
                                                <li><a href="/addPromotion/{{$course->id}}">Promosikan</a></li>
                                                <li><a href="/courseDate/{{$course->id}}">Atur Tanggal</a></li>
                                            </ul>
                                            <div class=button-box>
                                                <a href=# class="btn btn-default btn-cancel">Cancel</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach @endif



                        
                            
                        
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
  <script>
  var $j = jQuery.noConflict();
$j("#datepicker").datepicker({
    multidate: true
});
  </script>
@endsection