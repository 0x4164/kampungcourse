@extends('inc.layout')
@section('content')


<div id="content" class="main-container">
    <div class="woocommerce">
        <div id="customer-account">
            <div class="container">
                <div class="customer-account-wrapper">
                    <nav class="woocommerce-MyAccount-navigation">
                        <ul>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--dashboard"><a href="{{ route('learner-dashboard') }}">Dashboard</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--orders"><a href="{{ route('invoice') }}">Invoice</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards"><a href="{{ route('history') }}">Riwayat</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--edit-account  is-active"><a href="{{ route('edit.account') }}">Akun</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--customer-logout"><a href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                              document.getElementById('logout-form').submit();">Logout</a></li>
                        </ul>
                    </nav>
                    <div class="woocommerce-MyAccount-content">
                        @include('inc.message')
                            <br><br>
                            <div class=business-profile-box>
                                <form action="{{ route('user.update') }}" method="POST">
                                    @csrf
                                    <div class="col-md-10">
                                        <div class=form-group>
                                            <label>Nama <small class=text-italic></small></label>
                                            <input type=text class=form-control placeholder="Name" value="{{ Auth::user()->name }}" name="name"></div>
                                        <div class=form-group>
                                            <label>Nomor Telepon <small class=text-italic></small></label>
                                            <input type=text class=form-control placeholder="08123123123" value="{{ Auth::user()->phone }}" name="phone"></div>
                                        <div class=form-group>
                                            <label>Password <small class=text-italic></small></label>
                                            <input name="password" type=password class=form-control></div>
                                        <br><button class="btn btn-default" type="submit">Update Profile</button>
                                </div>
                            </form>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    @endsection