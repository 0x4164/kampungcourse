@extends('inc.layout') @section('content')


<div id="content" class="main-container">
    <div class="woocommerce">
        <div id="customer-account">
            <div class="container">
                <div class="customer-account-wrapper">
                    <nav class="woocommerce-MyAccount-navigation">
                        <ul>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--dashboard"><a href="{{ route('adminpage') }}">Dashboard</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards  is-active"><a href="{{ route('admin_verify') }}">Verifikasi</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--orders"><a href="{{ route('admin_omzet') }}">Analisis Omzet & Peserta</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards"><a href="{{ route('admin_promotion') }}">Kelola Promosi</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards"><a href="{{ route('admin_marketing') }}">Marketing</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards"><a href="{{ route('admin_blog') }}">Blog</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--customer-logout"><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                              document.getElementById('logout-form').submit();">Logout</a></li>
                        </ul>
                    </nav>
                    <div class="woocommerce-MyAccount-content">
                        <label>Menu Verifikasi : </label><br>
                        <select id="selection">
                                <option value="lembaga">Verifikasi Lembaga</option>
                                <option value="kursus">Verifikasi Kursus</option>
                                <option value="iklan">Verifikasi Iklan</option>
                                <option value="pendaftaran">Verifikasi Pendaftaran</option>
                            </select>
                        <button class="btn" onclick="$('.message').css('display','none'); var sel = document.getElementById('selection').selectedIndex; document.getElementById(sel).style.display='block';">Submit</button>
                        <br>
                        <br>
                        <div id="0" class="message" style="display:none">
                                @if(count($unverifiedMerchant)>0)
                                <table class="woocommerce-orders-table woocommerce-MyAccount-orders shop_table shop_table_responsive my_account_orders account-orders-table">
                                    <thead>
                                        <tr>
                                            <th class="woocommerce-orders-table__header woocommerce-orders-table__header-workshop"><span class="nobr">Nama Lembaga</span></th>
                                            <th class="woocommerce-orders-table__header woocommerce-orders-table__header-workshop"><span class="nobr">Kategori</span></th>
                                            <th class="woocommerce-orders-table__header woocommerce-orders-table__header-start_date"><span class="nobr">No Hp</span></th>
                                            <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-date"><span class="nobr">Kota</span></th>
                                            <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-status"><span class="nobr">Email</span></th>
                                            <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-actions"><span class="nobr">Action</span></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                            @foreach($unverifiedMerchant as $merchant)
                                            <tr class="woocommerce-orders-table__row woocommerce-orders-table__row--status-cancelled order">
                                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-workshop" data-title="Workshop">
                                                        {{$merchant->name}}</td>
                                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-workshop" data-title="Workshop">
                                                        {{$merchant->categories}}</td>
                                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-start_date" data-title="Start Date">
                                                        {{$merchant->phone}}</td>
                                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-date" data-title="Order Date">
                                                        {{$merchant->city}}</td>
                                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-status" data-title="Status">
                                                        {{$merchant->email}}</td>
                                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-actions" data-title="Actions">
                                                    <a href="/showVerifyMerchant/{{$merchant->id}}" class="woocommerce-button button view">View</a>&nbsp<a href="/verifyMerchant/{{$merchant->id}}" class="woocommerce-button button view">Verify</a></td>
                                                </tr>
                                            @endforeach
                                        @else
                                        <h4>Belum Ada Lembaga Yang Perlu Di Verifikasi</h4>
                                        @endif
                                    </tbody>
                                </table>
                                @if(count($unverifiedMerchant)>0)
                                {{$unverifiedMerchant->links()}} 
                                @endif 
                        </div>
                        <div id="1" class="message" style="display:none">
                            @if(count($unverifiedCourse)>0)
                            <table class="woocommerce-orders-table woocommerce-MyAccount-orders shop_table shop_table_responsive my_account_orders account-orders-table">
                                <thead>
                                    <tr>
                                        <th class="woocommerce-orders-table__header woocommerce-orders-table__header-workshop"><span class="nobr">Lembaga</span></th>
                                        <th class="woocommerce-orders-table__header woocommerce-orders-table__header-workshop"><span class="nobr">Judul</span></th>
                                        <th class="woocommerce-orders-table__header woocommerce-orders-table__header-start_date"><span class="nobr">Kategori</span></th>
                                        <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-date"><span class="nobr">Metode</span></th>
                                        <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-status"><span class="nobr">Durasi</span></th>
                                        <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-actions"><span class="nobr">Action</span></th>
                                    </tr>
                                </thead>
                                <tbody>
                                        @foreach($unverifiedCourse as $course)
                                        <tr class="woocommerce-orders-table__row woocommerce-orders-table__row--status-cancelled order">
                                                <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-workshop" data-title="Workshop">
                                                    {{$course->merchant->name}}</td>
                                                <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-workshop" data-title="Workshop">
                                                    {{$course->title}}</td>
                                                <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-start_date" data-title="Start Date">
                                                    {{$course->category}}</td>
                                                <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-date" data-title="Order Date">
                                                    {{$course->method}}</td>
                                                <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-status" data-title="Status">
                                                    {{$course->duration}}</td>
                                                <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-actions" data-title="Actions">
                                                <a href="/course/{{$course->id}}" class="woocommerce-button button view">View</a>&nbsp<a href="/verifyCourse/{{$course->id}}" class="woocommerce-button button view">Verify</a></td>
                                            </tr>
                                        @endforeach
                                    @else
                                    <h4>Belum Ada Kursus Yang Perlu Di Verifikasi</h4>
                                    @endif
                                </tbody>
                            </table>
                            @if(count($unverifiedCourse)>0)
                            {{$unverifiedCourse->links()}}  
                            @endif
                            
                        </div>
                        <div id="2" class="message" style="display:none">
                                @if(count($unverifiedSlider)>0)
                                <h4>Daftar Pengajuan Iklan Slider : </h4>
                                <table class="woocommerce-orders-table woocommerce-MyAccount-orders shop_table shop_table_responsive my_account_orders account-orders-table">
                                    <thead>
                                        <tr>
                                            <th class="woocommerce-orders-table__header woocommerce-orders-table__header-workshop"><span class="nobr">Lembaga</span></th>
                                            <th class="woocommerce-orders-table__header woocommerce-orders-table__header-workshop"><span class="nobr">Kursus</span></th>
                                            <th class="woocommerce-orders-table__header woocommerce-orders-table__header-start_date"><span class="nobr">Kategori</span></th>
                                            <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-date"><span class="nobr">Metode</span></th>
                                            <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-status"><span class="nobr">Durasi</span></th>
                                            <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-actions"><span class="nobr">Action</span></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                            @foreach($unverifiedSlider as $slide)
                                            <tr class="woocommerce-orders-table__row woocommerce-orders-table__row--status-cancelled order">
                                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-workshop" data-title="Workshop">
                                                        {{$slide->merchant->name}}</td>
                                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-workshop" data-title="Workshop">
                                                        {{$slide->course->title}}</td>
                                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-start_date" data-title="Start Date">
                                                        {{$slide->course->category}}</td>
                                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-date" data-title="Order Date">
                                                        {{$slide->course->method}}</td>
                                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-status" data-title="Status">
                                                        {{$slide->course->duration}}</td>
                                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-actions" data-title="Actions">
                                                    <a onclick="if(confirm('Apakah Anda Yakin Akan Menghapus Pengajuan promosi slider Ini ?')) {
                                                        window.location.href = '/deleteSlide/{{$slide->id}}'
                                                    }" class="woocommerce-button button view">Hapus</a>&nbsp<a href="/verifikasiSlider/{{$slide->id}}" class="woocommerce-button button view">Verifikasi</a></td>
                                                </tr>
                                            @endforeach
                                        @else
                                        <h4>Belum Ada Pengajuan Slider Yang Perlu Di Verifikasi</h4>
                                        @endif
                                    </tbody>
                                </table>
                                @if(count($unverifiedSlider)>0)
                                {{$unverifiedSlider->links()}}  
                                @endif
                                @if(count($unverifiedFlash)>0)
                                <h4>Daftar Pengajuan Flash Sale : </h4>
                                <table class="woocommerce-orders-table woocommerce-MyAccount-orders shop_table shop_table_responsive my_account_orders account-orders-table">
                                    <thead>
                                        <tr>
                                            <th class="woocommerce-orders-table__header woocommerce-orders-table__header-workshop"><span class="nobr">Lembaga</span></th>
                                            <th class="woocommerce-orders-table__header woocommerce-orders-table__header-workshop"><span class="nobr">Kursus</span></th>
                                            <th class="woocommerce-orders-table__header woocommerce-orders-table__header-start_date"><span class="nobr">Kategori</span></th>
                                            <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-date"><span class="nobr">Metode</span></th>
                                            <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-status"><span class="nobr">Durasi</span></th>
                                            <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-actions"><span class="nobr">Action</span></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                            @foreach($unverifiedFlash as $flash)
                                            <tr class="woocommerce-orders-table__row woocommerce-orders-table__row--status-cancelled order">
                                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-workshop" data-title="Workshop">
                                                        {{$flash->merchant->name}}</td>
                                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-workshop" data-title="Workshop">
                                                        {{$flash->course->title}}</td>
                                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-start_date" data-title="Start Date">
                                                        {{$flash->course->category}}</td>
                                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-date" data-title="Order Date">
                                                        {{$flash->course->method}}</td>
                                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-status" data-title="Status">
                                                        {{$flash->course->duration}}</td>
                                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-actions" data-title="Actions">
                                                    <a onclick="if(confirm('Apakah Anda Yakin Akan Menghapus Pengajuan promosi slider Ini ?')) {
                                                        window.location.href = '/deleteFlash/{{$flash->id}}'
                                                    }" class="woocommerce-button button view">Hapus</a>&nbsp<a href="/verifikasiFlash/{{$flash->id}}" class="woocommerce-button button view">Verifikasi</a></td>
                                                </tr>
                                            @endforeach
                                        @else
                                        <h4>Belum Ada Pengajuan Flash Sale Yang Perlu Di Verifikasi</h4>
                                        @endif
                                    </tbody>
                                </table>
                                @if(count($unverifiedFlash)>0)
                                {{$unverifiedFlash->links()}}  
                                @endif
                                @if(count($unverifiedTop)>0)
                                <h4>Daftar Pengajuan Iklan Top Kursus : </h4>
                                <table class="woocommerce-orders-table woocommerce-MyAccount-orders shop_table shop_table_responsive my_account_orders account-orders-table">
                                    <thead>
                                        <tr>
                                            <th class="woocommerce-orders-table__header woocommerce-orders-table__header-workshop"><span class="nobr">Lembaga</span></th>
                                            <th class="woocommerce-orders-table__header woocommerce-orders-table__header-workshop"><span class="nobr">Kursus</span></th>
                                            <th class="woocommerce-orders-table__header woocommerce-orders-table__header-start_date"><span class="nobr">Kategori</span></th>
                                            <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-date"><span class="nobr">Metode</span></th>
                                            <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-status"><span class="nobr">Durasi</span></th>
                                            <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-actions"><span class="nobr">Action</span></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                            @foreach($unverifiedTop as $top)
                                            <tr class="woocommerce-orders-table__row woocommerce-orders-table__row--status-cancelled order">
                                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-workshop" data-title="Workshop">
                                                        {{$top->merchant->name}}</td>
                                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-workshop" data-title="Workshop">
                                                        {{$top->course->title}}</td>
                                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-start_date" data-title="Start Date">
                                                        {{$top->course->category}}</td>
                                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-date" data-title="Order Date">
                                                        {{$top->course->method}}</td>
                                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-status" data-title="Status">
                                                        {{$top->course->duration}}</td>
                                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-actions" data-title="Actions">
                                                    <a onclick="if(confirm('Apakah Anda Yakin Akan Menghapus Pengajuan promosi slider Ini ?')) {
                                                        window.location.href = '/deleteTop/{{$top->id}}'
                                                    }" class="woocommerce-button button view">Hapus</a>&nbsp<a href="/verifikasiTop/{{$top->id}}" class="woocommerce-button button view">Verifikasi</a></td>
                                                </tr>
                                            @endforeach
                                        @else
                                        <h4>Belum Ada Pengajuan Iklan Top Kursus Yang Perlu Di Verifikasi</h4>
                                        @endif
                                    </tbody>
                                </table>
                                @if(count($unverifiedTop)>0)
                                {{$unverifiedTop->links()}}  
                                @endif
                                @if(count($unverifiedRecommended)>0)
                                <h4>Daftar Pengajuan Recommended Course : </h4>
                                <table class="woocommerce-orders-table woocommerce-MyAccount-orders shop_table shop_table_responsive my_account_orders account-orders-table">
                                    <thead>
                                        <tr>
                                            <th class="woocommerce-orders-table__header woocommerce-orders-table__header-workshop"><span class="nobr">Lembaga</span></th>
                                            <th class="woocommerce-orders-table__header woocommerce-orders-table__header-workshop"><span class="nobr">Kursus</span></th>
                                            <th class="woocommerce-orders-table__header woocommerce-orders-table__header-start_date"><span class="nobr">Kategori</span></th>
                                            <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-date"><span class="nobr">Metode</span></th>
                                            <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-status"><span class="nobr">Durasi</span></th>
                                            <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-actions"><span class="nobr">Action</span></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                            @foreach($unverifiedRecommended as $recommended)
                                            <tr class="woocommerce-orders-table__row woocommerce-orders-table__row--status-cancelled order">
                                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-workshop" data-title="Workshop">
                                                        {{$recommended->merchant->name}}</td>
                                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-workshop" data-title="Workshop">
                                                        {{$recommended->course->title}}</td>
                                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-start_date" data-title="Start Date">
                                                        {{$recommended->course->category}}</td>
                                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-date" data-title="Order Date">
                                                        {{$recommended->course->method}}</td>
                                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-status" data-title="Status">
                                                        {{$recommended->course->duration}}</td>
                                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-actions" data-title="Actions">
                                                    <a onclick="if(confirm('Apakah Anda Yakin Akan Menghapus Pengajuan promosi slider Ini ?')) {
                                                        window.location.href = '/deleteRecommended/{{$recommended->id}}'
                                                    }" class="woocommerce-button button view">Hapus</a>&nbsp<a href="/verifikasiRecommended/{{$recommended->id}}" class="woocommerce-button button view">Verifikasi</a></td>
                                                </tr>
                                            @endforeach
                                        @else
                                        <h4>Belum Ada Pengajuan Recommended Course Yang Perlu Di Verifikasi</h4>
                                        @endif
                                    </tbody>
                                </table>
                                @if(count($unverifiedRecommended)>0)
                                {{$unverifiedRecommended->links()}}  
                                @endif
                        </div>
                        <div id="3" class="message" style="display:none">
                            @if(count($unverifiedParticipant)>0)
                            <table class="woocommerce-orders-table woocommerce-MyAccount-orders shop_table shop_table_responsive my_account_orders account-orders-table">
                                <thead>
                                    <tr>
                                        <th class="woocommerce-orders-table__header woocommerce-orders-table__header-workshop"><span class="nobr">Lembaga</span></th>
                                        <th class="woocommerce-orders-table__header woocommerce-orders-table__header-workshop"><span class="nobr">Kursus</span></th>
                                        <th class="woocommerce-orders-table__header woocommerce-orders-table__header-start_date"><span class="nobr">Total</span></th>
                                        <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-date"><span class="nobr">Tanggal Mulai</span></th>
                                        <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-status"><span class="nobr">Periode</span></th>
                                        <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-actions"><span class="nobr">Action</span></th>
                                    </tr>
                                </thead>
                                <tbody>
                                        @foreach($unverifiedParticipant as $course)
                                        <tr class="woocommerce-orders-table__row woocommerce-orders-table__row--status-cancelled order">
                                                <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-workshop" data-title="Workshop">
                                                    {{$course->merchant->name}}</td>
                                                <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-workshop" data-title="Workshop">
                                                    {{$course->title}}</td>
                                                <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-start_date" data-title="Start Date">
                                                    {{$course->invoices[0]->total}}</td>
                                                <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-date" data-title="Order Date">
                                                    {{$course->invoices[0]->start_date}}</td>
                                                <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-status" data-title="Status">
                                                    {{$course->invoices[0]->periode}}</td>
                                                <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-actions" data-title="Actions">
                                                <a href="{{asset('images/'.$course->invoices[0]->bukti)}}" class="woocommerce-button button view">View</a>&nbsp<a href="/verifPendaftaran/{{$course->invoices[0]->id}}" class="woocommerce-button button view">Verify</a></td>
                                            </tr>
                                        @endforeach
                                    @else
                                    <h4>Belum Ada Pendaftaran Yang Perlu Di Verifikasi</h4>
                                    @endif
                                </tbody>
                            </table>
                            @if(count($unverifiedParticipant)>0)
                            {{$unverifiedParticipant->links()}}  
                            @endif
                            
                        </div>
                        <br>
                            @include('inc.message')
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection