@extends('inc.layout') @section('content')


<div id="content" class="main-container">
    <div class="woocommerce">
        <div id="customer-account">
            <div class="container">
                <div class="customer-account-wrapper">
                    <nav class="woocommerce-MyAccount-navigation">
                        <ul>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--dashboard"><a href="{{ route('adminpage') }}">Dashboard</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards"><a href="{{ route('admin_verify') }}">Verifikasi</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--orders"><a href="{{ route('admin_omzet') }}">Analisis Omzet & Peserta</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards is-active"><a href="{{ route('admin_promotion') }}">Kelola Promosi</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards"><a href="{{ route('admin_marketing') }}">Marketing</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards"><a href="{{ route('admin_blog') }}">Blog</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--customer-logout"><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                              document.getElementById('logout-form').submit();">Logout</a></li>
                        </ul>
                    </nav>
                    <div class="woocommerce-MyAccount-content">
                        @if(count($slider)>0) <hr><h4>Daftar Slider Aktif : </h4> @foreach ($slider as $slide)
                        

                    <div class="col-md-7 col-sm-4">
                        <div class="post course">
                            <div class=content-wrapper>
                                <a href="/course/{{$slide->course->id}}" class=img-fit-wrapper>
                                    <img width=450 height=250 src="<?php echo asset('/images') ?>/<?php echo $slide->image?>" data-lazy-type=image data-src=https://cdn.maubelajarapa.com/wp-content/uploads/2018/10/31103821/pexels-photo-529922-950x850.jpeg class="lazy lazy-hidden img-lazy post-img wp-post-image"
                                        alt="sensory coffee jakarta"><noscript><img width=450 height=250 src="<?php echo asset('/images') ?>/<?php echo $slide->image?>" class="post-img wp-post-image" alt="sensory coffee jakarta"></noscript>                                    </a>
                                <div class=post-content>
                                    <span class=course-category title=Coffee>{{$slide->course->category}}</span>
                                    <a href="/course/{{$slide->course->id}}" class=post-title>
                                        <strong>Kursus:</strong> {{$slide->course->title}}</a>
                                    <div class=course-info>
                                        <strong class=course-price>
                                                <span class="woocommerce-Price-amount amount">
                                                    <span class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;{{$slide->course->price}}</span>		</strong>
                                        <strong class=time-exp> {{$slide->course->duration}}	</strong></div>
                                </div>
                                <div class=course-footer>
                                    <div class=course-footer-heading>
                                        <a class=course-vendor>
                                            <img src="<?php echo asset('/images') ?>/<?php echo $slide->course->merchant->profileImage?>" alt="{{$slide->course->merchant->name}}" class=vendor-img>
                                            <span>{{$slide->course->merchant->name}}</span>
                                            <strong>{{$slide->course->merchant->address}}</strong>
                                        </a>
                                        <div class=course-date>
                                            <span class=month>Aug</span>
                                            <strong class=date>27</strong></div>
                                        <a href=# class=show-other-dates>Action</a></div>
                                    <div class=course-other-dates>
                                            <ul class=date-list>
                                                <li><a onclick="if(confirm('Apakah Anda Yakin Akan Menghapus Slider Ini ?')) {
                                                    window.location.href = '/deleteSlide/{{$slide->id}}'
                                                }">Hapus</a></li>
                                            </ul>
                                            <div class=button-box>
                                                <a href=# class="btn btn-default btn-cancel">Cancel</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @else
                    <h4>Tidak ada slider aktif </h4>
                    @endif
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection