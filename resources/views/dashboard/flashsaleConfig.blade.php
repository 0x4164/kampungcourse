@extends('inc.layout') @section('content')


<div id="content" class="main-container">
    <div class="woocommerce">
        <div id="customer-account">
            <div class="container">
                <div class="customer-account-wrapper">
                    <nav class="woocommerce-MyAccount-navigation">
                        <ul>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--dashboard"><a href="{{ route('adminpage') }}">Dashboard</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards"><a href="{{ route('admin_verify') }}">Verifikasi</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--orders"><a href="{{ route('admin_omzet') }}">Analisis Omzet & Peserta</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards is-active"><a href="{{ route('admin_promotion') }}">Kelola Promosi</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards"><a href="{{ route('admin_marketing') }}">Marketing</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards"><a href="{{ route('admin_blog') }}">Blog</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--customer-logout"><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                              document.getElementById('logout-form').submit();">Logout</a></li>
                        </ul>
                    </nav>
                    <div class="woocommerce-MyAccount-content">
                        @include('inc.message')
                        <form action="/flashsaleConfig" method="post">
                            @csrf
                            <h4>Pengaturan Tanggal & Waktu Flash Sale</h4>
                            <div class="form-group">
                                Date:<input id="date" type="date" name="task_date" required/>
                                Time:<input id="time" type="time" name="task_time" required/><br><br>
                                <input type="hidden" name="flashtime" id="flashtime">
                                <button type="submit" class="btn btn-default">Update</button>
                            </div>
                            <script>
                                var calc = document.getElementById("time")
                                var date = document.getElementById("date")
                                calc.addEventListener("change", function() {
                                    var date = document.getElementById("date").value,
                                        time = document.getElementById("time").value
                                    document.getElementById('flashtime').value=new Date(date + " " + time).getTime();
                                    
                                })
                                date.addEventListener("change", function() {
                                    var date = document.getElementById("date").value,
                                        time = document.getElementById("time").value
                                    document.getElementById('flashtime').value=new Date(date + " " + time).getTime();
                                    
                                })
                            </script>
                        </form>
                        <hr>
                        @if(count($flashsale)>0) <h4>Daftar Flash Sale Aktif : </h4> @foreach ($flashsale as $flash)
                        

                        <div class="col-md-4 col-sm-6">
                        <div class="post course">
                            <div class=content-wrapper>
                                <a href="/course/{{$flash->course->id}}" class=img-fit-wrapper>
                                    <?php $area = (array) json_decode($flash->course->thumbnail, true);
                                        $image = 'noimage.png';
                                        foreach($area as $v)
                                        {
                                            $image = $v;
                                        } ?>
                                    <img width=450 height=250 src="<?php echo asset('/images') ?>/<?php echo $image?>" data-lazy-type=image data-src=https://cdn.maubelajarapa.com/wp-content/uploads/2018/10/31103821/pexels-photo-529922-950x850.jpeg class="lazy lazy-hidden img-lazy post-img wp-post-image"
                                        alt="sensory coffee jakarta"><noscript><img width=450 height=250 src="<?php echo asset('/images') ?>/<?php echo $image?>" class="post-img wp-post-image" alt="sensory coffee jakarta"></noscript>                                    </a>
                                <div class=post-content>
                                    <span class=course-category title=Coffee>{{$flash->course->category}}</span>
                                    <a href="/course/{{$flash->course->id}}" class=post-title>
                                        <strong>Kursus:</strong> {{$flash->course->title}}</a>
                                    <div class=course-info>
                                        <strong class=course-price>
                                                <span class="woocommerce-Price-amount amount">
                                                    <span class=woocommerce-Price-currencySymbol><del>Rp</del></span>&nbsp;<del>{{$flash->course->price}}</del></span>
                                                    <span class="woocommerce-Price-amount amount">
                                                            <span class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;{{$flash->flashPrice}}</span>
                                                		</strong>
                                        <strong class=time-exp> {{$flash->course->duration}}	</strong></div>
                                </div>
                                <div class=course-footer>
                                    <div class=course-footer-heading>
                                        <a class=course-vendor>
                                            <img src="<?php echo asset('/images') ?>/<?php echo $flash->course->merchant->profileImage?>" alt="{{$flash->course->merchant->name}}" class=vendor-img>
                                            <span>{{$flash->course->merchant->name}}</span>
                                            <strong>{{$flash->course->merchant->address}}</strong>
                                        </a>
                                        <div class=course-date>
                                            <span class=month>Aug</span>
                                            <strong class=date>27</strong></div>
                                        <a href=# class=show-other-dates>Action</a></div>
                                    <div class=course-other-dates>
                                            <ul class=date-list>
                                                <li><a onclick="if(confirm('Apakah Anda Yakin Akan Menghapus Flash Sale Ini ?')) {
                                                    window.location.href = '/deleteFlash/{{$flash->id}}'
                                                }">Hapus</a></li>
                                            </ul>
                                            <div class=button-box>
                                                <a href=# class="btn btn-default btn-cancel">Cancel</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @else
                    <h4>Tidak ada Flash Sale aktif </h4>
                    @endif

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection