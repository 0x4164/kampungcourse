@extends('inc.layout') @section('content')


<div id="content" class="main-container">
    <div class="woocommerce">
        <div id="customer-account">
            <div class="container">
                <div class="customer-account-wrapper">
                    <nav class="woocommerce-MyAccount-navigation">
                        <ul>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--dashboard"><a href="{{ route('adminpage') }}">Dashboard</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards"><a href="{{ route('admin_verify') }}">Verifikasi</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--orders is-active"><a href="{{ route('admin_omzet') }}">Analisis Omzet & Peserta</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards"><a href="{{ route('admin_promotion') }}">Kelola Promosi</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards"><a href="{{ route('admin_marketing') }}">Marketing</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards"><a href="{{ route('admin_blog') }}">Blog</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--customer-logout"><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                              document.getElementById('logout-form').submit();">Logout</a></li>
                        </ul>
                    </nav>
                    <div class="woocommerce-MyAccount-content">
                        <label>Filter Lembaga : </label><br>
                        <form action="/admin-omzet" method="POST">
                        @csrf
                        <select id="selection" name="lembaga">
                            @foreach($merchants as $merchant)
                        <option value="{{$merchant->id}}">{{$merchant->name}}</option>
                            @endforeach
                            </select>
                        <br><br><label>Filter Tanggal : </label><br>
                        <select name="bulan">
                                <option value="">Semua</option>
                                <option value="-01-">Januari</option>
                                <option value="-02-">Februari</option>
                                <option value="-03-">Maret</option>
                                <option value="-04-">April</option>
                                <option value="-05-">Mei</option>
                                <option value="-06-">Juni</option>
                                <option value="-07-">Juli</option>
                                <option value="-08-">Agustus</option>
                                <option value="-09-">September</option>
                                <option value="-10-">Oktober</option>
                                <option value="-11-">November</option>
                                <option value="-12-">Desember</option>
                            </select>
                        <select name="tahun">
                            <option value="">Semua</option>
                            <option value="2019-">2019</option>
                            <option value="2020-">2020</option>
                            <option value="2021-">2021</option>
                        </select>
                        <br><br><label>Filter Periode : </label><br>
                        <select name="periode">
                                <option value="">Semua</option>
                                <option value="1 Minggu">1 Minggu</option>
                                <option value="2 Minggu">2 Minggu</option>
                                <option value="1 Tahun">1 Tahun</option>
                            </select>
                        <br><br><button class="btn" type="submit">Submit</button>
                        </form>
                        @if(isset($omzet))
                        <h4>Total Omzet : {{$omzet}}</h4>
                        @endif
                        @if(isset($participant))
                        <h4>Total Peserta : {{$participant}}</h4>
                        @endif
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection