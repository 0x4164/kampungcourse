@extends('inc.layout') @section('content')


<div id="content" class="main-container">
    <div class="woocommerce">
        <div id="customer-account">
            <div class="container">
                <div class="customer-account-wrapper">
                    <nav class="woocommerce-MyAccount-navigation">
                        <ul>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--dashboard"><a href="{{ route('adminpage') }}">Dashboard</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards"><a href="{{ route('admin_verify') }}">Verifikasi</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--orders"><a href="{{ route('admin_omzet') }}">Analisis Omzet & Peserta</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards"><a href="{{ route('admin_promotion') }}">Kelola Promosi</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards"><a href="{{ route('admin_marketing') }}">Marketing</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards is-active"><a href="{{ route('admin_blog') }}">Blog</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--customer-logout"><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                              document.getElementById('logout-form').submit();">Logout</a></li>
                        </ul>
                    </nav>
                    <div class="woocommerce-MyAccount-content">
                    <a class="btn btn-default" href="{{ route('blog.create') }}">Buat Post Baru</a>
                        <br><br>

                        <table class="woocommerce-orders-table woocommerce-MyAccount-orders shop_table shop_table_responsive my_account_orders account-orders-table">
                            <thead>
                                <tr>
                                    <th class="woocommerce-orders-table__header woocommerce-orders-table__header-workshop"><span class="nobr">Title</span></th>
                                    <th class="woocommerce-orders-table__header woocommerce-orders-table__header-start_date"><span class="nobr">Publish Date</span></th>
                                    <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-date"><span class="nobr">Like</span></th>
                                    <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-status"><span class="nobr">Comment</span></th>
                                    <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-actions"><span class="nobr">Actions</span></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($posts)>0)
                                    @foreach($posts as $post)
                                    <tr class="woocommerce-orders-table__row woocommerce-orders-table__row--status-cancelled order">
                                            <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-workshop" data-title="Workshop">
                                                {{$post->title}}</td>
                                            <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-start_date" data-title="Start Date">
                                                {{$post->created_at}}</td>
                                            <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-date" data-title="Order Date">
                                                812</td>
                                            <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-status" data-title="Status">
                                                125</td>
                                            <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-actions" data-title="Actions">
                                                    <a href="/posts/{{$post->id}}" class="woocommerce-button button view">View</a>&nbsp
                                                    <button onclick="document.getElementById('deleteForm{{$post->id}}').submit()" class="woocommerce-button button view">Delete</button>
                                            {!!Form::open(['action' => ['BlogController@destroy', $post->id], 'id' => 'deleteForm'.$post->id, 'method' => 'POST', 'style' => 'display:inline; padding:0; margin:0; border 0;'])!!}
                                                {{Form::hidden('_method', 'DELETE')}}
                                            {!!Form::close()!!}
                                            
                                        </tr>
                                    @endforeach
                                @else
                                <tr class="woocommerce-orders-table__row woocommerce-orders-table__row--status-cancelled order">
                                        <td rowspan="5"; class="woocommerce-orders-table__cell woocommerce-orders-table__cell-workshop" data-title="Workshop">
                                            No Post Found</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                        {{$posts->links()}}
                        <br>
                        @include('inc.message')
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection