@extends('inc.layout')
@section('content')


<div id="content" class="main-container">
    <div class="woocommerce">
        <div id="customer-account">
            <div class="container">
                <div class="customer-account-wrapper">
                    <div class="woocommerce-MyAccount-content">
                        <table class="woocommerce-orders-table woocommerce-MyAccount-orders shop_table shop_table_responsive my_account_orders account-orders-table">
                            <thead>
                                <tr>
                                    <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-number"><span class="nobr">Order</span></th>
                                    <th class="woocommerce-orders-table__header woocommerce-orders-table__header-workshop"><span class="nobr">Workshop</span></th>
                                    <th class="woocommerce-orders-table__header woocommerce-orders-table__header-start_date"><span class="nobr">Start Date</span></th>
                                    <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-date"><span class="nobr">Order Date</span></th>
                                    <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-status"><span class="nobr">Status</span></th>
                                    <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-total"><span class="nobr">Total</span></th>
                                    <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-actions"><span class="nobr">Upload Bukti Transfer</span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="woocommerce-orders-table__row woocommerce-orders-table__row--status-cancelled order">
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-number" data-title="Order">
                                        <a href="#">
#205628	</a></td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-workshop" data-title="Workshop">
                                        Learn How To Measure &amp; Analyze Your Mobile App Success</td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-start_date" data-title="Start Date">
                                        September 18, 2019 19:00:00</td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-date" data-title="Order Date">
                                        <time datetime="2019-09-16T15:10:37+00:00">Sep 16, 2019</time></td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-status" data-title="Status">
                                        Cancelled</td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-total" data-title="Total">
                                        <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">Rp</span>&nbsp;1.500.000</span> for 1 item</td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-actions"
                                        data-title="Actions">
                                        <input type="file" onchange="window.location.href='/'" class="woocommerce-button button view"></td>
                                </tr>
                                <tr class="woocommerce-orders-table__row woocommerce-orders-table__row--status-cancelled order">
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-number" data-title="Order">
                                        <a href="https://maubelajarapa.com/account/view-order/205627">
#205627	</a></td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-workshop" data-title="Workshop">
                                        Learn The Essentials Of Business Writing</td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-start_date" data-title="Start Date">
                                        September 18, 2019 08:30:00</td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-date" data-title="Order Date">
                                        <time datetime="2019-09-16T15:07:36+00:00">Sep 16, 2019</time></td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-status" data-title="Status">
                                        Cancelled</td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-total" data-title="Total">
                                        <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">Rp</span>&nbsp;2.500.000</span> for 1 item</td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-actions"
                                        data-title="Actions">
                                        <input type="file" onchange="window.location.href='/'" class="woocommerce-button button view"></td>
                                </tr>
                                <tr class="woocommerce-orders-table__row woocommerce-orders-table__row--status-cancelled order">
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-number" data-title="Order">
                                        <a href="https://maubelajarapa.com/account/view-order/197861">
#197861	</a></td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-workshop" data-title="Workshop">
                                        Transforming Your Family Business In The Digital Era</td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-start_date" data-title="Start Date">
                                        August 15, 2019 19:00:00</td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-date" data-title="Order Date">
                                        <time datetime="2019-08-02T22:55:36+00:00">Aug 2, 2019</time></td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-status" data-title="Status">
                                        Cancelled</td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-total" data-title="Total">
                                        <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">Rp</span>&nbsp;150.000</span> for 1 item</td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-actions"
                                        data-title="Actions">
                                        <input type="file" onchange="window.location.href='/'" class="woocommerce-button button view"></td>
                                </tr>
                            </tbody>
                        </table>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    @endsection