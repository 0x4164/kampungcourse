@extends('inc.layout')
@section('content')


<div id="content" class="main-container">
    <div class="woocommerce">
        <div id="customer-account">
            <div class="container">
                <div class="customer-account-wrapper">
                    <nav class="woocommerce-MyAccount-navigation">
                        <ul>
                                <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--dashboard is-active"><a href="{{ route('adminpage') }}">Dashboard</a></li>
                                <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards"><a href="{{ route('admin_verify') }}">Verifikasi</a></li>
                                <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--orders"><a href="{{ route('admin_omzet') }}">Analisis Omzet & Peserta</a></li>
                                <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards"><a href="{{ route('admin_promotion') }}">Kelola Promosi</a></li>
                                <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards"><a href="{{ route('admin_marketing') }}">Marketing</a></li>
                                <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards"><a href="{{ route('admin_blog') }}">Blog</a></li>
                                <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--customer-logout"><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                              document.getElementById('logout-form').submit();">Logout</a></li>
                        </ul>
                    </nav>
                    <div class="woocommerce-MyAccount-content">
                        <p>Hai <strong>{{ Auth::guard('admin')->user()->name }}</strong> (bukan <strong>{{ Auth::guard('admin')->user()->name }}</strong>? <a href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                          document.getElementById('logout-form').submit();">Log out</a>)</p>
                        <p>Dari dashboard anda dapat Memverifikasi <a href="#">Lembaga, Kursus, & Iklan</a>, Melihat analisis <a href="#">Omzet & Peserta</a>, Mengelola <a href="#">Promosi</a>, <a href="#">Marketing</a>, dan <a href="#">Blog</a></p>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    @endsection