@extends('inc.layout') @section('content')
<?php use \App\Http\Controllers\ChatController; ?>

<div id="content" class="main-container">
    <div class="woocommerce">
        <div id="customer-account">
            <div class="container">
                <div class="customer-account-wrapper">
                    <nav class="woocommerce-MyAccount-navigation">
                        <ul>

                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--dashboard"><a>{{$to->name}}</a></li>


                        </ul>
                    </nav>

                    <div class="woocommerce-MyAccount-content" style="text-align:center;">
                        <div class="row">
                            <style>
                                @media (max-width: 800px) {
                                    #show {
                                        height: 50vh;
                                        border: gray;
                                        border-style: solid;
                                        overflow-x: auto;
                                        ;
                                    }
                                    #message {}
                                    #mescol {
                                        display: grid;
                                        grid-template-columns: 80% 20%
                                    }
                                }
                                
                                @media (min-width: 800px) {
                                    #show {
                                        width: 40vw;
                                        height: 17vw;
                                        border: gray;
                                        border-style: solid;
                                        overflow-x: auto;
                                        ;
                                    }
                                    #message {
                                        width: 40vw;
                                    }
                                    #hr {
                                        width: 40vw;
                                    }
                                    #mescol {
                                        width: 40vw;
                                        display: grid;
                                        grid-template-columns: 90% 10%
                                    }
                                }
                            </style>
                            <div id="show">
                                @if(count($chat)>0) @foreach ($chat as $item) @if($item->dari == 'peserta')
                                <p style="text-align:right; padding-right:5px;"><a class="btn btn-primary">{{$item->message}}</a></p>
                                @endif @if($item->dari == 'lembaga')
                                <p style="text-align:left; padding-left:5px;"><a class="btn btn-default">{{$item->message}}</a></p>
                                @endif @endforeach @endif
                            </div>
                        </div>
                        <div class="row">
                            <hr id="hr">
                            <form id="message-form" method="post">
                                @csrf @if(auth()->check())
                                <input type="hidden" name="merchant_id" value="{{$to->id}}">
                                <input type="hidden" name="user_id" value="{{auth()->id()}}"> @endif @if(auth('merchant')->check())
                                <input type="hidden" name="merchant_id" value="{{auth('merchant')->id()}}">
                                <input type="hidden" name="user_id" value="{{$to->id}}"> @endif
                                <div id="mescol">
                                    <input name="message" id="message" type="text">
                                    <button type="submit" style="height:35px; padding-left:8px;" class="btn btn-primary">Send</button>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>



<script>
    var $j = jQuery.noConflict();
    var date = new Date();
    var lastCheck = date.toISOString().split('T')[0] + ' ' + date.toTimeString().split(' ')[0];

    $j(document).ready(function() {
        // run the first time; all subsequent calls will take care of themselves
        var element = document.getElementById("show");
        element.scrollTop = element.scrollHeight;
        setTimeout(executeQuery, 5000);
    });

    function executeQuery() {
        var fd = new FormData();
        fd.append('lastCheck', lastCheck);
        var xhr = new XMLHttpRequest();

        xhr.open('POST', APP_URL + '/getMessage');
        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));

        xhr.onload = function() {
            if (this.status == 200) {
                var res = JSON.parse(this.response);

                console.log('Server got:', res);

                if (res.success) {
                    var chat = JSON.parse(res.chat);
                    chat.forEach(element => {
                        if(element.dari == 'peserta')
                        {
                            var chatpanel = document.getElementById('show');
                            var node = document.createElement("p");
                            var anode = document.createElement("a");
                            anode.classList.add('btn');
                            
                            node.style.textAlign = "right";
                            node.style.paddingRight = "5px";
                            anode.classList.add('btn-primary');
                            
                            var textnode = document.createTextNode(element.message);
                            anode.appendChild(textnode);
                            node.appendChild(anode);
                            chatpanel.appendChild(node);
                        }
                        if(element.dari == 'lembaga')
                        {
                            var chatpanel = document.getElementById('show');
                            var node = document.createElement("p");
                            var anode = document.createElement("a");
                            anode.classList.add('btn');
                            
                            node.style.textAlign = "left";
                            node.style.paddingLeft = "5px";
                            anode.classList.add('btn-default');
                            
                            var textnode = document.createTextNode(element.message);
                            anode.appendChild(textnode);
                            node.appendChild(anode);
                            chatpanel.appendChild(node);
                        }
                    });
                    date = new Date();
                    lastCheck = date.toISOString().split('T')[0] + ' ' + date.toTimeString().split(' ')[0];
                    var element = document.getElementById("show");
                    element.scrollTop = element.scrollHeight;
                } else {

                }
            } else {
                swal({
                    type: 'error',
                    title: 'Error!',
                    html: 'Failed to retrieve message'
                });
            }

        };

        xhr.send(fd);
        setTimeout(executeQuery, 5000); // you could choose not to continue on failure...
    }


    $j(document).on('submit', '#message-form', function(e) {
        e.preventDefault();


        var fd = new FormData(document.getElementById("message-form"));

        var xhr = new XMLHttpRequest();

        xhr.open('POST', APP_URL + '/sendChat');
        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));

        xhr.onload = function() {
            if (this.status == 200) {
                var res = JSON.parse(this.response);

                console.log('Server got:', res);

                if (res.success) {
                    /*var chatpanel = document.getElementById('show');
                    var mess = document.getElementById('message');
                    var node = document.createElement("p");
                    var anode = document.createElement("a");
                    anode.classList.add('btn');
                    @if(auth()->check())
                    node.style.textAlign = "right";
                    node.style.paddingRight = "5px";
                    anode.classList.add('btn-primary');
                    @else
                    node.style.textAlign = "left";
                    node.style.paddingLeft = "5px";
                    anode.classList.add('btn-default');
                    @endif
                    var textnode = document.createTextNode(mess.value);
                    anode.appendChild(textnode);
                    node.appendChild(anode);
                    chatpanel.appendChild(node);*/
                    document.getElementById('message').value = "";
                } else {

                }
            } else {
                swal({
                    type: 'error',
                    title: 'Error!',
                    html: 'Failed to send message'
                });
            }

        };

        xhr.send(fd);

    });
</script>
@endsection