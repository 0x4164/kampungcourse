@extends('inc.layout') @section('content') {!! Form::open(['action' => ['BlogController@update', $post->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
<div class="container site-content">
    <a href="{{ route('admin_blog') }}" class="btn btn-default">Back</a>
    <input type="submit" value="save" class="btn btn-default pull-right hidden-xs">
    <div style="width:24%;" class="pull-right"><input type="submit" value="save" class="btn btn-default visible-xs"></div>
    <hr>
    <section id=single-post class=page-section>
        <div class=row>
            <div class="col-md-10 col-md-push-1">
                <div class=single-post-wrapper>
                    <article id=post-177072 class="post single-post">
                        <div class=post-header>
                            <h1 class="post-title"><textarea style="width:100%; height:auto; !important font-size:30px" name="title">{{$post->title}}</textarea></h1>
                            <ul class=post-meta>
                                <li class=post-viewed><span class=pageviews-placeholder data-key=177072></span></li>
                                <li class=post-date>{{$post->created_at}}</li>
                                <li class=post-category>On &nbsp<input type="text" style="height:90%" name="tags" value="{{$post->tags}}"></li>
                            </ul>
                        </div>
                        <div class="post-img img-fit-wrapper">
                            <img width=1947 height=1095 src="<?php echo asset('/images') ?>/{{$post->thumbnail}}" data-lazy-type=image data-src=https://cdn.maubelajarapa.com/wp-content/uploads/2019/01/21093008/photo-1534829178390-5312a631a68e.jpg class="lazy lazy-hidden img-lazy attachment-full size-full wp-post-image"
                                alt="tips biar sukses" data-lazy-srcset="https://cdn.maubelajarapa.com/wp-content/uploads/2019/01/21093008/photo-1534829178390-5312a631a68e.jpg 1947w, https://cdn.maubelajarapa.com/wp-content/uploads/2019/01/21093008/photo-1534829178390-5312a631a68e-768x432.jpg 768w"
                                data-lazy-sizes="(max-width: 1947px) 100vw, 1947px"><noscript><img
width=1947 height=1095 src=../../cdn.maubelajarapa.com/wp-content/uploads/2019/01/21093008/photo-1534829178390-5312a631a68e.jpg class="attachment-full size-full wp-post-image" alt="tips biar sukses" srcset="https://cdn.maubelajarapa.com/wp-content/uploads/2019/01/21093008/photo-1534829178390-5312a631a68e.jpg 1947w, https://cdn.maubelajarapa.com/wp-content/uploads/2019/01/21093008/photo-1534829178390-5312a631a68e-768x432.jpg 768w" sizes="(max-width: 1947px) 100vw, 1947px"></noscript></div>
                        <div class="form-group">
                            {{Form::label('image', 'Gambar Thumbnail')}} {{Form::file('image')}}
                        </div>
                        <div class=author-box>
                            <div class=author-pic>
                                <img src=../../cdn.maubelajarapa.com/wp-content/uploads/2018/10/03141535/evi.png alt width=90 height=90 class="avatar photo"></div>
                            <div class=author-header>
                                <span class=label-title>Author:</span>
                                <h3 class="author-name"><a href=../author/evi/index.html title="Posts by Evi" rel=author>Evi</a></h3>
                                <ul class=author-social>
                                    <li><a href=#><i
class="icon icon-email"></i></a></li>
                                    <li><a href=#><i
class="icon icon-facebook"></i></a></li>
                                    <li><a href=#><i
class="icon icon-twitter"></i></a></li>
                                </ul>
                            </div>
                            <p class=author-bio></p>
                        </div>
                        <div class=post-entries>
                            <textarea id="editor" name="body">{{$post->body}}</textarea>
                        </div>
                    </article>
                    
                </div>
            </div>
        </div>
    </section>
</div>
{!! Form::close() !!}
<script src="https://cdn.ckeditor.com/ckeditor5/12.4.0/classic/ckeditor.js"></script>
<script>
    ClassicEditor
        .create(document.querySelector('#editor'))
        .catch(error => {
            console.error(error);
        });
</script>
@endsection