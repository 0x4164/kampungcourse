@extends('inc.layout')
@section('content')


<div id="content" class="main-container">
    <div class="woocommerce">
        <div id="customer-account">
            <div class="container">
                <div class="customer-account-wrapper">
                    <nav class="woocommerce-MyAccount-navigation">
                        <ul>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--dashboard"><a href="{{ route('learner-dashboard') }}">Dashboard</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--orders is-active"><a href="{{ route('invoice') }}">Invoice</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards"><a href="{{ route('history') }}">Riwayat</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--edit-account"><a href="{{ route('edit.account') }}">Akun</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--customer-logout"><a href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                              document.getElementById('logout-form').submit();">Logout</a></li>
                        </ul>
                    </nav>
                    <div class="woocommerce-MyAccount-content">
                            <table class="woocommerce-orders-table woocommerce-MyAccount-orders shop_table shop_table_responsive my_account_orders account-orders-table">
                                <thead>
                                    <tr>
                                        <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-number"><span class="nobr">Order</span></th>
                                        <th class="woocommerce-orders-table__header woocommerce-orders-table__header-workshop"><span class="nobr">Workshop</span></th>
                                        <th class="woocommerce-orders-table__header woocommerce-orders-table__header-start_date"><span class="nobr">Start Date</span></th>
                                        <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-date"><span class="nobr">Order Date</span></th>
                                        <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-status"><span class="nobr">Status</span></th>
                                        <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-total"><span class="nobr">Total</span></th>
                                        <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-total"><span class="nobr">Action</span></th>
                                        <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-actions"><span class="nobr">Upload Bukti Transfer</span></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($invoices)>0)
                                    @foreach ($invoices as $invoice)
                                    @foreach ($invoice->invoices as $key => $value)
                                    <tr class="woocommerce-orders-table__row woocommerce-orders-table__row--status-cancelled order">
                                        <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-number" data-title="Order">
                                            <a href="">
                                            #{{$invoice->invoices[$key]->id}}	</a></td>
                                        <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-workshop" data-title="Workshop">
                                            {{$invoice->title}}</td>
                                        <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-start_date" data-title="Start Date">
                                            {{$invoice->invoices[$key]->start_date}}</td>
                                        <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-date" data-title="Order Date">
                                            <time datetime="2019-08-02T22:55:36+00:00">{{$invoice->invoices[$key]->created_at}}</time></td>
                                        <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-status" data-title="Status">
                                            {{$invoice->invoices[$key]->status}}</td>
                                        <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-total" data-title="Total">
                                            <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">Rp</span>&nbsp;{{$invoice->invoices[$key]->total}}</span></td>
                                        <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-actions"
                                        data-title="Actions">
                                        <form style="display:none" id="message{{$invoice->invoices[$key]->id}}" method="post" action="/downloadInvoice">@csrf<input type="hidden" name="message" value="{{$invoice->invoices[$key]->invoice_message}}"></form>
                                        <a href="{{asset('images/'.$invoice->invoices[$key]->bukti)}}" class="woocommerce-button button view">View</a><a onclick="document.getElementById('message{{$invoice->invoices[$key]->id}}').submit()"
                                            class="woocommerce-button button invoice">Download invoice (PDF)</a></td>
                                        <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-actions"
                                            data-title="Upload Bukti Transfer">
                                            <form action="/uploadBuktiTransfer" method="POST" id="trfFrom{{$invoice->invoices[$key]->id}}" enctype="multipart/form-data">
                                                @csrf
                                            <input type="file" name="image" onchange="document.getElementById('trfFrom{{$invoice->invoices[$key]->id}}').submit();" class="woocommerce-button button view"></td>
                                            <input type="hidden" name="invoiceId" value="{{$invoice->invoices[$key]->id}}">
                                            </form>
                                    </tr>
                                    @endforeach
                                    
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>

    @endsection