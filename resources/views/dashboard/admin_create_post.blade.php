@extends('inc.layout') @section('content')


<div id="content" class="main-container">
    <div class="woocommerce">
        <div id="customer-account">
            <div class="container">
                <div class="customer-account-wrapper">
                    <nav class="woocommerce-MyAccount-navigation">
                        <ul>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--dashboard"><a href="{{ route('adminpage') }}">Dashboard</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards"><a href="{{ route('admin_verify') }}">Verifikasi</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--orders"><a href="{{ route('admin_omzet') }}">Analisis Omzet & Peserta</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards"><a href="{{ route('admin_promotion') }}">Kelola Promosi</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards"><a href="{{ route('admin_marketing') }}">Marketing</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards  is-active"><a href="{{ route('admin_blog') }}">Blog</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--customer-logout"><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                              document.getElementById('logout-form').submit();">Logout</a></li>
                        </ul>
                    </nav>
                    <div class="woocommerce-MyAccount-content">
                        @include('inc.message')
                            <h1>Buat Postingan Blog Baru</h1>
                            {!! Form::open(['action' => 'BlogController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                                <div class="form-group">
                                    {{Form::label('title', 'Judul')}}
                                    {{Form::text('title', '', ['class' => 'form-control', 'placeholder' => 'Title'])}}
                                </div>
                                <div class="form-group">
                                    {{Form::label('body', 'Isi')}}
                                    {{Form::textarea('body', '', ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Body Text'])}}
                                </div>
                                <div class="form-group">
                                    {{Form::label('tags', 'Tags')}}
                                    {{Form::text('tags', '', ['class' => 'form-control', 'placeholder' => 'Bahasa Inggris, Speaking, Grammar'])}}
                                </div>
                                <div class="form-group">
                                    {{Form::label('image', 'Gambar Thumbnail')}}
                                    {{Form::file('image')}}
                                </div>
                                {{Form::submit('Submit', ['class'=>'btn btn-primary'])}}
                            {!! Form::close() !!}


                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdn.ckeditor.com/ckeditor5/12.4.0/classic/ckeditor.js"></script>
<script>
    ClassicEditor
        .create(document.querySelector('#editor'))
        .catch(error => {
            console.error(error);
        });
</script>

@endsection