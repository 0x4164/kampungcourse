@extends('inc.layout')
@section('content')

<div id="content" class="main-container">

    <div id="login" style="background-image: url({{asset('assets/wp-content/themes/maubelajarapa/assets/images/vendor-bg.jpg')}})">
        <header class="login-header">
            <h1 class="header-title">Kampung Course</h1>
            <h3 class="header-subtitle">Login Admin</h3>
        </header>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-push-4" style="background: rgba(255, 255, 255, 0.9);">


                    
                        <div class="box-wrapper">
                            <form method="post" action="{{ route('admin-login') }}" class="login-form box-column">
                                @csrf
                                <div class="box-header" style="text-align:center;">
                                    <h3 class="box-title">Login</h3>
                                </div>
                                @if ($errors->has('email'))
                                    <span style="color:red">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                                <div class="form-group">
                                    <label>Email :</label>
                                    <input type="text" class="form-control has-icon" placeholder="Business Email" id="email" name="email" value="{{ old('email') }}" required>
                                    
                                </div>
                                <div class="form-group">
                                        <label>Password :</label>
                                    <input type="password" class="form-control has-icon" placeholder="Password" id="password" name="password" required>
                                </div>
                                <div class="form-group text-right">
                                    <label class="checkbox-style pull-left"><input type="checkbox" name="rememberme" id="rememberme"  value="forever"> <span>Ingat Saya</span></label>
                                    <a href="{{ route('forgotPassword') }}" class="btn-forgot">Lupa Password</a>
                                </div>
                                <div class="form-group">
                                    <input type="hidden" id="woocommerce-login-nonce" name="woocommerce-login-nonce" value="e04e607d49" /><input type="hidden" name="_wp_http_referer" value="/login-as-educator/" /> <input type="hidden" name="login-type"
                                        value="educator">
                                    <input type="submit" class="btn btn-primary btn-submit" name="login" value="Login">
                                </div>
                            </form>

                        </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection