@extends('inc.layout')
@section('content')

<div id=content class=main-container>
    <div class=woocommerce>
        <div class="container lost-password-wrapper">
            <form method=post class="woocommerce-ResetPassword lost_reset_password">
                <p>Kehilangan password anda? Silahkan masukkan email anda, kami akan mengirim tautan ke email untuk mengatur ulang password anda</p>
                <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
                    <label for=user_login>Email</label>
                    <input class="woocommerce-Input woocommerce-Input--text input-text" type=text name=user_login id=user_login></p>
                <div class=clear></div>
                <p class="woocommerce-form-row form-row">
                    <input type=hidden name=wc_reset_password value=true>
                    <input type=submit class="woocommerce-Button button" value="Reset password"></p><input type=hidden id=_wpnonce name=_wpnonce value=bd749e198f><input type=hidden name=_wp_http_referer value=/account/lost-password/></form>
        </div>
    </div>
</div>
@endsection