@extends('inc.layout') @section('content')


<div id=content class=main-container>
    <div id=registration>
        <div class=page-heading>
            <div class=container>
                <h1 class="heading-title">Kampung Course</h1>
                <h3 class="heading-subtitle">Registrasi Lembaga</h3>
            </div>
        </div>
        <div class=container>
            <div id=agreement>
                <h2 class="agreement-title text-center">Term & Condition</h2>
                <div class="row row-eq-height agreement-box-wrapper">
                    <div class=col-md-4>
                        <div class="agreement-box text-center">
                            <i class="agreement-icon icon-approval"></i>
                            <h3 class="agreement-box-title">Persetujuan Akun Lembaga</h3>
                            <p>Setelah mengisi data dan membuat akun Lembaga di kampung course, segera konfirmasi ke admin@kampungcourse.co.id atau WhatsApp +62 857 9898 0858 untuk mempercepat peninjauan pendaftaran dan pengaktifan akun Lembaga. Akun Lembaga
                                akan dinonaktifkan atau dihapus di database apabila tidak pernah aktif dan update selama satu tahun.s</p>
                        </div>
                    </div>
                    <div class=col-md-4>
                        <div class="agreement-box text-center">
                            <i class="agreement-icon icon-free-account"></i>
                            <h3 class="agreement-box-title">Gratis Akun Lembaga</h3>
                            <p>Setiap Lembaga dapat bergabung di Kampung Course secara gratis. Isi data identitas Lembaga secara jelas dan benar. Setelah pendaftaran akun maka Lembaga akan mendapatkan surat melalui email mengenai kontrak perjanjian, syarat,
                                dan ketentuan secara lengkap untuk bergabung di Kampung Course.</p>
                        </div>
                    </div>
                    <div class=col-md-4>
                        <div class="agreement-box text-center">
                            <i class="agreement-icon icon-workshop"></i>
                            <h3 class="agreement-box-title">Unggah Program Kursus & Pelatihan</h3>
                            <p>Hanya akun Lembaga yang telah disetujui yang dapat mengunggah program kursus dan pelatihannya di Platform Kampung Course. Program kursus & pelatihan yang di unggah akan kami tinjau terlebih dulu sebelum kami tampilkan di Platform
                                Kampung Course. Hal tersebut untuk mengecek kebenaran informasi program yang di unggah.
                            </p>
                        </div>
                    </div>
                </div>
                <div class=row>
                    <div class=col-md-12>
                        <p class=text-center>Dengan menekan tombol setuju maka anda tunduk pada Persyaratan & Kondisi <br> yang disebutkan pada <a href=#>Persyaratan & Kondisi</a></p>
                    </div>
                </div>
                <footer class="agreement-footer text-center">
                    <a href=../index.html class="btn btn-default btn-disagreed">I Disagree</a>
                    <a href=#agree class="btn btn-primary btn-agreed">I Agree</a></footer>
            </div>
        <form action="{{route('merchant.register')}}" method="post" enctype="multipart/form-data" class=registration-form id=registration-form>
            @csrf
                <div class=row>
                    <div class="col-md-8 col-md-push-2">
                        <ul class=registration-nav>
                            <li class=active><a href=#business-information>Akun Lembaga</a></li>
                            <li><a href=#business-detail>Detail Lembaga</a></li>
                            <li><a href=#business-location>Lokasi Lembaga</a></li>
                            <li><a href=#verification>Verifikasi</a></li>
                            <li><a href=#done>Selesai</a></li>
                        </ul>
                        <div class="content-step active" id=business-information>
                            <header class="step-header text-center">
                                <h2 class="step-title">Akun Lembaga</h2>
                                <p class=step-description>
                                    Email yang anda masukkan adalah username lembaga anda dan tidak dapat diganti<br>Harap gunakan email institusi seperti : <br><em class=text-italic>marketing@namalembaga.com, event@namalembaga.com,
                                            namalembaga@gmail.com</em>
                            </header>
                            <div class=row>
                                <div class="col-md-8 col-md-push-2">
                                    <div class=form-group>
                                        <label>Email Lembaga</label>
                                        <input id=email name="email" type=email class="form-control check-educator-email"></div>
                                    <div class=form-group>
                                        <label>Password</label>
                                        <input id=password name="password" minlength="6" type=password class=form-control>
                                    </div>
                                    <div class=form-group>
                                        <label>Konfirmasi Password</label>
                                        <input id=password2 name="password2" type=password class=form-control>
                                    </div>
                                </div>
                            </div>
                            <footer class=step-footer>
                                <div class=row>
                                    <div class="col-md-8 col-md-push-2">
                                        <a href=#business-detail class="btn btn-primary text-uppercase btn-next btn-center">Next</a>
                                    </div>
                                </div>
                            </footer>
                        </div>
                        <div class=content-step id=business-detail>
                            <header class="step-header text-center">
                                <h2 class="step-title">Detail Lembaga</h2>
                                <p class=step-description>
                                    Informasi yang dimasukkan disini akan ditampilkan pada profil lembaga
                                    <p class=step-description>Upload banner & logo untuk lembaga anda<br> Ukuran banner (1920x420) pixels.<br>Ukuran logo yang direkomendasikan adalah (800 x 800) pixels.<br>(Hanya upload file jpg)
                                        <p></p>
                            </header>
                            <div class=business-profile-box>
                                <div class="profile-cover upload-wrapper form-group" data-identifier=cover data-return=id>
                                    <div class="upload-loading hidden">Uploading</div>
                                    <div class=upload-fields>
                                        <label for=profile_cover class=upload-btn>
                                                <span class=label-content><i class=icon-camera></i> Foto Cover</span>
                                            </label>
                                        <input type=hidden class=file_id name=cover value>
                                        <input type=file id=profile_cover class="upload-preview upload-input"></div>
                                </div>
                                <div class="profile-logo upload-wrapper form-group" data-identifier=logo data-return=id>
                                    <div class="upload-loading hidden">Uploading</div>
                                    <div class=upload-fields>
                                        <label for=profile_logo class=upload-btn>
                                                <span class=label-content><i class=icon-camera></i>
                                                    Logo/Foto</span>
                                            </label>
                                        <input type=hidden class="file_id validate" id=logo name=logo value>
                                        <input type=file id=profile_logo class="upload-preview upload-input"></div>
                                </div>
                            </div>
                            <div class=row>
                                <div class="col-md-8 col-md-push-2">
                                    <div class=form-group>
                                        <label>Nama Lembaga <small class=text-italic></small></label>
                                        <input type=text class=form-control name=name></div>
                                    <div class=form-group>
                                        <label>Nomor Telepon <small class=text-italic></small></label>
                                        <input type=text class=form-control placeholder data-type=phone name=phone id=admin_phone></div>
                                    <div class=form-group>
                                        <label>Tipe Lembaga</label>
                                        <select class="selectpicker form-control" data-live-search=true name=businessType>
                                                        <option disabled>Pilih Tipe Lembaga</option>
                                                        <option value="Lembaga">Lembaga</option>
                                                        <option value="Tutor">Tutor</option>
                                                    </select></div>
                                    <div class=form-group>
                                        <label>Kategori</label>
                                        <select class="selectpicker form-control" data-live-search=true name=categories>
                                                            <option disabled>Pilih Kategori</option>
                                                            <option value="Bahasa Inggris">Bahasa Inggris</option>
                                                            <option value="Bahasa Mandarin">Bahasa Mandarin</option>
                                                            <option value="Bahasa Arab">Bahasa Arab</option>
                                                        </select></div>
                                    <div class=form-group>
                                        <label>Deskripsi Profil <small class=text-italic>(Minimal 50 Kata, Maksimal 150 Kata)</small></label><textarea class=form-control cols=30 rows=5 name=description id=description></textarea></div>
                                    <div class=form-group>
                                        <label>Maps <small class=text-italic></small></label>
                                        <input type=url class=form-control name=maps></div>
                                    <div class=form-group>
                                        <label>Foto & Video Galeri <small class=text-italic></small></label>
                                        <input type=file class=form-control name=galery[] multiple></div>
                                </div>
                            </div>
                            <footer class=step-footer>
                                <div class=row>
                                    <div class="col-md-8 col-md-push-2">
                                        <a href=#business-information class="btn btn-default text-uppercase btn-previous">Previous</a>
                                        <a href=#business-location class="btn btn-primary text-uppercase btn-next">Next</a></div>
                                </div>
                            </footer>
                        </div>
                        <div class=content-step id=business-location>
                            <header class="step-header text-center">
                                <h2 class="step-title">Lokasi Lembaga</h2>
                                <p class=step-description>Masukkan Alamat Lengkap & Kode Pos Lembaga Serta Kota Lembaga</p>
                            </header>
                            <div class=row>
                                <div class="col-md-8 col-md-push-2">
                                    <div class=form-group>
                                        <label>Alamat Lengkap</label><textarea class=form-control cols=30 rows=5 name=address id=business_address></textarea>
                                    </div>
                                    <div class=form-group>
                                        <label>Kode Pos</label><input type="text" class=form-control name=postCode>
                                    </div>
                                    <div class=form-group>
                                        <label>Kota</label>
                                        <select class="selectpicker form-control" data-live-search=true name=city >
                                                <option disabled>Pilih Kota Lembaga</option>
                                                <option value="Malang">Malang</option>
                                                <option value="Kediri">Kediri</option>
                                            </select></div>
                                </div>
                            </div>
                            <footer class=step-footer>
                                <div class=row>
                                    <div class="col-md-8 col-md-push-2">
                                        <a href=#business-detail class="btn btn-default text-uppercase btn-previous">Previous</a>
                                        <a href=#verification class="btn btn-primary text-uppercase btn-next">Next</a></div>
                                </div>
                            </footer>
                        </div>
                        <div class=content-step id=verification>
                            <header class="step-header text-center">
                                <h2 class="step-title">Verifikasi</h2>
                                <p class=step-description>Upload KTP dan berkas pendukung sebagai bahan verifikasi team internal kami</p>
                            </header>
                            <div class=verification-wrapper>
                                    <h3 class="label-title">KTP</h3>
                                    <p class=label-description>Upload foto/scan KTP Anda. Format file yang diterima adalah jpg, gif, atau png.</p>
                                <div class="id-card upload-wrapper form-group" data-identifier=id-card data-return=url>
                                    <div class="upload-loading hidden">Uploading</div>
                                    <div class=upload-fields>
                                        <label for=ktp_passport class=upload-btn>
                                                <span class=label-content><i class=icon-camera></i> KTP</span>
                                            </label>
                                        <input type=hidden class="file_url validate" name=id_card_url id=id_card_url value>
                                        <input type=file class="upload-preview upload-input" id=ktp_passport></div>
                                </div>
                                <div class="row">
                                        <div class=form-group>
                                                <label>Berkas Pendukung (Opsional)<small class=text-italic>Contoh : Akta Lembaga, Prestasi</small></label>
                                                <input type=file class=form-control name=berkas[] multiple></div>
                                </div>
                            </div>
                            
                            <footer class=step-footer>
                                <div class=row>
                                    <div class="col-md-8 col-md-push-2">
                                        <a href=#business-location class="btn btn-default text-uppercase btn-previous">Previous</a>
                                        <a href=# class="submit-register btn btn-primary text-uppercase btn-next pull-right">Submit</a>
                                        <a href=#done class="btn btn-primary text-uppercase btn-next hidden"></a>
                                    </div>
                                </div>
                            </footer>
                        </div>
                        <div class=content-step id=done>
                            <header class="step-header text-center">
                                <h2 class="step-title">
                                    Terimakasih! <small>Registrasi Selesai <i
                                                class=icon-checkmark></i></small></h2>
                                <p class=step-description>
                                    Terimakasih telah meluangkan waktu anda untuk mengisi form pendaftaran lembaga. Data pendaftaran akan di review oleh team internal kami. Kita akan memberi balasan pada 3 hari kerja setelah registrasi dilakukan. Hubungi kami di
                                    <a href=mailto:admin@kampungcourse.id>admin@kampungcourse.id</a> jika anda memiliki pertanyaan.</p>
                            </header>
                            <div class="form-group text-center">
                                <a href="{{route('home')}}" class="btn btn-primary">Kembali ke beranda</a></div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection