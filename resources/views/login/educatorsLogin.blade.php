@extends('inc.layout')
@section('content')
<style>
        /* Style the tab */
    .tab {
      overflow: hidden;
      width: 100%;
      padding-bottom: 5%;
    }
    
    /* Style the buttons inside the tab */
    .tab button {
      background-color: inherit;
      float: left;
      border: none;
      outline: none;
      cursor: pointer;
      padding: 14px 16px;
      transition: 0.3s;
      font-size: 17px;
      width: 50%;
    }
    
    /* Change background color of buttons on hover */
    .tab button:hover {
      background-color: #ddd;
    }
    
    /* Create an active/current tablink class */
    .tab button.active {
        background-color: #f1f1f1;
    }
    
    /* Style the tab content */
    .tabcontent {
      display: none;
      padding: 6px 12px;
      border: 1px solid #ccc;
      border-top: none;
    }

    @media (max-width: 800px) {
        .login-header{
            margin-bottom:0% !important;
        }
    }

    </style>
<div id="content" class="main-container">

    <div id="login" style="background-image: url({{asset('assets/wp-content/themes/maubelajarapa/assets/images/vendor-bg.jpg')}})">
        <header class="login-header">
            <h1 class="header-title">Kampung Course</h1>
            <h3 class="header-subtitle">Login Lembaga</h3>
        </header>
        <div class="tab visible-xs">
                <button onclick="location.href = '{{ route('login') }}';" class="tablinks active">Pengguna</button>
                <button class="tablinks">Lembaga</button>
              </div>
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-push-1">


                    <div class="login-box">
                        <div class="box-wrapper">
                            <form method="post" action="{{ route('merchant-login') }}" class="login-form box-column">
                                @csrf
                                <div class="box-header">
                                    <h3 class="box-title">Login</h3>
                                </div>
                                @if ($errors->has('email'))
                                    <span style="color:red">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                                <div class="form-group">
                                    <input type="text" class="form-control has-icon" placeholder="Business Email" id="email" name="email" value="{{ old('email') }}" required>
                                    <i class="icon-email-m input-icon"></i>
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control has-icon" placeholder="Password" id="password" name="password" required>
                                    <i class="icon-lock input-icon"></i>
                                </div>
                                <div class="form-group text-right">
                                    <label class="checkbox-style pull-left"><input type="checkbox" name="rememberme" id="rememberme"  value="forever"> <span>Ingat Saya</span></label>
                                    <a href="{{ route('forgotPassword') }}" class="btn-forgot">Lupa Password</a>
                                </div>
                                <div class="form-group">
                                    <input type="hidden" id="woocommerce-login-nonce" name="woocommerce-login-nonce" value="e04e607d49" /><input type="hidden" name="_wp_http_referer" value="/login-as-educator/" /> <input type="hidden" name="login-type"
                                        value="educator">
                                    <input type="submit" class="btn btn-primary btn-submit" name="login" value="Login">
                                </div>
                                <a href="{{ route('login') }}" class="btn-forgot">Masuk Sebagai Pengguna</a>
                            </form>
                            <div class="box-column register-box">
                                <div class="box-header">
                                    <h3 class="box-title">Register</h3>
                                </div>
                                <p>Belum punya akun? ayo gabung di Kampung Course Indonesia! Kamu dapat mendaftarkan akun baru dengan menekan tombol register dibawah:</p>
                                <a href="{{ route('merchant-register') }}" class="btn btn-gray btn-default">Register</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection