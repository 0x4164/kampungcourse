@extends('inc.layout')
@section('content')

<style>
    /* Style the tab */
.tab {
  overflow: hidden;
  width: 100%;
  padding-bottom: 5%;
}

/* Style the buttons inside the tab */
.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
  font-size: 17px;
  width: 50%;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
    background-color: #f1f1f1;
}

/* Style the tab content */
.tabcontent {
  display: none;
  padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;
}

@media (max-width: 800px) {
        .login-header{
            margin-bottom:0% !important;
        }
    }
</style>

<div id="content" class="main-container">


    <div class="woocommerce">
        <div id="login" style="background-image: url({{asset('assets/wp-content/themes/maubelajarapa/assets/images/learner-bg.jpg')}})">
            <header class="login-header">
                <h1 class="header-title">Kampung Course</h1>
                <h3 class="header-subtitle">Login - Register</h3>
            </header>
            <div class="tab visible-xs">
                    <button class="tablinks">Pengguna</button>
                    <button onclick="location.href = '{{ route('merchant-login') }}';" class="tablinks active">Lembaga</button>
                  </div>
            <div class="container">
                    
                <div class="row">


                    <div class="col-md-10 col-md-push-1">


                        <div class="login-box">
                                
                            <div class="box-wrapper">
                                <form method="post" class="login-form box-column" action="{{ route('login') }}">
                                    @csrf
                                    
                                    <div class="box-header">
                                        <h3 class="box-title">Masuk Sebagai Pengguna</h3>
                                    </div>
                                    @error('email')
                                    <span style="color:red">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    <div class="form-group">
                                        <input type="email" class="form-control has-icon" placeholder="Email" id="username" name="email" value="{{ old('email') }}" required>
                                        <i class="icon-email-m input-icon"></i>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control has-icon" placeholder="Password" id="password" name="password" required>
                                        <i class="icon-lock input-icon"></i>
                                    </div>


                                    <div class="form-group text-right">
                                        <label class="checkbox-style pull-left"><input type="checkbox" name="remember" id="rememberme" {{ old('remember') ? 'checked' : '' }}> <span>Ingat Saya</span></label>
                                        <a href="{{ route('forgotPassword') }}" class="btn-forgot">Lupa Password?</a>
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-primary btn-submit" name="login" value="Login">
                                    </div>
                                    <a href="{{ route('merchant-login') }}" class="btn-forgot">Masuk Sebagai Lembaga</a>

                                </form>

                                <div class="box-column register-box">
                                    <div class="box-header">
                                        <h3 class="box-title">Registrasi Pengguna</h3>
                                    </div>
                                    <form method="post" action="{{ route('register') }}">
                                        @csrf
                                        @error('userid')
                                        <span style="color:red">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                        <div class="form-group">
                                            <input type="text" name="userid" id="userid" class="form-control" placeholder="ID Pengguna" value="{{ old('userid') }}" required>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="name" placeholder="Nama Lengkap" value="{{ old('name') }}">
                                        </div>
                                        <div class="form-group">
                                            <input type="tel" name="phone" id="phone" class="form-control" placeholder="Nomor HP" required value="{{ old('phone') }}">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="referenceId" id="referenceId" class="form-control" placeholder="ID Referensi" value="{{ old('referenceId') }}" required>
                                        </div>
                                        <div class="form-group">
                                            <input type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}" required>
                                        </div>
                                        <div class="form-group">
                                            <input type="password" name="password" id="password" class="form-control" placeholder="Password" required minlength="8">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" name="password_confirmation" id="password-confirm" class="form-control" placeholder="Konfirmasi Password" required>
                                        </div>
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-default btn-gray" name="register" value="Register">
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>


</div>
@endsection