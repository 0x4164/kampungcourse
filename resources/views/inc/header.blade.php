<?php use \App\Http\Controllers\NotificationController; ?>
<?php use \App\Http\Controllers\ChatController; ?>
<header class="header-floating-navbar hidden-xs">
    <a style="all:unset;" href="{{route('home')}}"><img src="{{asset('assets/wp-content/themes/maubelajarapa/assets/images/KampungCourseLogo.png')}}" alt="Kampung Course" width="8%" style="display: inline-block; margin-right:1%; margin-left:1%;"></a>
    <div class="dropdown" style="display:inline-block !important;"><a class="dropdown-toggle" data-toggle="dropdown">Kategori</a>
        <ul class="dropdown-menu dropdown-menu-right" style="width:max-content !important;">
            <li><a href="/category/filter?kategori=Bahasa+Inggris">Bahasa Inggris</a></li>
            <li><a href="/category/filter?kategori=Bahasa+Arab">Bahasa Arab</a></li>
            <li><a href="/category/filter?kategori=Bahasa+Mandarin">Bahasa Mandarin</a></li>
          </ul></div>
    <a href="/category/filter?">Kursus</a>
    <a href="{{route('aboutus')}}">Tentang Kami</a>
        <a href="{{route('blog')}}">Blog</a>
        <a href="{{route('merchant')}}">Lembaga</a>
    
    <div style="display:inline-block; float:right; width:35%; padding-bottom:1%; text-align:right; ">
    <form method="get" action="{{route('course.filter')}}">
    <input type="text" name="searchKey" id="searchKey" @if(isset($search)) value="{{$searchKey}}" @endif placeholder="Cari Kursus..." style="width:50%;"><a><i class="fas fa-search"></i></a>
    
    <a @if(Auth::check()) href="{{ route('wishlist')}}" @else href="{{route('login')}}" @endif><i class="far fa-heart"></i></a>
    <div class="dropdown" style="display:inline-block !important;"><a class="dropdown-toggle" @if(Auth::check()) data-toggle="dropdown" @elseif(Auth::guard('merchant')->check()) data-toggle="dropdown" @elseif(Auth::guard('admin')->check()) data-toggle="dropdown" @else href="{{route('login')}}" @endif ><i class="far fa-bell"></i></a>
        <ul class="dropdown-menu dropdown-menu-right" style="width:max-content !important; z-index:5 !important;">
            @if(session('success'))<li ><a href="#">@include('inc.message')</a></li>@endif
            <?php
            $notif = NotificationController::show(); 
            if($notif!=null){
              foreach($notif as $not){
                ?>
                <li ><a href="{{$not->link}}">{{$not->message}}</a></li>
                <?php
              }
            }else{
              ?>
                <li ><a href="#">Tidak Ada Notifikasi</a></li>
              <?php
            }
            ?>
          </ul></div>
    <a @if(Auth::check()) href="{{ route('chat')}}" @elseif(Auth::guard('merchant')->check()) href="{{ route('chat')}}" @else href="{{route('login')}}" @endif><i class="far fa-comments"><?php
      $chat = ChatController::read(); 
      if($chat!=null){
        if(count($chat)>0)
        {
          ?> <span style="position: absolute;
  top: 30px;
  right: 10px;
  padding: 5px 5px;
  border-radius: 50%;
  background: red;
  color: white;"></span><?php
        }
      }
      ?></i></a>
  </form>
    </div>
</header>
<header class="header-floating-navbar-small visible-xs">
    <a style="all:unset;" href="{{route('home')}}"><img src="{{asset('assets/wp-content/themes/maubelajarapa/assets/images/KampungCourseLogo2.png')}}" alt="Kampung Course" width="10%" style="display: inline-block; margin-left:2%; padding-top:1%;"></a>
    <div style="display:inline-block; float:right; padding-top:2%; width:88%; padding-bottom:1%; text-align:right; ">
    <form method="get" action="{{route('course.filter')}}">
    <input type="text" name="searchKey" id="searchBar" @if(isset($search)) value="{{$searchKey}}" @endif placeholder="Cari Kursus..." style="width:62%; margin-right:2%; line-height:17%;">
    <a @if(Auth::check()) href="{{ route('wishlist')}}" @else href="{{route('login')}}" @endif><i class="far fa-heart"></i></a>
    <div class="dropdown" style="display:inline-block !important;"><a class="dropdown-toggle" @if(Auth::check()) data-toggle="dropdown" @elseif(Auth::guard('merchant')->check()) data-toggle="dropdown" @elseif(Auth::guard('admin')->check()) data-toggle="dropdown" @else href="{{route('login')}}" @endif ><i class="far fa-bell"></i></a>
      <div style="position:fixed; z-index:10000000; display:block"><ul class="dropdown-menu dropdown-menu-right" style="width:max-content !important;">
          @if(session('success'))<li ><a href="#">@include('inc.message')</a></li>@endif
        <?php
            $notif = NotificationController::show(); 
            if($notif!=null){
              foreach($notif as $not){
                ?>
                <li ><a href="{{$not->link}}">{{$not->message}}</a></li>
                <?php
              }
            }else{
              ?>
                <li ><a href="#">Tidak Ada Notifikasi</a></li>
              <?php
            }
            ?>
        </ul></div></div>
        <a @if(Auth::check()) href="{{ route('chat')}}" @elseif(Auth::guard('merchant')->check()) href="{{ route('chat')}}" @else href="{{route('login')}}" @endif><i class="far fa-comments"> <?php
          $chat = ChatController::read(); 
          if($chat!=null){
            if(count($chat)>0)
            {
              ?> <span style="position: absolute;
      top: 30px;
      right: 10px;
      padding: 5px 5px;
      border-radius: 50%;
      background: red;
      color: white;"></span><?php
            }
          }
          ?>
          </i></a>
    </form>
    </div>
</header>
<script type="text/javascript">
var $j = jQuery.noConflict();
    $j(document).ready(function() {
      $j('#searchKey').keydown(function(e) {
        var sk = document.getElementById('searchKey').value;
        if (e.which === 13) {
            window.location = {!! json_encode(url('/')) !!} + '/search/' + sk;
        }
      });
      $j('#searchBar').keydown(function(e) {
        var sk = document.getElementById('searchBar').value;
        if (e.which === 13) {
            window.location = {!! json_encode(url('/')) !!} + '/search/' + sk;
        }
      });
    });
    </script>