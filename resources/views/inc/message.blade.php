@if(count($errors) > 0)
    @foreach($errors->all() as $error)
        <div class="btn btn-danger">
            {{$error}}
        </div>
    @endforeach
@endif

@if(session('success'))
    <div class="btn btn-success">
        {{session('success')}}
    </div>
@endif

@if(session('error'))
    <div class="btn btn-danger">
        {{session('error')}}
    </div>
@endif