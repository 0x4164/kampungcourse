<footer id="colophon" class="site-footer">
    <div class="container">
        <div class="footer-menu-wrapper">
            <div class="row">
                <div class="col-lg-4 col-md-5">
                    <a href="#" class="footer-brand"><img src="{{asset('assets/wp-content/themes/maubelajarapa/assets/images/KamungCourseLogoFooter.png')}}" alt="Kampung Course"></a>
                    <br>
                    <p class="brand-description">CV. Kampung Course Indonesia</p>
                    <p class="brand-description">Kampung Course Indonesia</p>
                    <p class="brand-description">171-008-204-8888</p>
                </div>
                <div class="col-lg-8 col-md-7">
                    <div class="row row-eq-height">
                        <div class="col-md-3 col-sm-6">
                            <h3 class="footer-menu-title">Kampung Course</h3>
                            <div class="menu-discover-container">
                                <ul id="menu-discover" class="footer-menu">
                                    <li id="menu-item-141755" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-141755"><a href="{{route('aboutus')}}">Tentang Kami</a></li>
                                    <li id="menu-item-154452" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-154452"><a href="#">Lowongan Kerja</a></li>
                                    <li id="menu-item-154453" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-154453"><a href="#">Penghargaan</a></li>
                                    <li id="menu-item-154454" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-154454"><a href="#">Persyaratan Kerjasama</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <h3 class="footer-menu-title">Layanan</h3>
                            <div class="menu-popular-categories-container">
                                <ul id="menu-popular-categories" class="footer-menu">
                                    <li id="menu-item-90039" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-90039"><a href="#">Jasa Penjemputan</a></li>
                                    <li id="menu-item-90039" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-90039"><a href="#">Konsultasi Kursus</a></li>
                                    <li id="menu-item-90039" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-90039"><a href="#">Cara Daftar</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <h3 class="footer-menu-title">Kategori</h3>
                            <div class="menu-terms-container">
                                <ul id="menu-terms" class="footer-menu">
                                    <li id="menu-item-90006" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-90006"><a href="{{route('merchant')}}">Pilih Lembaga Kursus</a></li>
                                    <li id="menu-item-154448" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-154448"><a href="{{route('course.index')}}">Pilih Program Kursus</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <h3 class="footer-menu-title">Social Media</h3>
                            <ul class="footer-social">
                                <li>
                                    <a href="https://www.instagram.com/"><img src="{{asset('assets/wp-content/themes/maubelajarapa/assets/images/icon-instagram.svg')}}" alt="Instagram"></a>
                                </li>
                                <li>
                                    <a href="https://www.facebook.com/"><img src="{{asset('assets/wp-content/themes/maubelajarapa/assets/images/icon-facebook.svg')}}" alt="Facebook"></a>
                                </li>
                                <li>
                                    <a href="https://wa.me/"><img src="{{asset('assets/wp-content/themes/maubelajarapa/assets/images/icon-whatsapp.png')}}" alt="WhatsApp"></a>
                                </li>
                                <li>
                                    <a href="https://line.me/"><img src="{{asset('assets/wp-content/themes/maubelajarapa/assets/images/icon-line.png')}}" alt="Line"></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="site-info">
            <div class="copyright-text">&copy; 2019 Kampung Course</div>
        </div>
    </div>
</footer>