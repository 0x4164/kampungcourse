<div class="bottom-floating-navbar visible-xs" style="text-align: center;">
    <a href="{{route('home')}}" id="homenavbar" ><i class="fas fa-home"><br><span style="font-size:50%;">Beranda<span></i></a>
    <a href="/category/filter?" id="filternavbar" ><i class="fab fa-leanpub"><br><span style="font-size:50%;">Kursus<span></i></a>
    <a href="{{route('merchant')}}" id="lembaganavbar"><i class="fas fa-university"><br><span style="font-size:50%;">Lembaga<span></i></a>
    <a id="transaksinavbar" @if(Auth::check()) href="{{ route('invoice')}}" @else href="{{route('login')}}" @endif ><i class="fas fa-shopping-cart"><br><span style="font-size:50%;">Transaksi<span></i></a>
    <a id="akunnavbar" @if(Auth::check()) href="{{ route('learner-dashboard')}}" @elseif(Auth::guard('merchant')->check()) href="{{route('merchantpage')}}" @elseif(Auth::guard('admin')->check()) href="{{route('adminpage')}}" @else href="{{route('login')}}" @endif ><i class="fas fa-user"><br><span style="font-size:50%;">Akun<span></i></a>
  </div> 

  <script type="text/javascript">
    if ( window.location.pathname == '/' ){
      document.getElementById('homenavbar').className += 'active';
    }
    if ( window.location.pathname == '/category/filter' ){
      document.getElementById('filternavbar').className += 'active';
    }
    if ( window.location.pathname == '/lembaga' ){
      document.getElementById('lembaganavbar').className += 'active';
    }
    if ( window.location.pathname == '/invoice' ){
      document.getElementById('transaksinavbar').className += 'active';
    }
    if ( window.location.pathname == '/account' ){
      document.getElementById('akunnavbar').className += 'active';
    }
</script> 