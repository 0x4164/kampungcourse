
<!DOCTYPE html>
<html>

<head>
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('assets/wp-content/themes/maubelajarapa/assets/favicon/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('assets/wp-content/themes/maubelajarapa/assets/favicon/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets/wp-content/themes/maubelajarapa/assets/favicon/favicon-16x16.png')}}">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{$title}} - Kampung Course</title>

    <script src="{{ asset('js/app.js') }}" defer></script>
    <link href="{{asset('assets/wp-content/themes/maubelajarapa/assets/css/all.css')}}" rel="stylesheet">
    <!--load all styles -->
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <link rel='stylesheet' id='vc_extensions_admin_cqaccordion-css' href="{{asset('assets/wp-content/plugins/vc-extensions-accordion/css/admin_iconcd04.css?ver=92142127d5348755609cd496c2b8552a')}}" type='text/css' media='all' />
    <link rel='stylesheet' id='smart-coupon-css' href="{{asset('assets/wp-content/plugins/woocommerce-smart-coupons/assets/css/smart-coupon.minf3df.css?ver=3.7.2')}}" type='text/css' media='all' />
    <link rel='stylesheet' id='wsl-widget-css' href="{{asset('assets/wp-content/mu-plugins/mba-project/plugin-dependencies/wordpress-social-login/assets/css/stylecd04.css?ver=92142127d5348755609cd496c2b8552a')}}" type='text/css' media='all' />
    <link rel='stylesheet' id='rs-plugin-settings-css' href="{{asset('assets/wp-content/plugins/revslider/public/assets/css/settings9d07.css?ver=5.4.6.4')}}" type='text/css' media='all' />
    <link rel='stylesheet' id='woocommerce-layout-css' href="{{asset('assets/wp-content/plugins/woocommerce/assets/css/woocommerce-layout2072.css?ver=3.2.5')}}" type='text/css' media='all' />
    <link rel='stylesheet' id='woocommerce-smallscreen-css' href="{{asset('assets/wp-content/plugins/woocommerce/assets/css/woocommerce-smallscreen2072.css?ver=3.2.5')}}" type='text/css' media='only screen and (max-width: 768px)' />
    <link rel='stylesheet' id='woocommerce-general-css' href="{{asset('assets/wp-content/plugins/woocommerce/assets/css/woocommerce2072.css?ver=3.2.5')}}" type='text/css' media='all' />
    <link rel='stylesheet' id='metorik-css-css' href="{{asset('assets/wp-content/plugins/metorik-helper/assets/css/metorik97de.css?ver=1.0.5')}}" type='text/css' media='all' />
    <link rel='stylesheet' id='dokan-style-css' href="{{asset('assets/wp-content/plugins/dokan-lite/assets/css/style.css')}}" type='text/css' media='all' />
    <link rel='stylesheet' id='dokan-fontawesome-css' href="{{asset('assets/wp-content/plugins/dokan-lite/assets/vendors/font-awesome/font-awesome.min.css')}}" type='text/css' media='all' />
    <link rel='stylesheet' id='algolia-autocomplete-css' href="{{asset('assets/wp-content/plugins/search-by-algolia-instant-relevant-results/css/algolia-autocompletef342.css?ver=2.9.3')}}" type='text/css' media='screen' />
    <link rel='stylesheet' id='js_composer_front-css' href="{{asset('assets/wp-content/plugins/js_composer/assets/css/js_composer.min5243.css?ver=5.4.5')}}" type='text/css' media='all' />
    <link rel='stylesheet' id='js_composer_custom_css-css' href="{{asset('assets/wp-content/uploads/js_composer/custom5243.css?ver=5.4.5')}}" type='text/css' media='all' />

    <link rel='stylesheet' id='mba-theme-font-css' href="{{asset('assets/wp-content/themes/maubelajarapa/assets/css/fonts5152.css?ver=1.0')}}" type='text/css' media='all' />
    <link rel='stylesheet' id='plyr-style-css' href="{{asset('assets/wp-content/themes/maubelajarapa/assets/css/plyr5152.css?ver=1.0')}}" type='text/css' media='all' />
    <link rel='stylesheet' id='mba-theme-css' href="{{asset('assets/wp-content/themes/maubelajarapa/style5152.css?ver=1.0')}}" type='text/css' media='all' />
    <link rel='stylesheet' id='mba-css' href="{{asset('assets/wp-content/mu-plugins/mba-project/assets/css/mba5152.css?ver=1.0')}}" type='text/css' media='all' />
    <link rel='stylesheet' id='icon-wc' href="{{asset('assets/wp-content/themes/maubelajarapa/assets/css/icon.css')}}" type='text/css' media='all' />
    <script type='text/javascript' src="{{asset('assets/wp-includes/js/jquery/jqueryb8ff.js?ver=1.12.4')}}"></script>
    <script type='text/javascript' src="{{asset('assets/wp-includes/js/jquery/jquery-migrate.min330a.js?ver=1.4.1')}}"></script>


    <script type='text/javascript' src="{{asset('assets/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.tools.min9d07.js?ver=5.4.6.4')}}"></script>
    <script type='text/javascript' src="{{asset('assets/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min9d07.js?ver=5.4.6.4')}}"></script>
    <script type='text/javascript' src="{{asset('assets/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min44fd.js?ver=2.70')}}"></script>
    <script type='text/javascript' src="{{asset('assets/wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min6b25.js?ver=2.1.4')}}"></script>

    <script type='text/javascript' src="{{asset('assets/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min2072.js?ver=3.2.5')}}"></script>



    <script type='text/javascript' src="{{asset('assets/wp-content/plugins/mailchimp-for-woocommerce/public/js/mailchimp-woocommerce-public.mine7f3.js?ver=2.1.7')}}"></script>
    <script type='text/javascript' src="{{asset('assets/wp-includes/js/underscore.min4511.js?ver=1.8.3')}}"></script>

    <script type='text/javascript' src="{{asset('assets/wp-includes/js/wp-util.mincd04.js?ver=92142127d5348755609cd496c2b8552a')}}"></script>
    <script type='text/javascript' src="{{asset('assets/wp-content/plugins/search-by-algolia-instant-relevant-results/js/algoliasearch/algoliasearch.jquery.minf342.js?ver=2.9.3')}}"></script>
    <script type='text/javascript' src="{{asset('assets/wp-content/plugins/search-by-algolia-instant-relevant-results/js/autocomplete.js/autocomplete.minf342.js?ver=2.9.3')}}"></script>
    <script type='text/javascript' src="{{asset('assets/wp-content/plugins/search-by-algolia-instant-relevant-results/js/autocomplete-noconflictf342.js?ver=2.9.3')}}"></script>
    <script type='text/javascript' src="{{asset('assets/wp-content/themes/maubelajarapa/assets/js/bootstrap.min7433.js?ver=3.3.7')}}"></script>
    <script type='text/javascript' src="{{asset('assets/wp-content/themes/maubelajarapa/assets/js/bootstrap-select5152.js?ver=1.0')}}"></script>
    <script type='text/javascript' src="{{asset('assets/wp-content/themes/maubelajarapa/assets/js/sms5152.js?ver=1.0')}}"></script>

    <script type='text/javascript' src="{{asset('assets/wp-content/plugins/woocommerce-additional-fees/js/wc_additional_feescd04.js?ver=92142127d5348755609cd496c2b8552a')}}"></script>
    <script type='text/javascript' src="{{asset('assets/wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min2072.js?ver=3.2.5')}}"></script>
    <script type='text/javascript' src="{{asset('assets/wp-content/plugins/mc4wp-premium/ecommerce3/assets/js/tracker.minc1f9.js?ver=4.4.2')}}"></script>
    <script type='text/javascript' src="{{asset('assets/wp-content/themes/maubelajarapa/assets/js/slick.min5152.js?ver=1.0')}}"></script>
    <script type='text/javascript' src="{{asset('assets/wp-content/themes/maubelajarapa/assets/js/modernizr-output5152.js?ver=1.0')}}"></script>
    <script type='text/javascript' src="{{asset('assets/wp-content/mu-plugins/mba-project/assets/js/jquery.validate.min92e2.js?ver=1.15.0')}}"></script>
    <script type='text/javascript' src="{{asset('assets/wp-content/themes/maubelajarapa/assets/js/cancel-zoom5152.js?ver=1.0')}}"></script>
    <script type='text/javascript' src="{{asset('assets/wp-content/themes/maubelajarapa/assets/js/plyr.min5152.js?ver=1.0')}}"></script>
    <script type='text/javascript' src="{{asset('assets/wp-content/themes/maubelajarapa/assets/js/apps5152.js?ver=1.0')}}"></script>
    <script type='text/javascript' src="{{asset('assets/wp-content/themes/maubelajarapa/assets/js/url5152.js?ver=1.0')}}"></script>
    <script type='text/javascript' src="{{asset('assets/wp-content/themes/maubelajarapa/assets/js/mba-theme5152.js?ver=1.0')}}"></script>
    <script type='text/javascript' src="{{asset('assets/wp-content/mu-plugins/mba-project/assets/js/jquery.popup.overlaye96c.js?ver=1.7.13')}}"></script>
    <script type='text/javascript' src="{{asset('assets/wp-content/mu-plugins/mba-project/assets/js/sweetalert29156.js?ver=7.3.0')}}"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>


</head>

<body class="home page-template page-template-page-templates page-template-homepage page-template-page-templateshomepage-php page page-id-141607 single-site woocommerce wpb-js-composer js-comp-ver-5.4.5 vc_responsive dokan-theme-maubelajarapa">

    <div id="page" class="site">


        @include('inc.header')
         @yield('content') @include('inc.navbar') @include('inc.footer')

        <script type="text/javascript">
            var APP_URL = {!! json_encode(url('/')) !!};
        </script>


</body>

</html>