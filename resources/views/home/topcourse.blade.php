<?php use \App\Http\Controllers\CourseController; ?>
<section class="page-section">
        <div class="section-header">
            <h2 class="section-title">Top Kursus</h2>
            <a href="{{route('course.index')}}" class="more" target="">See All <i class="icon icon-chevron-right"></i></a>
        </div>

        <div class="item-wrapper">
            <div class="item-scroll item-slider">

                <?php foreach ($tops as $topCourse) {
                    foreach ($topCourse->tops as $top) {
                     ?>
                    <?php $area = (array) json_decode($topCourse->thumbnail, true);
                                        $image = 'noimage.png';
                                        foreach($area as $v)
                                        {
                                            $image = $v;
                                        } ?>
                <div class="workshop">
                    <div class="workshop-img img-fit-wrapper">
                        <a href="/course/{{$topCourse->id}}">
                            <img width="250" height="250" src="<?php echo asset('/images') ?>/<?php echo $image?>" class="post-img wp-post-image" alt="circle-share" /> </a>
                    </div>
                    <div class="workshop-content">
                        <h3 class="workshop-title"><a href="/course/{{$topCourse->id}}">{{$topCourse->title}}</a></h3>
                        <span class="workshop-price">
                Rp&nbsp;{{ number_format($topCourse->price, 0, ",", ".").',-'}}							</span>
                        </span>
                        <?php $rating = CourseController::getCourseRating($topCourse->id); ?>
                        <div class="star-rating"><span style="width:<?php echo $rating/5 * 100 ?>%">Rated <strong class="rating"><?php echo $rating ?></strong> out of 5</span></div>
                                            
                                                
                                                
                        <span class="workshop-date">Jul 6, 2019</span>
                    </div>
                </div>
                <?php }}
                ?>

                

            </div>
        </div>
    </section>