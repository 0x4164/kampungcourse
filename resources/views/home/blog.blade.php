<section class="column-section">
        <div class="row">
            <div class="col-md-8">
                <div class="inspiration">
                    <div class="section-header">
                        <h2 class="section-title">Blog</h2>
                    </div>

                    @if(count($blog)==5)
                    <div class="post">
                        <a href="/showposts/<?php echo $blog[0]['id']?>" class="post-img img-fit-wrapper"><img width="950" height="850" src="<?php echo asset('/images') ?>/<?php echo $blog[0]['thumbnail']?>" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="{{$blog[0]['title']}}" /></a>
                        <div class="post-content">
                            <span class="post-category">Articles</span>
                            <h3 class="post-title"><a href="/showposts/<?php echo $blog[0]['id']?>">{{$blog[0]['title']}}</a></h3>
                            <p class="post-entries"><?php echo substr($blog[0]['body'], 0, 50); ?></p>
                            <ul class="post-meta">
                                <li class="post-author">{{$blog[0]['admin']['name']}}</li>
                                <li class="post-date">{{$blog[0]['created_at']}}</li>
                                <li class="post-comments">0</li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="post">
                                <a href="/showposts/<?php echo $blog[1]['id']?>" class="post-img img-fit-wrapper"><img width="150" height="94" src="<?php echo asset('/images') ?>/<?php echo $blog[1]['thumbnail']?>" class="attachment-150x100 size-150x100 wp-post-image" alt="{{$blog[1]['title']}}" /></a>
                                <div class="post-content">
                                    <h3 class="post-title"><a href="/showposts/<?php echo $blog[1]['id']?>">{{$blog[1]['title']}}</a></h3>
                                    <ul class="post-meta">
                                        <li class="post-date">{{$blog[1]['created_at']}}</li>
                                        <li class="post-comments">0</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="post">
                                <a href="/showposts/<?php echo $blog[2]['id']?>" class="post-img img-fit-wrapper"><img width="150" height="100" src="<?php echo asset('/images') ?>/<?php echo $blog[2]['thumbnail']?>" class="attachment-150x100 size-150x100 wp-post-image" alt="{{$blog[2]['title']}}"/></a>
                                <div class="post-content">
                                    <h3 class="post-title"><a href="/showposts/<?php echo $blog[2]['id']?>">{{$blog[2]['title']}}</a></h3>
                                    <ul class="post-meta">
                                        <li class="post-date">{{$blog[2]['created_at']}}</li>
                                        <li class="post-comments">0</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="post">
                                <a href="/showposts/<?php echo $blog[3]['id']?>" class="post-img img-fit-wrapper"><img width="150" height="100" src="<?php echo asset('/images') ?>/<?php echo $blog[3]['thumbnail']?>" class="attachment-150x100 size-150x100 wp-post-image" alt="{{$blog[3]['title']}}"/></a>
                                <div class="post-content">
                                    <h3 class="post-title"><a href="/showposts/<?php echo $blog[3]['id']?>">{{$blog[3]['title']}}</a></h3>
                                    <ul class="post-meta">
                                        <li class="post-date">{{$blog[3]['created_at']}}</li>
                                        <li class="post-comments">0</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="post">
                                <a href="/showposts/<?php echo $blog[4]['id']?>" class="post-img img-fit-wrapper"><img width="112" height="100" src="<?php echo asset('/images') ?>/<?php echo $blog[4]['thumbnail']?>" class="attachment-150x100 size-150x100 wp-post-image" alt="{{$blog[4]['title']}}"
                                    /></a>
                                <div class="post-content">
                                    <h3 class="post-title"><a href="/showposts/<?php echo $blog[4]['id']?>">{{$blog[4]['title']}}</a></h3>
                                    <ul class="post-meta">
                                        <li class="post-date">{{$blog[4]['created_at']}}</li>
                                        <li class="post-comments">0</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            @endif
            <div class="col-md-4">
                <div class="inspiration-quote">
                    <div class="section-header">
                        <h2 class="section-title">Inspiring People</h2>
                    </div>
                    <div class="quote-content">
                        <a href="#" class="quote-img img-fit-wrapper">
                            <img src="{{asset('assets/wp-content/uploads/2019/04/11111939/Jerome-Jonathan.jpg')}}">
                        </a>
                        <div class="quote-text">
                            <p>Hi Learners! Ingin tahu cerita salah satu Educator MBA yang berhasil menembus sebagai calligraphy dan watercolor artist?</p>
                            <p>Yuk, simak ceritanya selengkapnya <a href="#l">Click more here.</a></p>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </section>