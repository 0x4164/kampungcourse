@extends('inc.layout')

@section('content')
<div id="content" class="main-container">    
        @include('home.slider')
        @include('home.category')
        <div class="container">
            @include('home.flashsale')
            @include('home.topcourse')
            @include('home.recommendedcourse')
            @include('home.newestcourse')
            @include('home.whyus')
            @include('home.blog')
            
        </div>
    </div>
@endsection
