    <section class="center slider" style="height: 5%; padding-bottom:20px; padding-top:20px; background-color:#f9f9f9">
        
        <div>
                <a href="/category/filter?kategori=Bahasa+Inggris" style="color:gray; font-family:Avenir LT Std; font-size:10px; text-align:center;">
                    <img style="height:36px; display:inline-block; padding-bottom:5px" src="{{asset('assets/wp-content/uploads/2018/02/language.png')}}">
                    <br>
                    BAHASA INGGRIS
                </a>
            </div>
            <div>
                    <a href="/category/filter?kategori=Bahasa+Arab" style="color:gray; font-family:Avenir LT Std; font-size:10px; text-align:center;">
                        <img style="height:36px; display:inline-block; padding-bottom:5px" src="{{asset('assets/wp-content/uploads/2018/02/language.png')}}">
                        <br>
                        BAHASA ARAB
                    </a>
                </div>
                <div>
                        <a href="/category/filter?kategori=Bahasa+Mandarin" style="color:gray; font-family:Avenir LT Std; font-size:10px; text-align:center;">
                            <img style="height:36px; display:inline-block; padding-bottom:5px" src="{{asset('assets/wp-content/uploads/2018/02/language.png')}}">
                            <br>
                            BAHASA MANDARIN
                        </a>
                    </div>
                        
          </section>
    <script>
        var $j = jQuery.noConflict();
        $j(".center").slick({
            infinite: true,
            slidesToShow: 8,
            slidesToScroll: 1
      });
    </script>