<div class="vc_row wpb_row vc_row-fluid vc_custom_1493271099768">
        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="vc_column-inner ">
                <div class="wpb_wrapper">
                    <div class="wpb_text_column wpb_content_element ">
                        <div class="wpb_wrapper">
                            <h2 style="text-align: center; font-family: arial; color: #555555; font-weight: bold; font-size: 25px;">WHY CHOOSE US ?</h2>
                            <p style="text-align: center; font-family: arial; color: #555555; font-weight: bold;">Here's some reason why you have to choose us </p>
    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="vc_row wpb_row vc_row-fluid vc_custom_1493271821477">
        <div class="wpb_column vc_column_container vc_col-sm-3" style="margin-right:12%;">
            <div class="vc_column-inner vc_custom_1493272681230">
                <div class="wpb_wrapper">
                    <div class="wpb_single_image wpb_content_element vc_align_center">
    
                        <figure class="wpb_wrapper vc_figure">
                            <div class="vc_single_image-wrapper   vc_box_border_grey"><img class="vc_single_image-img " src="{{asset('assets/wp-content/uploads/2018/02/Educator01a-150x150.png')}}" width="150" height="150" alt="Educator01a" title="Educator01a" /></div>
                        </figure>
                    </div>
    
                    <div class="wpb_text_column wpb_content_element  vc_custom_1493273888973">
                        <div class="wpb_wrapper">
                            <h3 style="text-align: center; font-family: arial; font-weight: bold; font-size: 15px;">Pembayaran Aman & Terpercaya</h3>
    
                        </div>
                    </div>
                    <div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_50 vc_sep_border_width_2 vc_sep_pos_align_center vc_separator_no_text vc_sep_color_grey"><span class="vc_sep_holder vc_sep_holder_l"><span class="vc_sep_line"></span></span><span class="vc_sep_holder vc_sep_holder_r"><span class="vc_sep_line"></span></span>
                    </div>
                    <div class="wpb_text_column wpb_content_element ">
                        <div class="wpb_wrapper">
                            <p style="text-align: center; font-size: 17px; font-style: italic;">Sistem Pembayaran yang Aman dan Terpercaya dengan Rekening Atas Nama Perusahaan KAMPUNG COURSE INDONESIA</p>
    
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wpb_column vc_column_container vc_col-sm-3" style="margin-right:12%;">
            <div class="vc_column-inner vc_custom_1493273081478">
                <div class="wpb_wrapper">
                    <div class="wpb_single_image wpb_content_element vc_align_center">
    
                        <figure class="wpb_wrapper vc_figure">
                            <div class="vc_single_image-wrapper   vc_box_border_grey"><img class="vc_single_image-img " src="{{asset('assets/wp-content/uploads/2018/02/Educator03-150x150.png')}}" width="150" height="150" alt="Educator03" title="Educator03" /></div>
                        </figure>
                    </div>
    
                    <div class="wpb_text_column wpb_content_element ">
                        <div class="wpb_wrapper">
                            <h3 style="text-align: center; font-family: arial; font-weight: bold; font-size: 15px;">Lembaga Terverifikasi</h3>
    
                        </div>
                    </div>
                    <div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_50 vc_sep_border_width_2 vc_sep_pos_align_center vc_separator_no_text vc_sep_color_grey"><span class="vc_sep_holder vc_sep_holder_l"><span class="vc_sep_line"></span></span><span class="vc_sep_holder vc_sep_holder_r"><span class="vc_sep_line"></span></span>
                    </div>
                    <div class="wpb_text_column wpb_content_element ">
                        <div class="wpb_wrapper">
                            <p style="text-align: center; font-size: 17px; font-style: italic;">Terdapat berbagai pilihan Lembaga Kursus dengan spesialis yang berbeda satu sama lain dan telah terverifikasi oleh Kampung Course sehingga memudahkan peserta mendapatkan hasil, sesuai kebutuhannya.</p>
    
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wpb_column vc_column_container vc_col-sm-3" >
            <div class="vc_column-inner vc_custom_1493273088519">
                <div class="wpb_wrapper">
                    <div class="wpb_single_image wpb_content_element vc_align_center">
    
                        <figure class="wpb_wrapper vc_figure">
                            <div class="vc_single_image-wrapper   vc_box_border_grey"><img class="vc_single_image-img " src="{{asset('assets/wp-content/uploads/2018/02/Educator01-150x150.png')}}" width="150" height="150" alt="Educator02" title="Educator02" /></div>
                        </figure>
                    </div>
    
                    <div class="wpb_text_column wpb_content_element ">
                        <div class="wpb_wrapper">
                            <h3 style="text-align: center; font-family: arial; font-weight: bold; font-size: 15px;">Konsultasi Gratis</h3>
    
                        </div>
                    </div>
                    <div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_50 vc_sep_border_width_2 vc_sep_pos_align_center vc_separator_no_text vc_sep_color_grey"><span class="vc_sep_holder vc_sep_holder_l"><span class="vc_sep_line"></span></span><span class="vc_sep_holder vc_sep_holder_r"><span class="vc_sep_line"></span></span>
                    </div>
                    <div class="wpb_text_column wpb_content_element ">
                        <div class="wpb_wrapper">
                            <p style="text-align: center; font-size: 17px; font-style: italic;">Menyediakan Jasa Konsultasi seputar Lembaga & Program Kursus yang sesuai dengan kebutuhan peserta secara Gratis.</p>
    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="vc_row wpb_row vc_row-fluid vc_custom_1493271821477">
        <div class="wpb_column vc_column_container vc_col-sm-3" style="margin-right:12%;">
            <div class="vc_column-inner vc_custom_1493272681230">
                <div class="wpb_wrapper">
                    <div class="wpb_single_image wpb_content_element vc_align_center">
    
                        <figure class="wpb_wrapper vc_figure">
                            <div class="vc_single_image-wrapper   vc_box_border_grey"><img class="vc_single_image-img " src="{{asset('assets/wp-content/uploads/2018/02/Educator05-150x150.png')}}" width="150" height="150" alt="Educator01a" title="Educator01a" /></div>
                        </figure>
                    </div>
    
                    <div class="wpb_text_column wpb_content_element  vc_custom_1493273888973">
                        <div class="wpb_wrapper">
                            <h3 style="text-align: center; font-family: arial; font-weight: bold; font-size: 15px;">Reschedule Gratis</h3>
    
                        </div>
                    </div>
                    <div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_50 vc_sep_border_width_2 vc_sep_pos_align_center vc_separator_no_text vc_sep_color_grey"><span class="vc_sep_holder vc_sep_holder_l"><span class="vc_sep_line"></span></span><span class="vc_sep_holder vc_sep_holder_r"><span class="vc_sep_line"></span></span>
                    </div>
                    <div class="wpb_text_column wpb_content_element ">
                        <div class="wpb_wrapper">
                            <p style="text-align: center; font-size: 17px; font-style: italic;">Apabila kamu ternyata berhalangan mengikuti Periode Kursus yang telah kamu pilih, maka kamu bisa mengubahnya secara GRATIS.</p>
    
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wpb_column vc_column_container vc_col-sm-3" style="margin-right:12%;">
            <div class="vc_column-inner vc_custom_1493273081478">
                <div class="wpb_wrapper">
                    <div class="wpb_single_image wpb_content_element vc_align_center">
    
                        <figure class="wpb_wrapper vc_figure">
                            <div class="vc_single_image-wrapper   vc_box_border_grey"><img class="vc_single_image-img " src="{{asset('assets/wp-content/uploads/2018/02/Educator06-150x150.png')}}" width="150" height="150" alt="Educator03" title="Educator03" /></div>
                        </figure>
                    </div>
    
                    <div class="wpb_text_column wpb_content_element ">
                        <div class="wpb_wrapper">
                            <h3 style="text-align: center; font-family: arial; font-weight: bold; font-size: 15px;">Jasa Penjemputan</h3>
    
                        </div>
                    </div>
                    <div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_50 vc_sep_border_width_2 vc_sep_pos_align_center vc_separator_no_text vc_sep_color_grey"><span class="vc_sep_holder vc_sep_holder_l"><span class="vc_sep_line"></span></span><span class="vc_sep_holder vc_sep_holder_r"><span class="vc_sep_line"></span></span>
                    </div>
                    <div class="wpb_text_column wpb_content_element ">
                        <div class="wpb_wrapper">
                            <p style="text-align: center; font-size: 17px; font-style: italic;">Menyediakan Jasa Penjemputan bagi peserta : Bandara Juanda Surabaya Rp. 120.000/orang & Stasiun Kediri Rp. 80.000/orang</p>
    
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wpb_column vc_column_container vc_col-sm-3">
                <div class="vc_column-inner vc_custom_1493273095998">
                    <div class="wpb_wrapper">
                        <div class="wpb_single_image wpb_content_element vc_align_center">
        
                            <figure class="wpb_wrapper vc_figure">
                                <div class="vc_single_image-wrapper   vc_box_border_grey"><img class="vc_single_image-img " src="{{asset('assets/wp-content/uploads/2018/02/Educator04-150x150.png')}}" width="150" height="150" alt="Educator04" title="Educator04" /></div>
                            </figure>
                        </div>
        
                        <div class="wpb_text_column wpb_content_element ">
                            <div class="wpb_wrapper">
                                <h3 style="text-align: center; font-family: arial; font-weight: bold; font-size: 15px;">Pendaftaran & Booking Gratis</h3>
        
                            </div>
                        </div>
                        <div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_50 vc_sep_border_width_2 vc_sep_pos_align_center vc_separator_no_text vc_sep_color_grey"><span class="vc_sep_holder vc_sep_holder_l"><span class="vc_sep_line"></span></span><span class="vc_sep_holder vc_sep_holder_r"><span class="vc_sep_line"></span></span>
                        </div>
                        <div class="wpb_text_column wpb_content_element ">
                            <div class="wpb_wrapper">
                                <p style="text-align: center; font-size: 17px; font-style: italic;">Sistem Pendaftaran Online Gratis tanpa biaya administrasi, untuk booking kursus cukup membayar Uang Muka (DP) 20% dari biaya kursus, Pelunasan dapat dilakukan pada saat tiba di lembaga.</p>
        
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>