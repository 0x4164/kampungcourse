<?php use \App\Http\Controllers\CourseController; ?>
<section id="editor-pick" class="page-section categories">
        <div class="section-header">
            <h2 class="section-title">Flash Sale</h2>
            <!-- <a href="#" class="more">See all <i class="icon icon-chevron-right"></i></a> -->
        </div>
        <p id="demo"></p>
        <div class="item-wrapper">
            <div class="item-scroll item-carousel" id="flashsale">
                <?php foreach ($flashs as $flash) {
                    foreach ($flash->flashes as $course) {
                        $rating = CourseController::getCourseRating($course->course_id);
                     ?>
                    <?php $area = (array) json_decode($flash->thumbnail, true);
                                        $image = 'noimage.png';
                                        foreach($area as $v)
                                        {
                                            $image = $v;
                                        } ?>
                <a href="/course/{{$course->course_id}}" class="category-item"><img width="250" height="250" class="post-img wp-post-image" src="<?php echo asset('/images') ?>/<?php echo $image?>" alt=""><p style="color:black">{{$flash->title}}</p><div class="star-rating"><span style="width:<?php echo $rating/5 * 100 ?>%">Rated <strong class="rating"><?php echo $rating ?></strong> out of 5</span></div><p><del>Rp.{{ number_format($flash->price, 0, ",", ".").',-'}}</del> Rp.{{ number_format($course->flashPrice, 0, ",", ".").',-'}}</p></a>
                <?php }}
                ?>
            </div>
        </div>
    
    </section>
    <script>
        // Set the date we're counting down to
        var countDownDate = {{config('flashsale.flashdate')}};
        // Update the count down every 1 second
        var x = setInterval(function() {
    
            // Get today's date and time
            var now = new Date().getTime();
    
            // Find the distance between now and the count down date
            var distance = countDownDate - now;
    
            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
            // Display the result in the element with id="demo"
            document.getElementById("demo").innerHTML = days + "d " + hours + "h " +
                minutes + "m " + seconds + "s ";
    
            // If the count down is finished, write some text 
            if (distance < 0) {
                clearInterval(x);
                document.getElementById("demo").innerHTML = "EXPIRED";
                var flashsale = document.getElementsByClassName('category-item');
                var i;
                for (i = 0; i < flashsale.length; i++) {
                    flashsale[i].style.pointerEvents = "none";
                }
                document.getElementById('flashsale').style.opacity = 0.5;
            }
        }, 1000);
    </script>