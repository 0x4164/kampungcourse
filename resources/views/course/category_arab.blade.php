@extends('inc.layout') @section('content')

<div id=content class=main-container>
    <div class="container site-content">
        <div id=woocommerce-content role=main>
            <div id=directory-search class=main-search-wrapper>
                <div class=main-search>
                    <label class="visible-sm visible-xs text-center">Advanced Search</label>
                    <form method="post" action="{{route('course.filter')}}">
                            @csrf
                            <input type=hidden name=s value>
                            <div class=search-option>
                                <div class=form-group>
                                    <select class=mba-selectpicker name="kategori" title=Date>
                                        <option value="Semua Bahasa">Semua Bahasa</option>
                                        <option value="Bahasa Arab">Bahasa Arab</option>
                                        <option value="Bahasa Inggris" selected=selected >Bahasa Inggris</option>
                                        <option value="Bahasa Mandarin" >Bahasa Mandarin</option>
                                    </select>
                                </div>
                                <div class=form-group>
                                    <select name=lc class=mba-selectpicker data-live-search=true title=Location>
                                        <option value data-tokens=All>Semua Metode</option>
                                        <option data-tokens="Bekasi Barat" value=36 >Online</option>
                                        <option data-tokens="Bekasi Barat" value=36 >Offline</option>
                                    </select>
                                </div>
                                <div class=form-group>
                                    <select name=cat class=mba-selectpicker data-live-search=true data-size=7 title=Categories>
                                        <option value data-tokens=All>Semua Periode</option>
                                        <option value=art data-tokens=Art >1 Minggu</option>
                                        <option value=art data-tokens=Art >2 Minggu</option>
                                        <option value=calligraphy data-tokens=Calligraphy >1 Tahun</option>
                                    </select>
                                </div>
                                <div class=form-group>
                                    <div class=btn-group>
                                        <button type=button class="btn btn-default dropdown-toggle" data-toggle=dropdown>Semua Lokasi<span class=caret></span></button>
                                        <div class="dropdown-menu noclose manual_trigger_wrapper">
                                            <ul class=dropdown-menu-content>
                                                <li><label><input type=checkbox data-name=class_types class=manual-trigger value=workshop> Kediri</label></li>
                                                <li><label><input type=checkbox data-name=class_types class=manual-trigger value=seminar> Malang</label></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class=form-group>
                                    <select name=price class=mba-selectpicker title=Price>
                                        <option value=all >Semua Kursus</option>
                                        <option value=free >Promo</option>
                                        <option value=-250K >Diskon</option>
                                    </select>
                                </div>
                                <div class="form-group form-group-footer visible-sm visible-xs">
                                    <button class="btn btn-close btn-default"><i class="icon icon-close"></i> Close</button>
                                    <button class="btn btn-primary" type=submit><i class="icon icon-search"></i> Search</button></div>
                            </div>
                            <br><button class="btn btn-primary pull-right" type=submit><i class="icon icon-search"></i> Search</button><br><br>
                        </form>
                </div>
            </div>
            <section class=section-heading>
                <h3 class="heading-title pull-left">Kursus Kategori Bahasa Arab :</h3>
                <div class="heading-menu pull-right">
                    <a href=# class="btn visible-sm visible-xs btn-advanced-search btn-default"><i class="icon icon-gear"></i> Filter</a>
                    <form class="form-inline woocommerce-ordering" method=get>
                        <div class=form-group>
                            <label for=sortby>Sortir</label>
                            <select name=orderby class="selectpicker bs-select-update_filter_query orderby" title="Sort By">
                                <option value=menu_order >Terlama</option>
                                <option value=popularity >Terbaru</option>
                                <option value=rating >Popularitas</option>
                                <option value=date >Rating</option>
                                <option value=price >Harga: rendah ke tinggi</option>
                                <option value=price-desc >Harga: tinggi ke rendah</option>
                                <option value=startingsoon  selected=selected>Diskon</option>
                            </select>
                        </div>
                    </form>
                </div>
            </section>
            <section class="archive-box products">
                <div class="row workshops-container">
                        <p class="woocommerce-info">Belum Ada Kursus Dengan Kategori Bahasa Arab</p>
                </div>
            </section>
        </div>
    </div>
</div>
@endsection