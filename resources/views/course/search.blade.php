@extends('inc.layout') @section('content')
<?php use \App\Http\Controllers\CourseController; ?>
<div id=content class=main-container>
    <div class="container site-content">
        <div id=woocommerce-content role=main>
            <div id=directory-search class=main-search-wrapper>
                <div class=main-search>
                    <label class="visible-sm visible-xs text-center">Advanced Search</label>
                    <form method="get" action="{{route('course.filter')}}">
                            @csrf
                            <input type=hidden name=s value>
                            <div class=search-option>
                                <div class="col-md-2">
                                    <select class=mba-selectpicker name="kategori" style="width:100%">
                                        <option @if($kategori == 'Semua Bahasa') selected @endif value="Semua Bahasa">Semua Bahasa</option>
                                        <option @if($kategori == 'Bahasa Arab') selected @endif value="Bahasa Arab">Bahasa Arab</option>
                                        <option @if($kategori == 'Bahasa Inggris') selected @endif value="Bahasa Inggris">Bahasa Inggris</option>
                                        <option @if($kategori == 'Bahasa Mandarin') selected @endif value="Bahasa Mandarin" >Bahasa Mandarin</option>
                                    </select>
                                </div>
                                <div class=col-md-2>
                                    <select name="metode" class=mba-selectpicker style="width:100%">
                                        <option @if($metode == 'Semua Metode') selected @endif value="Semua Metode">Semua Metode</option>
                                        <option @if($metode == 'Online') selected @endif value="Online">Online</option>
                                        <option @if($metode == 'Offline') selected @endif value="Offline">Offline</option>
                                    </select>
                                </div>
                                <div class=col-md-2>
                                    <select name="periode" class=mba-selectpicker style="width:100%">
                                        <option @if($periode == 'Semua Periode') selected @endif value="Semua Periode" >Semua Periode</option>
                                        <option @if($periode == '1 Minggu') selected @endif value="1 Minggu" >1 Minggu</option>
                                        <option @if($periode == '2 Minggu') selected @endif value="2 Minggu" >2 Minggu</option>
                                        <option @if($periode == '1 Tahun') selected @endif value="1 Tahun" >1 Tahun</option>
                                    </select>
                                </div>
                                <div class=col-md-2>
                                    <select name="lembaga" class=mba-selectpicker style="width:100%">
                                        <option @if($periode == 'Semua Tipe Lembaga') selected @endif value="Semua Lembaga" >Semua Lembaga</option>
                                        <option @if($periode == 'Lembaga') selected @endif value="1 Minggu" >Lembaga</option>
                                        <option @if($periode == 'Tutor') selected @endif value="Tutor" >Tutor</option>
                                    </select>
                                </div>
                                <div class=col-md-2>
                                    <div class=btn-group style="width:100%">
                                        <button type=button class="btn btn-default dropdown-toggle" style="width:100%; text-align:left" data-toggle=dropdown>Lokasi<span class=caret></span></button>
                                        <div class="dropdown-menu noclose manual_trigger_wrapper">
                                            <ul class=dropdown-menu-content>
                                                <li><label><input onClick="toggle(this)" type=checkbox name="lokasi[]" class=manual-trigger value="Semua Lokasi"> Semua Lokasi</label></li>
                                                <li><label><input type=checkbox name="lokasi[]" class=manual-trigger value="Kediri"> Kediri</label></li>
                                                <li><label><input type=checkbox name="lokasi[]" class=manual-trigger value="Malang"> Malang</label></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class=col-md-2>
                                    <select name=price class=mba-selectpicker style="width:100%">
                                        <option value="Semua Kursus" >Semua Kursus</option>
                                        <option value="Promo" >Promo</option>
                                        <option value="Diskon" >Diskon</option>
                                    </select>
                                </div>
                                <div class="form-group form-group-footer visible-sm visible-xs">
                                    <button class="btn btn-close btn-default"><i class="icon icon-close"></i> Close</button>
                                    <button class="btn btn-primary" type=submit><i class="icon icon-search"></i> Search</button></div>
                            </div>
                            <br><button class="btn btn-primary pull-right" type=submit><i class="icon icon-search"></i> Search</button><br><br>
                            <input type="hidden" name="searchKey" @if(isset($searchKey)) value="{{$searchKey}}" @endif>
                        </form>
                </div>
            </div>
            <section class=section-heading>
                <h3 class="heading-title pull-left">Hasil Pencarian :</h3>
                <div class="heading-menu pull-right">
                    <a href=# class="btn visible-sm visible-xs btn-advanced-search btn-default"><i class="icon icon-gear"></i> Filter</a>
                    <form class="form-inline woocommerce-ordering" method=get>
                        <div class=form-group>
                            <label for=sortby>Sortir</label>
                            <select name=orderby class="selectpicker bs-select-update_filter_query orderby" title="Sort By">
                                <option value=menu_order >Terlama</option>
                                <option value=popularity >Terbaru</option>
                                <option value=rating >Popularitas</option>
                                <option value=date >Rating</option>
                                <option value=price >Harga: rendah ke tinggi</option>
                                <option value=price-desc >Harga: tinggi ke rendah</option>
                                <option value=startingsoon  selected=selected>Diskon</option>
                            </select>
                        </div>
                    </form>
                </div>
            </section>
            <section class="archive-box products">
                <div class="row workshops-container">
                    @if(count($courses)>0) @foreach ($courses as $course)

                    <div class="col-md-3 col-sm-6">
                        <div class="post course">
                            <div class=content-wrapper>
                            <a href="/course/{{$course->id}}" class=img-fit-wrapper>
                                    <?php $area = (array) json_decode($course->thumbnail, true);
                                        $image = 'noimage.png';
                                        foreach($area as $v)
                                        {
                                            $image = $v;
                                        } ?>
                                    <img width=279 height=250 src="<?php echo asset('/images') ?>/<?php echo $image?>" data-lazy-type=image data-src=https://cdn.maubelajarapa.com/wp-content/uploads/2018/10/31103821/pexels-photo-529922-950x850.jpeg class="lazy lazy-hidden img-lazy post-img wp-post-image"
                                        alt="{{$course->title}}"><noscript><img width=279 height=250 src="<?php echo asset('/images') ?>/<?php echo $image?>" class="post-img wp-post-image" alt="{{$course->title}}"></noscript>                                    </a>
                                <div class=post-content>
                                    <span class=course-category title=Coffee>{{$course->category}}</span>
                                    <a href="/course/{{$course->id}}" class=post-title>
                                        <strong>Kursus:</strong> {{$course->title}}</a>
                                    <div class=course-info>
                                        <strong class=course-price>
                                                <span class="woocommerce-Price-amount amount">
                                                    <span class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;{{ number_format($course->price, 0, ",", ".").',-'}}</span>		</strong>
                                        <strong class=time-exp> 14 hours left	</strong></div>
                                         <?php $rating = CourseController::getCourseRating($course->id); ?>
                        <div class="star-rating"><span style="width:<?php echo $rating/5 * 100 ?>%">Rated <strong class="rating"><?php echo $rating ?></strong> out of 5</span></div>
                                </div>
                                <div class=course-footer>
                                    <div class=course-footer-heading>
                                       
                                        <a href="/showLembaga/{{$course->merchant->id}}" class=course-vendor>
                                            <img src={{asset('images/'.$course->merchant->profileImage)}} alt="{{$course->merchant->name}}" class=vendor-img>
                                            <span>{{$course->merchant->name}}</span>
                                            <strong>{{$course->merchant->address}}</strong>
                                        </a>
                                        
                                        <div class=course-date>
                                            <span class=month>Aug</span>
                                            <strong class=date>27</strong></div>
                                            <a href=# class=show-other-dates>Add To Wishlist</a></div>
                                            <div class=course-other-dates>
                                                <p>Pengen Ikut Tapi Nggak Sempet Datang / Belum Punya Uang ? Tambahin Ke Wishlist Aja</p>
                                                <div class=button-box>
                                                <a name=add-to-cart value=167308 class="single_add_to_cart_button btn btn-primary" @if(Auth::check()) href="/addWishlist/{{$course->id}}" @else disabled="disabled" @endif>+ Wishlist</a>
                                                    <a href=# class="btn btn-default btn-cancel">Cancel</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach @else 
                    <p class="woocommerce-info">Tidak Ditemukan Kursus/Lembaga dengan kriteria pencarian yang dimasukkan.</p>
                    @endif

                </div>
            </section>
            @if(count($courses)>0)
            <div class="load-more-button-wrapper text-center">
                {{$courses->links()}}  
            </div>
            @endif
        </div>
    </div>
</div>
<script language="JavaScript">
    function toggle(source) {
  checkboxes = document.getElementsByName('foo');
  for(var i=0, n=checkboxes.length;i<n;i++) {
    checkboxes[i].checked = source.checked;
  }
}
    </script>
@endsection