@extends('inc.layout') @section('content')

<div id=content class=main-container>
    <div class="container site-content">
        <div id=woocommerce-content role=main>
            <div id=directory-search class=main-search-wrapper>
                <div class=main-search>
                    <label class="visible-sm visible-xs text-center">Advanced Search</label>
                    <form method="post" action="{{route('course.filter')}}">
                        @csrf
                        <input type=hidden name=s value>
                        <div class=search-option>
                            <div class=form-group>
                                <select class=mba-selectpicker name="kategori" title=Date>
                                        <option value="Semua Bahasa">Semua Bahasa</option>
                                        <option value="Bahasa Arab">Bahasa Arab</option>
                                        <option value="Bahasa Inggris" selected=selected >Bahasa Inggris</option>
                                        <option value="Bahasa Mandarin" >Bahasa Mandarin</option>
                                    </select>
                            </div>
                            <div class=form-group>
                                <select name=lc class=mba-selectpicker data-live-search=true title=Location>
                                        <option value data-tokens=All>Semua Metode</option>
                                        <option data-tokens="Bekasi Barat" value=36 >Online</option>
                                        <option data-tokens="Bekasi Barat" value=36 >Offline</option>
                                    </select>
                            </div>
                            <div class=form-group>
                                <select name=cat class=mba-selectpicker data-live-search=true data-size=7 title=Categories>
                                        <option value data-tokens=All>Semua Periode</option>
                                        <option value=art data-tokens=Art >1 Minggu</option>
                                        <option value=art data-tokens=Art >2 Minggu</option>
                                        <option value=calligraphy data-tokens=Calligraphy >1 Tahun</option>
                                    </select>
                            </div>
                            <div class=form-group>
                                <div class=btn-group>
                                    <button type=button class="btn btn-default dropdown-toggle" data-toggle=dropdown>Semua Lokasi<span class=caret></span></button>
                                    <div class="dropdown-menu noclose manual_trigger_wrapper">
                                        <ul class=dropdown-menu-content>
                                            <li><label><input type=checkbox data-name=class_types class=manual-trigger value=workshop> Kediri</label></li>
                                            <li><label><input type=checkbox data-name=class_types class=manual-trigger value=seminar> Malang</label></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class=form-group>
                                <select name=price class=mba-selectpicker title=Price>
                                        <option value=all >Semua Kursus</option>
                                        <option value=free >Promo</option>
                                        <option value=-250K >Diskon</option>
                                    </select>
                            </div>
                            <div class="form-group form-group-footer visible-sm visible-xs">
                                <button class="btn btn-close btn-default"><i class="icon icon-close"></i> Close</button>
                                <button class="btn btn-primary" type=submit><i class="icon icon-search"></i> Search</button></div>
                        </div>
                        <br><button class="btn btn-primary pull-right" type=submit><i class="icon icon-search"></i> Search</button><br><br>
                    </form>
                </div>
            </div>
            <section class=section-heading>
                <h3 class="heading-title pull-left">Hasil Pencarian :</h3>
                <div class="heading-menu pull-right">
                    <a href=# class="btn visible-sm visible-xs btn-advanced-search btn-default"><i class="icon icon-gear"></i> Filter</a>
                    <form class="form-inline woocommerce-ordering" method=get>
                        <div class=form-group>
                            <label for=sortby>Sortir</label>
                            <select name=orderby class="selectpicker bs-select-update_filter_query orderby" title="Sort By">
                                <option value=menu_order >Terlama</option>
                                <option value=popularity >Terbaru</option>
                                <option value=rating >Popularitas</option>
                                <option value=date >Rating</option>
                                <option value=price >Harga: rendah ke tinggi</option>
                                <option value=price-desc >Harga: tinggi ke rendah</option>
                                <option value=startingsoon  selected=selected>Diskon</option>
                            </select>
                        </div>
                    </form>
                </div>
            </section>
            <section class="archive-box products">
                <div class="row workshops-container">
                    @if(count($courses)>0) @foreach ($courses as $course)

                    <div class="col-md-3 col-sm-6">
                        <div class="post course">
                            <div class=content-wrapper>
                                <a href="/course/{{$course->id}}" class=img-fit-wrapper>
                                    <?php $area = (array) json_decode($course->thumbnail, true);
                                        $image = 'noimage.png';
                                        foreach($area as $v)
                                        {
                                            $image = $v;
                                        } ?>
                                    <img width=279 height=250 src="<?php echo asset('/images') ?>/<?php echo $image?>" data-lazy-type=image data-src=https://cdn.maubelajarapa.com/wp-content/uploads/2018/10/31103821/pexels-photo-529922-950x850.jpeg class="lazy lazy-hidden img-lazy post-img wp-post-image"
                                        alt="sensory coffee jakarta"><noscript><img width=279 height=250 src="<?php echo asset('/images') ?>/<?php echo $image?>" class="post-img wp-post-image" alt="sensory coffee jakarta"></noscript>                                    </a>
                                <div class=post-content>
                                    <span class=course-category title=Coffee>{{$course->category}}</span>
                                    <a href=culinary/beverage/coffee/learn-the-art-of-expresso-brewing/index.html class=post-title>
                                        <strong>Kursus:</strong> {{$course->title}}</a>
                                    <div class=course-info>
                                        <strong class=course-price>
                                                <span class="woocommerce-Price-amount amount">
                                                    <span class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;{{$course->price}}</span>		</strong>
                                        <strong class=time-exp> 14 hours left	</strong></div>
                                </div>
                                <div class=course-footer>
                                    <div class=course-footer-heading>
                                        <a href=../educator/halocoffee/index.html class=course-vendor>
                                            <img src=../../cdn.maubelajarapa.com/wp-content/uploads/2018/10/15151516/b37d5a1d-941b-eaec-3123-10b1b9c875ee_logo.jpg alt="Halo Coffee" class=vendor-img>
                                            <span>{{$course->merchant->name}}</span>
                                            <strong>{{$course->merchant->address}}</strong>
                                        </a>
                                        <div class=course-date>
                                            <span class=month>Aug</span>
                                            <strong class=date>27</strong></div>
                                        <a href=# class=show-other-dates>Add To Wishlist</a></div>
                                    <div class=course-other-dates>
                                        <p>Pengen Ikut Tapi Nggak Sempet Datang / Belum Punya Uang ? Tambahin Ke Wishlist Aja</p>
                                        <div class=button-box>
                                            <a name=add-to-cart value=167308 class="single_add_to_cart_button btn btn-primary" @if(Auth::check()) href="{{ route('wishlist')}}" @else disabled="disabled" @endif>+ Wishlist</a>
                                            <a href=# class="btn btn-default btn-cancel">Cancel</a></div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach @endif

                </div>
            </section>
            <div class="load-more-button-wrapper text-center">
                <img src=../wp-content/mu-plugins/mba-project/assets/images/loading.gif class="load-posts-loading hide" alt=Loading>
                <p><a class="mba-load-more button btn btn-primary" data-target=.workshops-container>Load more...</a></p>
            </div>
        </div>
    </div>
</div>
@endsection