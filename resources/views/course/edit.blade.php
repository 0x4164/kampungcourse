@extends('inc.layout')
@section('content')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<div id=content class=main-container>
    <a href="/merchant-course" class="btn btn-primary" style="margin-left:10%;">Kembali</a>
    <a onclick="document.getElementById('editForm').submit();" class="btn btn-primary pull-right" style="margin-right:10%;">Simpan</a>
    <hr>
    <div class="container site-content">
        <form id="editForm" method="post" action="/course/{{$course->id}}">
            <input type="hidden" name="_method" value="put" />
            @csrf
            <div id=single-workshop>
                <div class=row>
                    <div class=col-md-7>
                        <?php $images = json_decode($course->thumbnail, true);?>
                        <div id="myCarousel" class="carousel slide visible-xs" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                              <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                              <?php foreach ($images as $key => $value) { if($key!=0){ ?>
                                    <li data-target="#myCarousel" data-slide-to="{{$key}}"></li>
                                <?php }} ?>
                            </ol>
                        
                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                <?php foreach ($images as $key => $value) { ?>
                                    <div class="item <?php if ($key==0){echo 'active';}?>">
                                <img src="<?php echo asset('/images') ?>/<?php echo $value?>" alt="{{$key}}" style="width:100%;">
                              </div>
                                <?php } ?>
                              
                            </div>
                                
                                    <!-- Left and right controls -->
                                    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                      <span class="glyphicon glyphicon-chevron-left"></span>
                                      <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                      <span class="glyphicon glyphicon-chevron-right"></span>
                                      <span class="sr-only">Next</span>
                                    </a>
                                  </div>
    
    
                        <div class=post-header>
                            
                            <h1 style="font-weight:bold;" class="post-title"><textarea style="width:100%; height:auto; !important font-size:30px" name="title">{{$course->title}}</textarea></h1>
                            <ul class=post-meta>
                                <li class=post-category>
                                    <select name="category">
                                        <option @if($course->category=='Bahasa Inggris') selected @endif value="Bahasa Inggris">Bahasa Inggris</option>
                                        <option @if($course->category=='Bahasa Arab') selected @endif value="Bahasa Arab">Bahasa Arab</option>
                                        <option @if($course->category=='Bahasa Mandarin') selected @endif value="Bahasa Mandarin">Bahasa Mandarin</option>
                                    </select>
                                </li>
                            </ul>
                        </div>
                        <div class="widget-workshop-detail bg-gray pd-24 visible-sm visible-xs" data-update-schedule=details>
                            <div class=workshop-vendor>
                                <a href="/showLembaga/{{$course->merchant->id}}" class=vendor-img><img src={{asset('images/'.$course->merchant->profileImage)}} alt="{{$course->merchant->name}}"></a>
                                <i class=title-label>Hosted By</i>
                                <h4 class="vendor-title"><a href="/showLembaga/{{$course->merchant->id}}">{{$course->merchant->name}}</a></h4>
                                <a href="/showLembaga/{{$course->merchant->id}}" class=link>View other classes by vendor</a></div>
                            <ul class=workshop-detail>
                                <li class=workshop-teacher>
                                    <i class="icon-teachers icon"></i>
                                    <h4 class="list-title">Metode:</h4>
                                    <p class=list-content>
                                        <select name="method">
                                            <option @if($course->method=='Offline') selected @endif value="Offline">Offline</option>
                                            <option @if($course->method=='Online') selected @endif value="Online">Online</option>
                                        </select>
                                    </p>
                                </li>
                                <li class=workshop-location>
                                    <i class="icon-location-light icon"></i>
                                    <h4 class="list-title">Address:</h4>
                                    <div>
                                        <p class=list-content>
                                            {{$course->merchant->name}} <br> {{$course->address}}</p>
                                    <a href="{{$course->merchant->maps}}" target=_blank class=link>View Map</a></div>
                                </li>
                                <li class=workshop-class-size>
                                    <i class="icon-class-size icon"></i>
                                    <h4 class="list-title">Class Size:</h4>
                                    <p class=list-content>
                                        {{$course->capacity}}</p>
                                </li>
                                <li class=workshop-age-range>
                                    <i class="icon-teachers icon"></i>
                                    <h4 class="list-title">Jenis Lembaga:</h4>
                                    <p class=list-content>{{$course->merchant->businessType}}</p>
                                </li>
                                <li class=workshop-level>
                                    <i class="icon-workshop-level icon"></i>
                                    <h4 class="list-title">Level:</h4>
                                    <p class=list-content>{{$course->level}}</p>
                                </li>
                                <li class=workshop-language>
                                    <i class="icon-language icon"></i>
                                    <h4 class="list-title">Language:</h4>
                                    <p class=list-content>{{$course->category}}</p>
                                </li>
                            </ul>
                        </div>
                        <div class=post-intro>
                            <h2 class="section-title visible-sm visible-xs">Deskripsi Kursus</h2>
                            <textarea name="description" rows="5" style="width:100%;">{{$course->description}}</textarea>
                        </div>
                        <hr>
                        <section class="page-section learn">
                            <h2 class="section-title">Fasilitas</h2>
                            
                            <textarea name="facility" rows="5" style="width:100%;">{{$course->facility}}</textarea>
                        </section>
                        <hr>
                        <section class="page-section need-to-bring">
                            <h2 class="section-title">Persyaratan</h2>
                            <textarea name="terms" rows="5" style="width:100%;">{{$course->terms}}</textarea>
                        </section>
                        <hr>
                        <section class="page-section inclusive">
                            <h2 class="section-title">Jadwal & Materi</h2>
                            <textarea name="schedule" rows="5" style="width:100%;">{{$course->schedule}}</textarea>
                        </section>
                            
                        
                    <section class="page-section teacher">
                        <h2 class="section-title">Deskripsi Lembaga</h2>
                        <ul class=teacher-profile>
                            <li>
                                <div class="teacher-photo img-fit-wrapper">
                                    <img src={{asset('images/'.$course->merchant->profileImage)}} alt="Halo Coffee"></div>
                                <strong>{{$course->merchant->name}}, {{$course->merchant->categories}}</strong>
                                <p>{{$course->merchant->description}}
                                </p>
                            </li>
                        </ul>
                    </section>
                    
                    
                    
            </div>
            <div class=col-md-5>
                <div class=content-sidebar>
                    <div id="myCarousel" class="carousel slide visible-xl visible-md" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                          <?php foreach ($images as $key => $value) { if($key!=0){ ?>
                                <li data-target="#myCarousel" data-slide-to="{{$key}}"></li>
                            <?php }} ?>
                        </ol>
                    
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            <?php foreach ($images as $key => $value) { ?>
                                <div class="item <?php if ($key==0){echo 'active';}?>">
                            <img src="<?php echo asset('/images') ?>/<?php echo $value?>" alt="{{$key}}" style="width:100%;">
                          </div>
                            <?php } ?>
                          
                        </div>
                            
                                <!-- Left and right controls -->
                                <a class="left carousel-control" href="#myCarouselmd" data-slide="prev">
                                  <span class="glyphicon glyphicon-chevron-left"></span>
                                  <span class="sr-only">Previous</span>
                                </a>
                                <a class="right carousel-control" href="#myCarouselmd" data-slide="next">
                                  <span class="glyphicon glyphicon-chevron-right"></span>
                                  <span class="sr-only">Next</span>
                                </a>
                              </div>
                    <div class="widget-workshop-detail bg-gray pd-24 visible-lg visible-md" data-update-schedule=details>
                        <div class=workshop-vendor>
                            <a href="/showLembaga/{{$course->merchant->id}}" class=vendor-img><img src={{asset('images/'.$course->merchant->profileImage)}} alt="{{$course->merchant->name}}"></a>
                            <i class=title-label>Hosted By</i>
                            <h4 class="vendor-title"><a href="/showLembaga/{{$course->merchant->id}}">{{$course->merchant->name}}</a></h4>
                            <a href="/showLembaga/{{$course->merchant->id}}" class=link>Tampilkan kelas lain dari lembaga</a></div>
                        <ul class=workshop-detail>
                            <li class=workshop-teacher>
                                <i class="icon-teachers icon"></i>
                                <h4 class="list-title">Teachers:</h4>
                                <p class=list-content>{{$course->merchant->name}}</p>
                            </li>
                            <li class=workshop-location>
                                <i class="icon-location-light icon"></i>
                                <h4 class="list-title">Address:</h4>
                                <div>
                                    <p class=list-content>
                                        {{$course->merchant->name}} <br> {{$course->address}}</p>
                                        <a href="{{$course->merchant->maps}}" target=_blank class=link>View Map</a></div>
                            </li>
                            <li class=workshop-class-size>
                                <i class="icon-class-size icon"></i>
                                <h4 class="list-title">Kapasitas Kursus:</h4>
                                <p class=list-content>
                                    {{$course->capacity}}</p>
                            </li>
                            <li class=workshop-age-range>
                                <i class="icon-age-range icon"></i>
                                <h4 class="list-title">Minimum Age:</h4>
                                <p class=list-content>15 +</p>
                            </li>
                            <li class=workshop-level>
                                <i class="icon-workshop-level icon"></i>
                                <h4 class="list-title">Level:</h4>
                                <p class=list-content>{{$course->level}}</p>
                            </li>
                            <li class=workshop-language>
                                <i class="icon-language icon"></i>
                                <h4 class="list-title">Language:</h4>
                                <p class=list-content>Bahasa Indonesia & English</p>
                            </li>
                        </ul>
                    </div>
                    <div class="widget-register bg-gray visible-lg visible-md" data-update-schedule=price>
                            <h3 class="workshop-price widget-title">
                                <span><span
    class="woocommerce-Price-amount amount"><span
    class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;{{$course->price}}</span>
                                </span>
                            </h3>
                            
                                    
                </div>
            </div>
        </div>
    </div>
    
    </div>
</form>
    </div>

</div>
@endsection