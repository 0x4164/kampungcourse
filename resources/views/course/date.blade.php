@extends('inc.layout') @section('content')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" rel="stylesheet"/>
<div id="content" class="main-container">
    <div class="woocommerce">
        <div id="customer-account">
            <div class="container">
                <div class="customer-account-wrapper">
                    <nav class="woocommerce-MyAccount-navigation">
                        <ul>
                            
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards is-active"><a href="{{ route('merchant_course') }}">Kembali Ke Daftar Kursus</a></li>
                        </ul>
                    </nav>
                    <div class="woocommerce-MyAccount-content">
                        @include('inc.message')
                        <br><button onclick="document.getElementById('add').style.display='block'" class="btn">Tambah Tanggal Kursus</button>
                        <br><br>
                        {!! Form::open(['action' => 'CourseController@addCourseDate', 'method' => 'POST', 'enctype' => 'multipart/form-data', 'id' => 'add', 'style' => 'display: none;']) !!}
                                <hr>
                                <button type="button" onclick="document.getElementById('add').style.display='none'" class="btn pull-right">x</button><br>
                                <div class="form-group">
                                    {{Form::label('start', 'Tanggal Mulai Belajar :')}}
                                    {!! Form::text('date', '', array('id' => 'datepicker')) !!}
                                    
                                </div>
                                <input type="hidden" name="courseId" value="{{$courseId}}">
                                {{Form::submit('Submit', ['class'=>'btn btn-primary datepicker'])}}
                            {!! Form::close() !!}
                            <br>
                        @if(!count($courseDates)>0)
                        <h4>Belum Ada Tanggal Kursus</h4>
                        @else
                        <table class="woocommerce-orders-table woocommerce-MyAccount-orders shop_table shop_table_responsive my_account_orders account-orders-table">
                            <thead>
                                <tr>
                                    <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-date"><span class="nobr">Tanggal Mulai</span></th>
                                    <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-actions"><span class="nobr">Actions</span></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($courseDates as $date)
                                    <tr>
                                        <td><h6>{{$date->start_date}}</h6></td>
                                        <td><button class="danger">Delete</button></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @endif
                        
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
  <script>
  var $j = jQuery.noConflict();
$j("#datepicker").datepicker({
    multidate: true,
    format: 'yyyy-mm-dd'
});
  </script>
@endsection