@extends('inc.layout')
@section('content')
<?php use \App\Http\Controllers\CourseController; ?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>


<div id=content class=main-container>
    <div class="container site-content">
        <div id=single-workshop>
            <div class=row>
                <div class=col-md-7>
<?php $images = json_decode($course->thumbnail, true);?>
                        <div id="myCarousel" class="carousel slide visible-xs" data-ride="carousel">
                                <!-- Indicators -->
                                <ol class="carousel-indicators">
                                  <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                  <?php foreach ($images as $key => $value) { if($key!=0){ ?>
                                        <li data-target="#myCarousel" data-slide-to="{{$key}}"></li>
                                    <?php }} ?>
                                </ol>
                            
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner">
                                    <?php foreach ($images as $key => $value) { ?>
                                        <div class="item <?php if ($key==0){echo 'active';}?>">
                                    <img src="<?php echo asset('/images') ?>/<?php echo $value?>" alt="{{$key}}" style="width:100%;">
                                  </div>
                                    <?php } ?>
                                  
                                </div>
                            
                                <!-- Left and right controls -->
                                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                  <span class="glyphicon glyphicon-chevron-left"></span>
                                  <span class="sr-only">Previous</span>
                                </a>
                                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                  <span class="glyphicon glyphicon-chevron-right"></span>
                                  <span class="sr-only">Next</span>
                                </a>
                              </div>


                    <div class=post-header>
                        
                        <h1 style="font-weight:bold;" class="post-title">{{$course->title}}</h1>
                        <ul class=post-meta>
                            <li class=post-category>
                                <a href=product-category/culinary/beverage/coffee/index.html rel=tag>{{$course->category}}</a></li>
                                <?php $rating = CourseController::getCourseRating($course->id); ?>
                        <div class="star-rating"><span style="width:<?php echo $rating/5 * 100 ?>%">Rated <strong class="rating"><?php echo $rating ?></strong> out of 5</span></div>
                        </ul>
                    </div>
                    <div class="widget-workshop-detail bg-gray pd-24 visible-sm visible-xs" data-update-schedule=details>
                        <div class=workshop-vendor>
                            <a href="/showLembaga/{{$course->merchant->id}}" class=vendor-img><img src={{asset('images/'.$course->merchant->profileImage)}} alt="English Full Time"></a>
                            <i class=title-label>Hosted By</i>
                            <h4 class="vendor-title"><a href="/showLembaga/{{$course->merchant->id}}">{{$course->merchant->name}}</a></h4>
                            <a href="/showLembaga/{{$course->merchant->id}}" class=link>View other classes by vendor</a></div>
                        <ul class=workshop-detail>
                            <li class=workshop-teacher>
                                <i class="icon-teachers icon"></i>
                                <h4 class="list-title">Metode:</h4>
                                <p class=list-content>{{$course->method}}</p>
                            </li>
                            <li class=workshop-date>
                                <i class="icon-start-date icon"></i>
                                <h4 class="list-title">Start Date:</h4>
                                <p class=list-content>
                                    Friday, 30 Aug 2019 <br> 09.00 AM - 12.00 PM</p>
                                <a href=#other-dates class=link>View other dates</a></li>
                            <li class=workshop-location>
                                <i class="icon-location-light icon"></i>
                                <h4 class="list-title">Address:</h4>
                                <div>
                                    <p class=list-content>
                                        {{$course->merchant->name}} <br> {{$course->address}}</p>
                                    <a href="{{$course->merchant->maps}}" target=_blank class=link>View Map</a></div>
                            </li>
                            <li class=workshop-class-size>
                                <i class="icon-class-size icon"></i>
                                <h4 class="list-title">Class Size:</h4>
                                <p class=list-content>
                                    {{$course->capacity}}</p>
                            </li>
                            <li class=workshop-age-range>
                                <i class="icon-teachers icon"></i>
                                <h4 class="list-title">Jenis Lembaga:</h4>
                                <p class=list-content>{{$course->merchant->businessType}}</p>
                            </li>
                            <li class=workshop-level>
                                <i class="icon-workshop-level icon"></i>
                                <h4 class="list-title">Level:</h4>
                                <p class=list-content>{{$course->level}}</p>
                            </li>
                            <li class=workshop-language>
                                <i class="icon-language icon"></i>
                                <h4 class="list-title">Language:</h4>
                                <p class=list-content>{{$course->category}}</p>
                            </li>
                        </ul>
                    </div>
                    <div class=post-intro>
                        <h2 class="section-title visible-sm visible-xs">Deskripsi Kursus</h2>
                        {{$course->description}}
                    </div>
                    <hr>
                    <section class="page-section learn">
                        <h2 class="section-title">Fasilitas</h2>
                        {{$course->facility}}
                    </section>
                    <hr>
                <section class="page-section need-to-bring">
                    <h2 class="section-title">Persyaratan</h2>
                    <p>{{$course->terms}}</p>
                </section>
                <hr>
                <section class="page-section inclusive">
                    <h2 class="section-title">Jadwal & Materi</h2>
                    <p>{{$course->schedule}}</p>
                </section>
                    <hr>
                    <section id=other-dates class="page-section schedule">
                        <h2 class="section-title">Schedule &amp; other dates</h2>
                        <form action=#>
                            <ul class=schedule-table>
                                <li class=table-header>
                                    <ul class=row-header>
                                        <li>Start Date</li>
                                    </ul>
                                </li>
                                <div class=table-content>
                                    <?php foreach($course->courseDate as $date){ ?>
                                    <li class="date-item">
                                        <ul class=row-header>
                                            <li>{{date('Y F d', strtotime($date->start_date))}}</li>
                                        </ul>
                                    </li>
                                    <?php } ?>
                                </div>
                            </ul>
                        </form>
                    </section>
                    <div class="visible-sm visible-xs register-mobile-wrapper">
                        <div class="widget-register bg-gray" data-update-schedule=price>
                            <form class=cart action="/registerCourse" method=post>
                                @csrf
                                <h3 class="workshop-price widget-title">
                                    <span><span
class="woocommerce-Price-amount amount"><span
class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;{{$course->price}}</span>
                                    </span>
                                </h3>
                            <script>
                                function currencyFormatDE(num) {
                                    return (
                                        num
                                        .toFixed(2) // always two decimal digits
                                        .replace('.', ',') // replace decimal point character with ,
                                        .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
                                    ) // use . as a separator
                                    }
                            </script>    
                                <div class=quantity-box>
                                    <div class=form-row>
                                        <label>Jumlah</label>
                                        <input type="hidden" name="courseId" value="{{$course->id}}">
                                        <input id="quantity" name="quantity" onchange="var v=document.getElementById('quantity').value; var totals =v*{{$course->price}}; document.getElementById('totals').innerHTML=currencyFormatDE(totals)+',-'" type=number class="workshop-purhase-qty form-control disable-on-update-schedule" min=1 max="{{$course->capacity}}" value=1></div>
                                    <div class="form-row total-price-row">
                                        <label class=label-total>Total</label>
                                        <strong class=price-total><span
class="woocommerce-Price-amount amount"><span
class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;<span id="totals">{{$course->price}}</span></span></strong></div>
                                </div>
                                
                        </span><br>
                        <dl class=rightpress_live_product_price style="display: none;"><dt><span
class=label></span></dt>
                            <dd><span class=price></span></dd>
                        </dl>
                         <button type=submit class=" btn btn-primary btn-lg">Registrasi Sekarang</button></form>
                    </div>
                </div>
                <hr>
                <section class="page-section teacher">
                    <h2 class="section-title">Deskripsi Lembaga</h2>
                    <ul class=teacher-profile>
                        <li>
                            <div class="teacher-photo img-fit-wrapper">
                                <img src={{asset('images/'.$course->merchant->profileImage)}} alt="{{$course->merchant->name}}"></div>
                            <strong>{{$course->merchant->name}}, {{$course->merchant->categories}}</strong>
                            <p>{{$course->merchant->description}}
                            </p>
                        </li>
                    </ul>
                </section>
                
                
                <div data-update-schedule=map>
                    <hr>
                    <section class=page-section>
                        <h2 class="section-title">Map</h2>
                        <div class=map-responsive>
                            <iframe width=600 height=450 frameborder=0 style=border:0 src="https://www.google.com/maps/embed/v1/place?key=AIzaSyCUqsN9CPTYAAZDyBFuS2OhqfcNutt5EC8&#038;q=-6.12353100,106.79167300" allowfullscreen>
</iframe></div>
                        <a target=_blank class=map-direction href="{{$course->merchant->maps}}">Open in Maps</a></section>
                </div>
                <div class="visible-sm visible-xs subscribe-mobile-wrapper">
                    <div class="widget-subscribe bg-outline pd-48 mg-bottom-24">
                        <h2 class="widget-title">Tertarik?</h2>
                        <p class=mg-bottom-24>Tapi tidak bisa ikut kursus sekarang? Masukkan kursus kedalam wishlist dengan menekan tombol tambah ke wishlist dibawah dan registrasi dikemudian hari!</p>
                        <a href="/addWishlist/{{$course->id}}" class="open-subscribe-workshop btn btn-secondary btn-lg mg-bottom-24">Tambah ke wishlist</a>
                    </div>
                </div>
                <hr>
                <div class=widget-default>
                    <h2 class="widget-title">FAQ</h2>
                    <ul class=reset>
                        <li>
                            <p><b>Q:</b> Can I get my refund back if I am not able to make it last minute?</p>
                            <p><b>A:</b> No.</p>
                        </li>
                        <li>
                            <p><b>Q:</b> Can I get my friend to take over if I am not able to make it last minute?</p>
                            <p><b>A:</b> Yes. However, please kindly inform us for us to make any necessary arrangement.</p>
                        </li>
                    </ul>
                </div>
                <hr>
                <div class=widget-default>
                    <h2 class="widget-title">TERMS & CONDITIONS</h2>
                    <p>All payments are non-refundable and are not transferrable to other workshops but you are allowed to transfer your ticket to other person. Please kindly inform us so that we can make any necessary arrangement.</p>
                    <p>In the event if the class is cancelled due to some unexpected events by the educator, we will refund your payment fully.</p>
                    <p>In the event if the class information such as the price/time/location/learning description is amended by the educator/facilitator, you can have the option to ask for refund.</p>
                </div>
                <hr>
                
        </div>
        <div class=col-md-5>
            <div class=content-sidebar>
                <div id=workshop-featured-image-wrapper-170000 class="post-img-wrapper gallery-slider visible-lg visible-md">
                    <?php foreach ($images as $key => $value) { ?>
                        <div class=gallery-item>
                            <img width=600 height=600 src="<?php echo asset('/images') ?>/<?php echo $value?>" class="post-img img-fit-wrapper img-fill" alt="{{$key}}" >
                        </div>
                    <?php } ?>

                </div>
                <div class="widget-workshop-detail bg-gray pd-24 visible-lg visible-md" data-update-schedule=details>
                    <div class=workshop-vendor>
                        <a href="/showLembaga/{{$course->merchant->id}}" class=vendor-img><img src={{asset('images/'.$course->merchant->profileImage)}} alt="{{$course->merchant->name}}"></a>
                        <i class=title-label>Hosted By</i>
                        <h4 class="vendor-title"><a href="/showLembaga/{{$course->merchant->id}}">{{$course->merchant->name}}</a></h4>
                        <a href="/showLembaga/{{$course->merchant->id}}" class=link>Tampilkan kelas lain dari lembaga</a></div>
                    <ul class=workshop-detail>
                        <li class=workshop-teacher>
                            <i class="icon-teachers icon"></i>
                            <h4 class="list-title">Teachers:</h4>
                            <p class=list-content>{{$course->merchant->name}}</p>
                        </li>
                        <li class=workshop-date>
                            <i class="icon-start-date icon"></i>
                            <h4 class="list-title">Start Date:</h4>
                            <p class=list-content>
                                Friday, 30 Aug 2019 <br> 09.00 AM - 12.00 PM</p>
                            <a href=#other-dates class=link>View other dates</a></li>
                        <li class=workshop-location>
                            <i class="icon-location-light icon"></i>
                            <h4 class="list-title">Address:</h4>
                            <div>
                                <p class=list-content>
                                        {{$course->merchant->name}} <br> {{$course->address}}</p>
                                        <a href="{{$course->merchant->maps}}" target=_blank class=link>View Map</a></div>
                        </li>
                        <li class=workshop-class-size>
                            <i class="icon-class-size icon"></i>
                            <h4 class="list-title">Kapasitas Kursus:</h4>
                            <p class=list-content>
                                {{$course->capacity}}</p>
                        </li>
                        <li class=workshop-age-range>
                            <i class="icon-teachers icon"></i>
                            <h4 class="list-title">Jenis Lembaga:</h4>
                            <p class=list-content>{{$course->merchant->businessType}}</p>
                        </li>
                        <li class=workshop-level>
                            <i class="icon-workshop-level icon"></i>
                            <h4 class="list-title">Level:</h4>
                            <p class=list-content>{{$course->level}}</p>
                        </li>
                        <li class=workshop-language>
                            <i class="icon-language icon"></i>
                            <h4 class="list-title">Language:</h4>
                            <p class=list-content>{{$course->category}}</p>
                        </li>
                    </ul>
                </div>
                <div class="widget-register bg-gray visible-lg visible-md" data-update-schedule=price>
                    <form class=cart method=post action="/registerCourse">
                        @csrf
                        <h3 class="workshop-price widget-title">
                            <span><span
class="woocommerce-Price-amount amount"><span
class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;{{ number_format($course->price, 0, ",", ".").',-'}}</span>
                            </span>
                        </h3>
                        <div class=quantity-box>
                            <div class=form-row>
                                <label>Jumlah</label>
                                <input type="hidden" name="courseId" value="{{$course->id}}">
                                <input id="course_quantity" name="quantity" onchange="var v=document.getElementById('course_quantity').value; var totals =v*{{$course->price}}; document.getElementById('total_price').innerHTML=currencyFormatDE(totals)+',-'" type=number class="workshop-purhase-qty form-control disable-on-update-schedule" min=1 max="{{$course->capacity}}" value=1></div>
                            <div class="form-row total-price-row">
                                <label class=label-total>Total</label>
                                <strong class=price-total><span
class="woocommerce-Price-amount amount"><span
class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;<span id="total_price">{{ number_format($course->price, 0, ",", ".").',-'}}<span></span></strong></div>
                        </div>
                </span><br>
                <dl class=rightpress_live_product_price style="display: none;"><dt><span
class=label></span></dt>
                    <dd><span class=price></span></dd>
                </dl>
                 <button type="submit" class="single_add_to_cart_button btn btn-primary btn-lg">Registrasi Sekarang</button></form>
            </div>
            <div class="widget-subscribe bg-outline pd-48 mg-bottom-24 visible-lg visible-md">
                <h2 class="widget-title">Tertarik?</h2>
                <p class=mg-bottom-24>Tapi tidak bisa ikut kursus sekarang? Masukkan kursus kedalam wishlist dengan menekan tombol Tambah ke wishlist dibawah dan registrasi dikemudian hari!</p>
                <a href=# class="open-subscribe-workshop btn btn-secondary btn-lg mg-bottom-24">Tambah ke wishlist</a>
                
            </div>
            <div id=reviews class=widget-testimony>
                <div class="mg-bottom-24 text-center">
                    <span class="icon-quote mg-bottom-24"></span>
                    <h2 class="widget-title">Testimonials</h2>
                </div>
                <div class="vendor-review">
                <ul id="reviews" class="review-list">
                    @if(count($testimonies)>0)
                @foreach ($testimonies as $testimony)
                    @foreach ($testimony->invoices as $item)
                    <?php
                        $user = CourseController::getUser($item->user_id); 
                        ?>


                        <li>
                            <div class="review-profile">
                                <img src="https://secure.gravatar.com/avatar/c2e848388f348ab016d7417c4e1baf60?s=96&amp;d=mm&amp;r=g" alt="{{$user->name}}">
                                <span class="review-name">{{$user->name}}</span>
                            </div>
                            <div class="review-content">
                                <p class="review-entries">{{$item->testimony}}</p>
                            </div>
                        </li>
                    @endforeach
                @endforeach
                @else
                <p class=woocommerce-noreviews>Belum ada review</p>
                @endif
            </ul>
                    </div>
                <div class=testimony-footer></div>
            </div>
        </div>
    </div>
</div>

</div>
</div>
</div>
@endsection