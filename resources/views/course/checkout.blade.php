@extends('inc.layout') @section('content')

<div class="container site-content">
    <div class="container">

        <div class="woocommerce">
        <h2>Kursus : {{$course->title}}</h2>
        <h5>Tanggal Mulai : {{$course->start_date}}</h5><hr>


            <div class="woocommerce-order">

                <form name="checkout" method="post" class="checkout woocommerce-checkout" action="{{route('regist.success')}}" enctype="multipart/form-data">

                    @csrf

                    <div id="customer_details">
                        <div class="row">
                            <div class="col-md-6">
                                <h1 class="order-page-title">Form Pendaftaran</h1>


                                <div class="row workshop-detail">
                                    <div class="col-sm-6">
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="workshop-time">
                                        </div>
                                    </div>
                                </div>


                                <div class=" workshop-detail">
                                    <div class="woocommerce-billing-fields">

                                        <div class="woocommerce-billing-fields__field-wrapper">
                                            <p class="form-row form-row-wide form-group validate-required" id="billing_first_name_field" data-priority="10"><label for="billing_first_name" class="">Nama Lengkap <abbr class="required" title="required">*</abbr></label><input type="text" class="input-text form-control" name="name" id="billing_first_name"
                                                    placeholder="Nama Lengkap" autocomplete="given-name" autofocus="autofocus"></p>
                                            <p class="form-row form-row-wide form-group validate-required" id="customer_birthday_field" data-priority="20"><label for="customer_birthday" class="">Tanggal Lahir <abbr class="required" title="required">*</abbr></label><input type="text" class="input-text datepicker hasDatepicker" name="customer_birthday" id="customer_birthday"
                                                    placeholder="YYYY-MM-DD" autocomplete="birthday"></p>
                                            <p class="form-row form-row-wide form-group validate-required"><label for="idPengguna" class="">ID Pengguna <abbr class="required" title="required">*</abbr></label><input type="text" class="input-text form-control" name="idPengguna" id="idPengguna"
                                                    placeholder="ID Pengguna" autofocus="autofocus"></p>        
                                            <p class="form-row form-row-wide form-group validate-required validate-email" id="billing_email_field" data-priority="20"><label for="billing_email" class="">Email <abbr class="required" title="required">*</abbr></label><input type="email" class="input-text form-control" name="email" required="required" id="billing_email" placeholder="Email"
                                                    autocomplete="email username" autofocus="autofocus"></p>
                                            <p class="form-row form-row-wide hiddenx validate-required validate-phone" id="billing_phone_field" data-priority="30"><label for="billing_phone" class="">No HP <abbr class="required" title="required">*</abbr></label>
                                                
                                                    <input type="text" class="input-text form-control" name="phone" id="billing_phone" placeholder="0812-345-678" autocomplete="off">
                                            </p>
                                            <p class="form-row form-row-wide form-group" id="billing_company_field" data-priority="30"><label for="billing_company" class="">Institusi</label><input type="text" class="input-text form-control" name="billing_company" id="billing_company" placeholder="Institusi" autocomplete="organization"></p>
                                            <p class="form-row form-row-wide form-group address-field" id="billing_address_1_field" data-priority="60"><label for="billing_address_1" class="">Alamat</label><textarea name="address" class="input-text form-control" id="billing_address_1" placeholder="House number and street name" rows="2"
                                                    cols="5" autocomplete="address-line1"></textarea></p>
                                            <p class="form-row form-row-wide form-group address-field" ><label for="periode" class="">Periode</label>
                                                <select name="periode" class="form-control">
                                                        <option>1 Minggu</option>
                                                        <option>2 Minggu</option>
                                                        <option>1 Tahun</option>
                                                    </select></p>
                                            
                                            
                                        </div>

                                        <div class="participant-detail-fields">
                                            <h3>Detail Peserta</h3>
                                            <div class="participant-detail-fields__field-wrapper">
                                                    @for ($i = 1; $i <= $quantity; $i++)
                                                    <h3>Peserta {{$i}}</h3>
                                                    <div class="participant-detail-fields__item">
                                                        <p class="form-row form-row-wide validate-required"><label class="">Nama Lengkap <abbr class="required" title="required">*</abbr></label><input type="text" class="input-text form-control" name="nama_peserta{{$i}}"
                                                                placeholder="Nama Lengkap"></p>
                                                            <div class="col-sm-4">
                                                                <p class="form-row form-row-wide validate-required" ><label for="participant_detail_1_email" class="">No HP <abbr class="required" title="required">*</abbr></label><input type="text" class="input-text form-control" required="required" name="phone_peserta{{$i}}"
                                                                    placeholder="0812-345-678"></p>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <p class="form-row form-row-wide validate-required" id="participant_detail_1_email_field" data-priority=""><label for="participant_detail_1_email" class="">Email <abbr class="required" title="required">*</abbr></label><input required="required" type="email" class="input-text form-control" name="participant_email{{$i}}"
                                                                        id="participant_detail_1_email" placeholder="Email" ></p>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                    <p class="form-row form-row-wide validate-required" id="participant_detail_1_gender_field" data-priority=""><label for="participant_detail_1_gemder" class="">Gender <abbr class="required" title="required">*</abbr></label><select required="required" class="input-text form-control" name="participant_gender{{$i}}"
                                                                            id="participant_gender_1_gender"><option value="Laki Laki">Laki Laki</option><option value="Perempuan">Perempuan</option></select></p>
                                                            </div>
                                                        </div>
                                                    @endfor
                                                </div>
                                                
                                                
                                            </div>
                                        </div>
                                        <?php $randCode = rand(100,999);?>
                                        <input type="hidden" name="randCode" value="{{$randCode}}">
                                        <input type="hidden" name="merchantId" value="{{$course->merchant->id}}">
                                        <input type="hidden" name="courseId" value="{{$course->id}}">
                                        <input type="hidden" name="periode" value="{{$course->duration}}">
                                        <input type="hidden" name="quantity" value="{{$quantity}}">
                                        <input type="hidden" name="startDate" value="{{$course->start_date}}">
                                        <input type="hidden" name="total" value="{{($course->price*$quantity)+$randCode}}">
                                        <textarea name="message" hidden>
                                            <h1>Terimakasih Telah Mendaftar Kursus Di Kampung Course, Berikut Invoice Pembayaran Kursus Yang Telah Anda Pesan :</h1> 
                                            <table cellspacing="0" style="border: 2px solid #09abd7; width: 100%;"> 
                                                <tr> 
                                                    <th>Lembaga : </th><td> {{$course->merchant->name}}</td> 
                                                </tr> 
                                                <tr> 
                                                    <th>Alamat Lembaga : </th><td> {{$course->merchant->address}}</td> 
                                                </tr> 
                                                <tr> 
                                                    <th>CP Lembaga : </th><td> {{$course->merchant->phone}}</td> 
                                                </tr> 
                                                <tr> 
                                                    <th>Kursus : </th><td> {{$course->title}}</td> 
                                                </tr> 
                                                <tr style="background-color: #e0e0e0;"> 
                                                    <th>Kategori : </th><td> {{$course->category}}</td> 
                                                </tr> 
                                                <tr> 
                                                    <th>Metode : </th><td> {{$course->method}}</td> 
                                                </tr> 
                                                <tr style="background-color: #e0e0e0;"> 
                                                    <th>Durasi : </th><td> {{$course->duration}}</td> 
                                                </tr> 
                                                <tr> 
                                                    <th>Tanggal Mulai : </th><td> {{$course->start_date}}</td> 
                                                </tr> 
                                                <tr style="background-color: #e0e0e0;"> 
                                                    <th>Total Harga : </th><td> Rp. {{$course->price*$quantity}},-</td> 
                                                </tr> 
                                                <tr> 
                                                    <th>Total Yang Harus Di Transfer : </th><td> Rp. {{($course->price*$quantity)+$randCode}},-</td> 
                                                </tr> 
                                                <tr style="background-color: #e0e0e0;"> 
                                                    <th colspan="2">Silahkan Menyelesaikan Pembayaran DP melalui Rekening pembayaran berikut</th> 
                                                </tr> 
                                                <tr style="background-color: #e0e0e0;"> 
                                                    <th colspan="2">Mandiri 171-008-204-8888 A.N Kampung Course Indonesia</th> 
                                                </tr> 
                                                <tr style="background-color: #e0e0e0;"> 
                                                    <th colspan="2">BNI 0347624129 A.N Danang Pamungkas</th> 
                                                </tr> 
                                                <tr style="background-color: #e0e0e0;"> 
                                                    <th colspan="2">Mandiri Syariah 7119812137 A.N Danang Pamungkas</th> 
                                                </tr> 
                                            </table> 
                                        </textarea>
                                        <textarea name="mailto" hidden>
                                                <h1>Terimakasih Telah Mendaftar Kursus Di Kampung Course, Berikut Informasi Kursus Yang Telah Dipesan :</h1> 
                                                <table cellspacing="0" style="border: 2px solid #09abd7; width: 100%;"> 
                                                        <tr> 
                                                                <th>Lembaga : </th><td> {{$course->merchant->name}}</td> 
                                                            </tr> 
                                                            <tr> 
                                                                <th>Alamat Lembaga : </th><td> {{$course->merchant->address}}</td> 
                                                            </tr> 
                                                            <tr> 
                                                                <th>CP Lembaga : </th><td> {{$course->merchant->phone}}</td> 
                                                            </tr> 
                                                    <tr> 
                                                        <th>Kursus : </th><td> {{$course->title}}</td> 
                                                    </tr> 
                                                    <tr style="background-color: #e0e0e0;"> 
                                                        <th>Kategori : </th><td> {{$course->category}}</td> 
                                                    </tr> 
                                                    <tr> 
                                                        <th>Metode : </th><td> {{$course->method}}</td> 
                                                    </tr> 
                                                    <tr style="background-color: #e0e0e0;"> 
                                                        <th>Durasi : </th><td> {{$course->duration}}</td> 
                                                    </tr> 
                                                    <tr> 
                                                        <th>Tanggal Mulai : </th><td> {{$course->start_date}}</td> 
                                                    </tr> 
                                                </table> 
                                            </textarea>
                                            <textarea name="sms" hidden>
                                                    Terimakasih telah mendaftar kursus di Kampung Course. Kursus yang telah dipesan adalah {{$course->title}} yang akan dimulai pada tanggal {{$course->start_date}}. Silahkan cek email anda untuk informasi lebih lanjut.
                                                </textarea>
                                        <p class="form-row form-row-wide create-account"> <button type="submit" class="btn btn-primary">Submit</button></label>
                                        </p>
                                        
                                    </div>

                                </div>

                            </div>


                        </div>
                    </div>



                </form>

            </div>

        </div>

    </div>

</div>

@endsection