@extends('inc.layout') @section('content')

<div class="container site-content">
    <section id=single-post class=page-section>
        <div class=row>
            <div class="col-md-10 col-md-push-1">
                <div class=single-post-wrapper>
                    <div class="share-box-wrapper share-box-sticky visible-lg visible-md">
                        <ul class=share-box style=z-index:1;>
                            <li>
                                <a href=# class=btn-bookmark title="Bookmark this post" data-toggle=tooltip>
                                    <i class="icon icon-bookmark-o"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.facebook.com/dialog/share?app_id=1588093121509231&amp;display=page&amp;href=https://maubelajarapa.com/9-kebiasaan-yang-harus-dihilangkan-di-tahun-2019/&amp;redirect_uri=https://maubelajarapa.com/9-kebiasaan-yang-harus-dihilangkan-di-tahun-2019"
                                    class="btn-facebook popup-share" title="Share to Facebook" data-toggle=tooltip>
                                    <i class="icon icon-facebook"></i>
                                </a>
                                <div class="facebook-share-count share-count">0</div>
                            </li>
                            <li>
                                <a href="https://twitter.com/intent/tweet?url=https://maubelajarapa.com/9-kebiasaan-yang-harus-dihilangkan-di-tahun-2019/&amp;text=Check%20out%20this%20workshop%209%20Kebiasaan%20Buruk%20yang%20Harus%20Dihilangkan%20di%20Tahun%202019%20here%20https://maubelajarapa.com/9-kebiasaan-yang-harus-dihilangkan-di-tahun-2019/&amp;via=Maubelajarapa&amp;hashtags=#maubelajarapa#workshop"
                                    class="btn-twitter popup-share" title="Share to Twitter" data-toggle=tooltip>
                                    <i class="icon icon-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.linkedin.com/shareArticle?url=https://maubelajarapa.com/9-kebiasaan-yang-harus-dihilangkan-di-tahun-2019/&amp;title=Check%20out%20this%20workshop%209%20Kebiasaan%20Buruk%20yang%20Harus%20Dihilangkan%20di%20Tahun%202019%20here%20https://maubelajarapa.com/9-kebiasaan-yang-harus-dihilangkan-di-tahun-2019/"
                                    class="btn-linkedin popup-share" title="Share to Linkedin" data-toggle=tooltip>
                                    <i class="icon icon-linkedin"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <article id=post-177072 class="post single-post">
                        <div class=post-header>
                            <h1 class="post-title">{{$post->title}}</h1>
                            <ul class=post-meta>
                                <li class=post-viewed><span class=pageviews-placeholder data-key=177072></span></li>
                                <li class=post-date>{{$post->created_at}}</li>
                                <li class=post-category>On {{$post->tags}}</li>
                            </ul>
                        </div>
                        <div class="post-img img-fit-wrapper">
                            <img width=1947 height=1095 src="<?php echo asset('/images') ?>/{{$post->thumbnail}}" data-lazy-type=image data-src=https://cdn.maubelajarapa.com/wp-content/uploads/2019/01/21093008/photo-1534829178390-5312a631a68e.jpg class="lazy lazy-hidden img-lazy attachment-full size-full wp-post-image"
                                alt="tips biar sukses" data-lazy-srcset="https://cdn.maubelajarapa.com/wp-content/uploads/2019/01/21093008/photo-1534829178390-5312a631a68e.jpg 1947w, https://cdn.maubelajarapa.com/wp-content/uploads/2019/01/21093008/photo-1534829178390-5312a631a68e-768x432.jpg 768w"
                                data-lazy-sizes="(max-width: 1947px) 100vw, 1947px"><noscript><img
width=1947 height=1095 src=../../cdn.maubelajarapa.com/wp-content/uploads/2019/01/21093008/photo-1534829178390-5312a631a68e.jpg class="attachment-full size-full wp-post-image" alt="tips biar sukses" srcset="https://cdn.maubelajarapa.com/wp-content/uploads/2019/01/21093008/photo-1534829178390-5312a631a68e.jpg 1947w, https://cdn.maubelajarapa.com/wp-content/uploads/2019/01/21093008/photo-1534829178390-5312a631a68e-768x432.jpg 768w" sizes="(max-width: 1947px) 100vw, 1947px"></noscript></div>
                        <div class=author-box>
                            <div class=author-pic>
                                <img src=../../cdn.maubelajarapa.com/wp-content/uploads/2018/10/03141535/evi.png alt width=90 height=90 class="avatar photo"></div>
                            <div class=author-header>
                                <span class=label-title>Author:</span>
                                <h3 class="author-name"><a href=../author/evi/index.html title="Posts by {{$post->admin->name}}" rel=author>{{$post->admin->name}}</a></h3>
                                <ul class=author-social>
                                    <li><a href=#><i
class="icon icon-email"></i></a></li>
                                    <li><a href=#><i
class="icon icon-facebook"></i></a></li>
                                    <li><a href=#><i
class="icon icon-twitter"></i></a></li>
                                </ul>
                            </div>
                            <p class=author-bio></p>
                        </div>
                        <div class=post-entries>
                            {{$post->body}}
                        </div>
                    </article>
                    <div class="share-box-wrapper visible-sm visible-xs">
                        <label>Share this post</label>
                        <ul class=share-box>
                            <li>
                                <a href=# title="Bookmark this post" class=btn-bookmark>
                                    <i class="icon icon-bookmark-o"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.facebook.com/dialog/share?app_id=1588093121509231&amp;display=page&amp;href=https://maubelajarapa.com/9-kebiasaan-yang-harus-dihilangkan-di-tahun-2019/&amp;redirect_uri=https://maubelajarapa.com/9-kebiasaan-yang-harus-dihilangkan-di-tahun-2019"
                                    class="btn-facebook popup-share" title="Share to Facebook">
                                    <i class="icon icon-facebook"></i>
                                </a>
                                <div class="facebook-share-count share-count">0</div>
                            </li>
                            <li>
                                <a href="https://twitter.com/intent/tweet?url=https://maubelajarapa.com/9-kebiasaan-yang-harus-dihilangkan-di-tahun-2019/&amp;text=Check%20out%20this%20workshop%209%20Kebiasaan%20Buruk%20yang%20Harus%20Dihilangkan%20di%20Tahun%202019%20here%20https://maubelajarapa.com/9-kebiasaan-yang-harus-dihilangkan-di-tahun-2019/&amp;via=Maubelajarapa&amp;hashtags=#maubelajarapa#workshop"
                                    class="btn-twitter popup-share" title="Share to Twitter">
                                    <i class="icon icon-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.linkedin.com/shareArticle?url=https://maubelajarapa.com/9-kebiasaan-yang-harus-dihilangkan-di-tahun-2019/&amp;title=Check%20out%20this%20workshop%209%20Kebiasaan%20Buruk%20yang%20Harus%20Dihilangkan%20di%20Tahun%202019%20here%20https://maubelajarapa.com/9-kebiasaan-yang-harus-dihilangkan-di-tahun-2019/"
                                    class="btn-linkedin popup-share" title="Share to Linkedin">
                                    <i class="icon icon-linkedin"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection