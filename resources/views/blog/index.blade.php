@extends('inc.layout') @section('content')

<div class="container site-content">
    <section id=blog-archive class=page-section>
        <div class=section-header>
            <h2 class="section-title">Blog</h2>
        </div>
        <div class=row>
            <div class=col-md-9>
                <div class=post-archive>
                    @if(count($posts)>0)
                        @foreach ($posts as $post)
                        <div class=post>
                                <div class=row>
                                    <div class="col-md-5 col-sm-6">
                                        <a href="/showposts/<?php echo $post->id?>" class="post-img img-fit-wrapper">
                                            <img width=561 height=561 src="<?php echo asset('/images') ?>/{{$post->thumbnail}}" data-lazy-type=image data-src=https://cdn.maubelajarapa.com/wp-content/uploads/2019/05/20113904/photo-1551498624-3712f8e41930.jpg class="lazy lazy-hidden img-lazy attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                                alt="cerita bunda" data-lazy-srcset="https://cdn.maubelajarapa.com/wp-content/uploads/2019/05/20113904/photo-1551498624-3712f8e41930.jpg 561w, https://cdn.maubelajarapa.com/wp-content/uploads/2019/05/20113904/photo-1551498624-3712f8e41930-180x180.jpg 180w, https://cdn.maubelajarapa.com/wp-content/uploads/2019/05/20113904/photo-1551498624-3712f8e41930-300x300.jpg 300w"
                                                data-lazy-sizes="(max-width: 561px) 100vw, 561px"><noscript><img
        width=561 height=561 src=../../../cdn.maubelajarapa.com/wp-content/uploads/2019/05/20113904/photo-1551498624-3712f8e41930.jpg class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="cerita bunda" srcset="https://cdn.maubelajarapa.com/wp-content/uploads/2019/05/20113904/photo-1551498624-3712f8e41930.jpg 561w, https://cdn.maubelajarapa.com/wp-content/uploads/2019/05/20113904/photo-1551498624-3712f8e41930-180x180.jpg 180w, https://cdn.maubelajarapa.com/wp-content/uploads/2019/05/20113904/photo-1551498624-3712f8e41930-300x300.jpg 300w" sizes="(max-width: 561px) 100vw, 561px"></noscript>                                    </a>
                                    </div>
                                    <div class="col-md-7 col-sm-6">
                                        <div class=post-content>
                                            <span class=post-category>
        {{$post->tags}}	</span>
                                            <h3 class="post-title"><a href="/showposts/<?php echo $post->id?>">{{$post->title}}</a></h3>
                                            <ul class=post-meta>
                                                <li class=post-author>{{$post->admin->name}}</li>
                                                <li class=post-date>{{$post->created_at}}</li>
                                                <li class=post-comments>0</li>
                                            </ul>
                                            <p class=post-entries><?php echo substr($post->body, 0, 50); ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                    
                </div>
    </section>
    </div>
    @endsection