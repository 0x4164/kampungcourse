@extends('inc.layout') @section('content')
<?php use \App\Http\Controllers\CourseController; ?>



<div id=content class=main-container>
    <div class="container site-content">
        <div id=woocommerce-content role=main>
            <section >
                <h1 class="heading-title">Daftar Lembaga</h1>
            </section>
            <section class="archive-box products">
                <div class="row workshops-container">
                    @foreach ($merchants as $merchant)
                    <div class="col-md-3">
                            <div class="post course">
                                <div class=content-wrapper>
                                <a href="/showLembaga/{{$merchant->id}}" class=img-fit-wrapper>
                                <img width=250 height=250 src="<?php echo asset('/images') ?>/{{$merchant->profileImage}}" data-lazy-type=image data-src=https://cdn.maubelajarapa.com/wp-content/uploads/2018/10/31103821/pexels-photo-529922-950x850.jpeg class="lazy lazy-hidden img-lazy post-img wp-post-image"
                                            ><noscript><img width=279 height=250 src="<?php echo asset('/images') ?>/{{$merchant->profileImage}}" class="post-img wp-post-image" ></noscript>                                    </a>
                                    <div class=post-content>
                                        <a href="/showLembaga/{{$merchant->id}}" class=post-title>
                                            <strong>{{$merchant->name}}</strong><hr></a>
                                            <strong class="pull-left">{{$merchant->categories}}</strong>
                                            <strong class="pull-right">{{$merchant->businessType}}</strong>
                                            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
                                            <br>
                                            <style>.checked {
                                                    color: orange;
                                                  }</style>
                                            <div>
                                                <?php $rating = CourseController::getRating($merchant->id); 
                                                for($i=0;$i<5;$i++){
                                                    if($i<$rating){
                                                        echo '<span class="fa fa-star checked"></span>';
                                                    }else{
                                                        echo '<span class="fa fa-star"></span>';
                                                    }
                                                }  ?>
                                                </div>
                                        
                                            
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    

                </div>
            </section>
        </div>
    </div>
</div>
<script language="JavaScript">
    function toggle(source) {
  checkboxes = document.getElementsByName('foo');
  for(var i=0, n=checkboxes.length;i<n;i++) {
    checkboxes[i].checked = source.checked;
  }
}

    </script>
@endsection