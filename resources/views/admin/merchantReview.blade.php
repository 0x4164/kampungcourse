@extends('inc.layout')
@section('content')
<?php use \App\Http\Controllers\CourseController; ?>
<div id=content class=main-container>
        <div id=vendor-page>
            <section class=page-heading>
                <div class="heading-img img-fit-wrapper">
                    <img src={{asset('images/'.$merchant->coverImage)}} alt="{{$merchant->name}}"></div>
                <nav class="vendor-masthead page-masthead">
                    <div class="vendor-header-content page-header-content">
                        <div class=container>
                            <div class=row>
                                <div class="col-sm-4 col-sm-push-4">
                                    <div class=vendor-pic-wrapper>
                                        <figure class="vendor-pic img-fit-wrapper"><img src={{asset('images/'.$merchant->profileImage)}} alt="{{$merchant->name}}"></figure>
                                        <h2 class="vendor-name">{{$merchant->name}}</h2>
                                        <i class=vendor-about>{{$merchant->categories}}</i></div>
                                </div>
                                <div class="col-sm-4 col-sm-push-4">
                                        <button href=# style="background-color:green; color:white;" class="btn btn-transparent  navbar-btn form-report-vendor_open"><i class="fas fa-user"></i> {{$participant}} Peserta</button>
                                </div>
                                <div class="col-sm-4 col-sm-pull-8">
                                <a @if(Auth::check()) href="/chat/{{$merchant->id}}" @else href="{{route('login')}}" @endif  style="background-color:blue; color:white;" class="btn btn-transparent  navbar-btn form-report-vendor_open"><i class="fas fa-envelope"></i> Konsultasi</a>
                            </div>
                        </div>
                    </div>
                    <div class="vendor-menu page-header-menu">
                        <div class=container>
                            <ul class="nav navbar-nav">
                            <li><a href="/showLembagaKursus/{{$merchant->id}}">Kursus</a></li>
                                <li><a href="/showLembaga/{{$merchant->id}}">Profil</a></li>
                                <li><a  href="/showLembagaGaleri/{{$merchant->id}}">Galeri</a></li>
                                <li class=active><a  href="/showLembagaReview/{{$merchant->id}}">Review</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </section>

            <div class="container vendor-tab-content vendor-review">
                <?php $rating = CourseController::getRating($merchant->id); ?>
                <div class="vendor-rating">Reviews<span class="star"><span class="star-width" style="width: <?php echo $rating*20; ?>%"></span></span></div>
    
                <ul id="reviews" class="review-list">
                    @if(count($testimonies)>0)
                @foreach ($testimonies as $testimony)
                    @foreach ($testimony->invoices as $item)
                    <?php
                        $user = CourseController::getUser($item->user_id); 
                        ?>


                        <li>
                            <div class="review-profile">
                                <img src="https://secure.gravatar.com/avatar/c2e848388f348ab016d7417c4e1baf60?s=96&amp;d=mm&amp;r=g" alt="{{$user->name}}">
                                <span class="review-name">{{$user->name}}</span>
                            </div>
                            <div class="review-content">
                                <p class="review-entries">{{$item->testimony}}</p>
                                <strong class="review-detail">
                                    Attended: {{$testimony->title}}
                                </strong>
                            </div>
                        </li>
                    @endforeach
                @endforeach
                @else
                Belum Ada Review
                @endif
                                   
                                    
                    
                    
                </ul>
    
                </div>

            
        </div>
    </div>
    @endsection