@extends('inc.layout')
@section('content')

<div id=content class=main-container>
        <div id=vendor-page>
            <section class=page-heading>
                <div class="heading-img img-fit-wrapper">
                    <img src={{asset('assets/wp-content/themes/maubelajarapa/assets/images/KampungCourseLogo.png')}} alt="Fifty Seven Promenade"></div>
                <nav class="vendor-masthead page-masthead">
                    <div class="vendor-header-content page-header-content">
                        <div class=container>
                            <div class=row>
                                <div class="col-sm-4 col-sm-push-4">
                                    <div class=vendor-pic-wrapper>
                                        <figure class="vendor-pic img-fit-wrapper"><img src={{asset('assets/wp-content/themes/maubelajarapa/assets/images/KampungCourseLogo2.png')}} alt="Fifty Seven Promenade"></figure>
                                        <h2 class="vendor-name">Kampung Course</h2>
                                        <i class=vendor-about>(Where You Can Learn Everything)</i></div>
                                </div>
                                <div class="col-sm-4 col-sm-push-4">
                                        <button href=# style="background-color:green; color:white;" class="btn btn-transparent  navbar-btn form-report-vendor_open"><i class="fas fa-user"></i> 0 Peserta</button>
                                </div>
                                <div class="col-sm-4 col-sm-pull-8">
                                    <button href=# style="background-color:blue; color:white;" class="btn btn-transparent  navbar-btn form-report-vendor_open"><i class="fas fa-envelope"></i> Konsultasi</button>
                            </div>
                        </div>
                    </div>
                    <div class="vendor-menu page-header-menu">
                        <div class=container>
                            <ul class="nav navbar-nav">
                            <li><a href="{{route('lembaga.kursus')}}">Kursus</a></li>
                                <li class=active><a href={{route('lembaga')}}>Profil</a></li>
                                <li><a href="{{route('lembaga.gallery')}}">Galeri</a></li>
                                <li><a href="{{route('lembaga.review')}}">Review</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </section>
            <div id=form-report-vendor class=mba-popup data-blur=true>
                <div class=popup-header>
                    <h3>MAKE REPORT</h3>
                    <p>We apologize for your unpleasant experience with the educator. Please kindly write down your feedback below and our team will look into the matter as soon as possible.</p>
                    <button class=form-report-vendor_close><i
class=icon-close></i></button></div>
                <div class=popup-content>
                    <form action>
                        <div class=form-group><textarea name id class=form-control cols=30 rows=10></textarea></div>
                        <div class=form-group>
                            <button class="btn btn-primary pull-right">Submit</button></div>
                    </form>
                </div>
            </div>
            <div class="container vendor-tab-content vendor-profile">
                <p class=vendor-description>
                    No Review Yet
                </p>
            </div>
        </div>
    </div>
    @endsection