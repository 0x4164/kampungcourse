@extends('inc.layout')
@section('content')

<div id=content class=main-container>
        <div id=vendor-page>
            <section class=page-heading>
                <div class="heading-img img-fit-wrapper">
                    <img src={{asset('images/'.$merchant->coverImage)}} alt="{{$merchant->name}}"></div>
                <nav class="vendor-masthead page-masthead">
                    <div class="vendor-header-content page-header-content">
                        <div class=container>
                            <div class=row>
                                <div class="col-sm-4 col-sm-push-4">
                                    <div class=vendor-pic-wrapper>
                                        <figure class="vendor-pic img-fit-wrapper"><img src={{asset('images/'.$merchant->profileImage)}} alt="{{$merchant->name}}"></figure>
                                        <h2 class="vendor-name">{{$merchant->name}}</h2>
                                        <i class=vendor-about>{{$merchant->categories}}</i></div>
                                </div>
                                <div class="col-sm-4 col-sm-push-4">
                                        <button href=# style="background-color:green; color:white;" class="btn btn-transparent  navbar-btn form-report-vendor_open"><i class="fas fa-user"></i> {{$participant}} Peserta</button>
                                </div>
                                <div class="col-sm-4 col-sm-pull-8">
                                <a @if(Auth::check()) href="/chat/{{$merchant->id}}" @else href="{{route('login')}}" @endif  style="background-color:blue; color:white;" class="btn btn-transparent  navbar-btn form-report-vendor_open"><i class="fas fa-envelope"></i> Konsultasi</a>
                            </div>
                        </div>
                    </div>
                    <div class="vendor-menu page-header-menu">
                        <div class=container>
                            <ul class="nav navbar-nav">
                            <li><a href="/showLembagaKursus/{{$merchant->id}}">Kursus</a></li>
                            <li class=active><a href="/showLembaga/{{$merchant->id}}">Profil</a></li>
                                <li><a href="/showLembagaGaleri/{{$merchant->id}}">Galeri</a></li>
                                <li><a href="/showLembagaReview/{{$merchant->id}}">Review</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </section>
            <div class="container vendor-tab-content vendor-profile">
                <div class=vendor-location><strong>Kota:</strong> {{$merchant->city}}</div>
                <div class=vendor-location><strong>Tipe Lembaga:</strong> {{$merchant->businessType}}</div>
                <div class=vendor-location><strong>Nomor HP:</strong> {{$merchant->phone}}</div>
                <div class=vendor-location><strong>Alamat Lembaga:</strong> {{$merchant->address}}</div>
                <div class=vendor-location><strong>Kode Pos:</strong> {{$merchant->postCode}}</div>
                <p class=vendor-description>
                    {{$merchant->description}}
                </p>
                <?php
                    $berkas = json_decode($merchant->berkas);
                    foreach ($berkas as $file) {?>
                        <img width="700" src="<?php echo asset('/images') ?>/<?php echo $file ?>" style="padding:30px;">
                        <hr>
                        <?php  }
                ?>
            </div>
        </div>
    </div>
    @endsection