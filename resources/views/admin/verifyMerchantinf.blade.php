@extends('inc.layout')
@section('content')

<div id=vendor-page>
        <section class=page-heading>
            <div class="heading-img img-fit-wrapper">
                <img src="<?php echo asset('/images') ?>/{{$merchant->coverImage}}" alt="{{$merchant->name}}"></div>
            <nav class="vendor-masthead page-masthead">
                <div class="vendor-header-content page-header-content">
                    <div class=container>
                        <div class=row>
                            <div class="col-sm-4 col-sm-push-4">
                                <div class=vendor-pic-wrapper>
                                    <figure class="vendor-pic img-fit-wrapper"><img src="<?php echo asset('/images') ?>/{{$merchant->profileImage}}" alt="{{$merchant->name}}"></figure>
                                    <h2 class="vendor-name">{{$merchant->name}}</h2>
                                    <i class=vendor-about>{{$merchant->categories}}</i></div>
                            </div>
                            <div class="col-sm-4 col-sm-push-4">
                                <a href="/unverifyMerchant/{{$merchant->id}}" class="btn btn-danger"><i class="fas fa-times-circle"></i> Tolak</a>
                            </div>
                            <div class="col-sm-4 col-sm-pull-8">
                                <a href="/verifyMerchant/{{$merchant->id}}" class="btn btn-success"><i class="fas fa-check-circle"></i> Verifikasi</a>
                                </div>
                        </div>
                    </div>
                </div>
                <div class="vendor-menu page-header-menu">
                    <div class=container>
                        <ul class="nav navbar-nav">
                            <li class=active><a href="/showVerifyMerchantinf/{{$merchant->id}}">Detail Informasi</a></li>
                            <li ><a href="/showVerifyMerchant/{{$merchant->id}}">Profil</a></li>
                            <li class><a href="/showVerifyMerchantberkas/{{$merchant->id}}">Berkas</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </section>
        <div class="container vendor-tab-content vendor-profile">
            <p class=vendor-description>
            <p><label>Tipe Lembaga :&nbsp</label>{{$merchant->businessType}}</p>
            <p><label>Nomor HP :&nbsp</label>{{$merchant->phone}}</p>
            <p><label>Maps :&nbsp</label><a href="{{$merchant->maps}}">Tampilkan Peta</a></p>
            <p><label>Alamat :&nbsp</label>{{$merchant->address}}</p>
            <p><label>Kode Pos :&nbsp</label>{{$merchant->postCode}}</p>
            <p><label>Kota :&nbsp</label>{{$merchant->city}}</p>
            </p>
            <h4>Galeri : </h4>
            <div class="row workshops-container" style="padding-bottom:30px;">
                <?php
                    $galleries = json_decode($merchant->gallery);
                    foreach ($galleries as $gallery) {?>
                        <div class="col-md-4 col-sm-6" style="padding-bottom:15px">
                                <img width="279" height="250" src="<?php echo asset('/images') ?>/<?php echo $gallery ?>" data-lazy-type="image" data-src="https://cdn.maubelajarapa.com/wp-content/uploads/2019/05/03111124/pexels-photo-374009-950x850.jpeg" class="lazy img-lazy post-img wp-post-image" alt="">
                                <noscript>
                                    <img width=279 height=250 src=https://cdn.maubelajarapa.com/wp-content/uploads/2019/05/03111124/pexels-photo-374009-950x850.jpeg class="post-img wp-post-image" alt>
                                </noscript>
                            </div>
                        <?php  }
                ?>
                
            </div>
        </div>
    </div>

    @endsection