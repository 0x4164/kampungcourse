@extends('inc.layout')
@section('content')


<div id="content" class="main-container">
    <div class="woocommerce">
        <div id="customer-account">
            <div class="container">
                <div class="customer-account-wrapper">
                    <nav class="woocommerce-MyAccount-navigation">
                        <ul>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--dashboard"><a href="#">Dashboard</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--dashboard is-active"><a href="#">Verifikasi</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--dashboard"><a href="#">Analisis Omzet</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards"><a href="#">Promo Diskon & Flashsale</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--dashboard"><a href="#">Iklan</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--customer-logout"><a href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                              document.getElementById('logout-form').submit();">Logout</a></li>
                        </ul>
                    </nav>
                    <div class="woocommerce-MyAccount-content">

                            


                        <table class="woocommerce-orders-table woocommerce-MyAccount-orders shop_table shop_table_responsive my_account_orders account-orders-table">
                            <thead>
                                <tr>
                                    
                                    <th class="woocommerce-orders-table__header woocommerce-orders-table__header-workshop"><span class="nobr">Lembaga</span></th>
                                    <th class="woocommerce-orders-table__header woocommerce-orders-table__header-start_date"><span class="nobr">Kursus</span></th>
                                    <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-date"><span class="nobr">Tanggal</span></th>
                                    <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-date"><span class="nobr">Total</span></th>
                                    <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-date"><span class="nobr">Action</span></th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="woocommerce-orders-table__row woocommerce-orders-table__row--status-cancelled order">
                                    
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-workshop" data-title="Workshop">
                                        Learn How To Measure &amp; Analyze Your Mobile App Success</td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-start_date" data-title="Start Date">
                                            Learn How To Measure &amp; Analyze Your Mobile App Success</td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-date" data-title="Order Date">
                                        <time datetime="2019-09-16T15:10:37+00:00">Sep 16, 2019</time></td>
                                        <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-start_date" data-title="Start Date">
                                                Rp. 167.785.978.556</td>
                                                <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-actions"
                                        data-title="Actions">
                                        <a href="https://maubelajarapa.com/account/view-order/205628" class="woocommerce-button button view">View</a><a href="https://maubelajarapa.com/invoice/205628?action=generate_wpo_wcpdf&amp;my-account&amp;_wpnonce=f7ec9643cf"
                                            class="woocommerce-button button invoice">Verifikasi</a></td>
                                </tr>
                                <tr class="woocommerce-orders-table__row woocommerce-orders-table__row--status-cancelled order">
                                    
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-workshop" data-title="Workshop">
                                        Learn The Essentials Of Business Writing</td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-start_date" data-title="Start Date">
                                            Learn The Essentials Of Business Writing</td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-date" data-title="Order Date">
                                        <time datetime="2019-09-16T15:07:36+00:00">Sep 16, 2019</time></td>
                                        <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-start_date" data-title="Start Date">
                                                Rp. 167.785.978.556</td>
                                                <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-actions"
                                                data-title="Actions">
                                                <a href="https://maubelajarapa.com/account/view-order/205628" class="woocommerce-button button view">View</a><a href="https://maubelajarapa.com/invoice/205628?action=generate_wpo_wcpdf&amp;my-account&amp;_wpnonce=f7ec9643cf"
                                                    class="woocommerce-button button invoice">Verifikasi</a></td>
                                    
                                </tr>
                                <tr class="woocommerce-orders-table__row woocommerce-orders-table__row--status-cancelled order">
                                    
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-workshop" data-title="Workshop">
                                        Transforming Your Family Business In The Digital Era</td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-start_date" data-title="Start Date">
                                            Transforming Your Family Business In The Digital Era</td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-date" data-title="Order Date">
                                        <time datetime="2019-08-02T22:55:36+00:00">Aug 2, 2019</time></td>
                                        <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-start_date" data-title="Start Date">
                                                Rp. 167.785.978.556</td>
                                                <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-actions"
                                                data-title="Actions">
                                                <a href="https://maubelajarapa.com/account/view-order/205628" class="woocommerce-button button view">View</a><a href="https://maubelajarapa.com/invoice/205628?action=generate_wpo_wcpdf&amp;my-account&amp;_wpnonce=f7ec9643cf"
                                                    class="woocommerce-button button invoice">Verifikasi</a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="swal2-container swal2-center swal2-fade swal2-shown" id="testimony" style="overflow-y: auto; display:none;"><div role="dialog" aria-modal="true" aria-labelledby="swal2-title" aria-describedby="swal2-content" class="swal2-popup swal2-modal swal2-show" tabindex="-1" aria-live="assertive" style="width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; display: flex;"><ul class="swal2-progresssteps" style="display: none;"></ul><div class="swal2-icon swal2-error" style="display: none;"><span class="swal2-x-mark"><span class="swal2-x-mark-line-left"></span><span class="swal2-x-mark-line-right"></span></span></div><div class="swal2-icon swal2-question" style="display: none;">?</div><div class="swal2-icon swal2-warning" style="display: none;">!</div><div class="swal2-icon swal2-info" style="display: none;">i</div><div class="swal2-icon swal2-success" style="display: none;"><div class="swal2-success-circular-line-left" style="background: rgb(255, 255, 255) none repeat scroll 0% 0%;"></div><span class="swal2-success-line-tip"></span> <span class="swal2-success-line-long"></span><div class="swal2-success-ring"></div> <div class="swal2-success-fix" style="background: rgb(255, 255, 255) none repeat scroll 0% 0%;"></div><div class="swal2-success-circular-line-right" style="background: rgb(255, 255, 255) none repeat scroll 0% 0%;"></div></div><img class="swal2-image" src="{{asset('assets/wp-content/themes/maubelajarapa/assets/images/KampungCourseLogo.png')}}" alt="" style="display: block;"><div class="swal2-contentwrapper"><h2 class="swal2-title" id="swal2-title"></h2><div id="swal2-content" class="swal2-content" style="display: block;">Masukkan Testimoni Anda Dibawah, Kami akan menampilkannya pada testimoni kursus</div></div><textarea class="swal2-input" rows="5" style="display: block; height:max-content;" placeholder="" type="text"></textarea><input type="file" class="swal2-file" style="display: none;"><div class="swal2-range" style="display: none;"><output></output><input type="range"></div><select class="swal2-select" style="display: none;"></select><div class="swal2-radio" style="display: none;"></div><label for="swal2-checkbox" class="swal2-checkbox" style="display: none;"><input type="checkbox"></label><textarea class="swal2-textarea" style="display: none;"></textarea><div class="swal2-validationerror" id="swal2-validationerror" style="display: none;"></div><div class="swal2-buttonswrapper" style="display: flex;"><button type="button" class="swal2-confirm swal2-styled" aria-label="" style="background-color: rgb(48, 133, 214); border-left-color: rgb(48, 133, 214); border-right-color: rgb(48, 133, 214);">Submit</button><button type="button" class="swal2-cancel swal2-styled" style="display: inline-block; background-color: rgb(170, 170, 170);" aria-label="" onclick="document.getElementById('testimony').style.display = 'none';">Cancel</button></div><button type="button" class="swal2-close" style="display: none;">×</button></div></div>

    @endsection