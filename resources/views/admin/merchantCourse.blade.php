@extends('inc.layout')
@section('content')

<div id=content class=main-container>
        <div id=vendor-page>
            <section class=page-heading>
                <div class="heading-img img-fit-wrapper">
                    <img src={{asset('images/'.$merchant->coverImage)}} alt="{{$merchant->name}}"></div>
                <nav class="vendor-masthead page-masthead">
                    <div class="vendor-header-content page-header-content">
                        <div class=container>
                            <div class=row>
                                <div class="col-sm-4 col-sm-push-4">
                                    <div class=vendor-pic-wrapper>
                                        <figure class="vendor-pic img-fit-wrapper"><img src={{asset('images/'.$merchant->profileImage)}} alt="{{$merchant->name}}"></figure>
                                        <h2 class="vendor-name">{{$merchant->name}}</h2>
                                        <i class=vendor-about>{{$merchant->categories}}</i></div>
                                </div>
                                <div class="col-sm-4 col-sm-push-4">
                                        <button href=# style="background-color:green; color:white;" class="btn btn-transparent  navbar-btn form-report-vendor_open"><i class="fas fa-user"></i> {{$participant}} Peserta</button>
                                </div>
                                <div class="col-sm-4 col-sm-pull-8">
                                <a @if(Auth::check()) href="/chat/{{$merchant->id}}" @else href="{{route('login')}}" @endif  style="background-color:blue; color:white;" class="btn btn-transparent  navbar-btn form-report-vendor_open"><i class="fas fa-envelope"></i> Konsultasi</a>
                            </div>
                        </div>
                    </div>
                    <div class="vendor-menu page-header-menu">
                        <div class=container>
                            <ul class="nav navbar-nav">
                            <li class=active><a  href="/showLembagaKursus/{{$merchant->id}}">Kursus</a></li>
                                <li><a href="/showLembaga/{{$merchant->id}}">Profil</a></li>
                                <li><a href="/showLembagaGaleri/{{$merchant->id}}">Galeri</a></li>
                                <li><a href="/showLembagaReview/{{$merchant->id}}">Review</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </section>
            <div class="container vendor-tab-content">
                    @if(count($courses)>0) @foreach ($courses as $course)

                    <div class="col-md-4 col-sm-6">
                        <div class="post course">
                            <div class=content-wrapper>
                            <a href="/course/{{$course->id}}" class=img-fit-wrapper>
                                    <?php $area = (array) json_decode($course->thumbnail, true);
                                        $image = 'noimage.png';
                                        foreach($area as $v)
                                        {
                                            $image = $v;
                                        } ?>
                                    <img width=279 height=250 src="<?php echo asset('/images') ?>/<?php echo $image?>" data-lazy-type=image data-src=https://cdn.maubelajarapa.com/wp-content/uploads/2018/10/31103821/pexels-photo-529922-950x850.jpeg class="lazy lazy-hidden img-lazy post-img wp-post-image"
                                        alt="sensory coffee jakarta"><noscript><img width=279 height=250 src="<?php echo asset('/images') ?>/<?php echo $image?>" class="post-img wp-post-image" alt="sensory coffee jakarta"></noscript>                                    </a>
                                <div class=post-content>
                                    <span class=course-category title=Coffee>{{$course->category}}</span>
                                    <a href="/course/{{$course->id}}" class=post-title>
                                        <strong>Kursus:</strong> {{$course->title}}</a>
                                    <div class=course-info>
                                        <strong class=course-price>
                                                <span class="woocommerce-Price-amount amount">
                                                    <span class=woocommerce-Price-currencySymbol>Rp</span>&nbsp;{{$course->price}}</span>		</strong>
                                        <strong class=time-exp> 14 hours left	</strong></div>
                                </div>
                                <div class=course-footer>
                                    <div class=course-footer-heading>
                                        <a href="/showLembaga/{{$course->merchant->id}}" class=course-vendor>
                                            <img src={{asset('images/'.$course->merchant->profileImage)}} alt="{{$course->merchant->name}}" class=vendor-img>
                                            <span>{{$course->merchant->name}}</span>
                                            <strong>{{$course->merchant->address}}</strong>
                                        </a>
                                        <div class=course-date>
                                            <span class=month>Aug</span>
                                            <strong class=date>27</strong></div>
                                            <a href=# class=show-other-dates>Add To Wishlist</a></div>
                                            <div class=course-other-dates>
                                                <p>Pengen Ikut Tapi Nggak Sempet Datang / Belum Punya Uang ? Tambahin Ke Wishlist Aja</p>
                                                <div class=button-box>
                                                <a name=add-to-cart value=167308 class="single_add_to_cart_button btn btn-primary" @if(Auth::check()) href="/addWishlist/{{$course->id}}" @else disabled="disabled" @endif>+ Wishlist</a>
                                                    <a href=# class="btn btn-default btn-cancel">Cancel</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach @else 
                    <p class="woocommerce-info">Tidak Ditemukan Kursus/Lembaga dengan kriteria pencarian yang dimasukkan.</p>
                    @endif
            </div>
        </div>
    </div>
    @endsection