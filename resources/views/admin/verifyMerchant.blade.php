@extends('inc.layout')
@section('content')

<div id=vendor-page>
        <section class=page-heading>
            <div class="heading-img img-fit-wrapper">
                <img src="<?php echo asset('/images') ?>/{{$merchant->coverImage}}" alt="{{$merchant->name}}"></div>
            <nav class="vendor-masthead page-masthead">
                <div class="vendor-header-content page-header-content">
                    <div class=container>
                        <div class=row>
                            <div class="col-sm-4 col-sm-push-4">
                                <div class=vendor-pic-wrapper>
                                    <figure class="vendor-pic img-fit-wrapper"><img src="<?php echo asset('/images') ?>/{{$merchant->profileImage}}" alt="{{$merchant->name}}"></figure>
                                    <h2 class="vendor-name">{{$merchant->name}}</h2>
                                    <i class=vendor-about>{{$merchant->categories}}</i></div>
                            </div>
                            <div class="col-sm-4 col-sm-push-4">
                                <a href="/unverifyMerchant/{{$merchant->id}}" class="btn btn-danger"><i class="fas fa-times-circle"></i> Tolak</a>
                            </div>
                            <div class="col-sm-4 col-sm-pull-8">
                                <a href="/verifyMerchant/{{$merchant->id}}" class="btn btn-success"><i class="fas fa-check-circle"></i> Verifikasi</a>
                                </div>
                        </div>
                    </div>
                </div>
                <div class="vendor-menu page-header-menu">
                    <div class=container>
                        <ul class="nav navbar-nav">
                            <li class><a href="/showVerifyMerchantinf/{{$merchant->id}}">Detail Informasi</a></li>
                            <li class=active><a href="/showVerifyMerchant/{{$merchant->id}}">Profil</a></li>
                            <li class><a href="/showVerifyMerchantberkas/{{$merchant->id}}">Berkas</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </section>
        <div class="container vendor-tab-content vendor-profile">
            <p class=vendor-description>
                {{$merchant->description}}
            </p>
        </div>
    </div>

    @endsection