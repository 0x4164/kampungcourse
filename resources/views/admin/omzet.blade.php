@extends('inc.layout')
@section('content')


<div id="content" class="main-container">
    <div class="woocommerce">
        <div id="customer-account">
            <div class="container">
                <div class="customer-account-wrapper">
                    <nav class="woocommerce-MyAccount-navigation">
                        <ul>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--dashboard"><a href="#">Dashboard</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--dashboard"><a href="#">Verifikasi</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--dashboard"><a href="#">Analisis Omzet</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--points-and-rewards"><a href="#">Promo Diskon & Flashsale</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--dashboard is-active"><a href="#">Iklan</a></li>
                            <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--customer-logout"><a href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                              document.getElementById('logout-form').submit();">Logout</a></li>
                        </ul>
                    </nav>
                    <div class="woocommerce-MyAccount-content">

                            <form method="post">
                                    <h3>Email Marketing</h3>
                                    <div class="woocommerce-address-fields">
                                        <div class="woocommerce-address-fields__field-wrapper">

                                                <p class="form-row form-row-wide address-field validate-required validate-state" id="billing_state_field" data-priority="80">
                                                        <label for="billing_state" class="">Grup <abbr class="required" title="required">*</abbr></label><select style="width:700px" name="billing_state" id="billing_state" class="state_select  select2-hidden-accessible" autocomplete="address-level1" data-placeholder=""
                                                            tabindex="-1" aria-hidden="true"><option value="">Pilih Grup Pengiriman…</option><option value="AC">Daerah Istimewa Aceh</option></select>
                                                        
                                                        <span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span>
                                                        </span>
                                                        </span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                                                    </p>
                                                    <p class="form-row form-row-wide address-field validate-required validate-state" id="billing_state_field" data-priority="80">
                                                            <label for="billing_state" class="">Pesan <abbr class="required" title="required">*</abbr></label><textarea rows="10"></textarea>
                                                        </p>


                                        <p>
                                            <input type="submit" class="button" name="save_address" value="Kirim Pesan">
                                            <input type="hidden" id="_wpnonce" name="_wpnonce" value="8a88ee81e6"><input type="hidden" name="_wp_http_referer" value="/account/edit-address/billing/"> <input type="hidden" name="action" value="edit_address"></p>
                                    </div>
                                </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="swal2-container swal2-center swal2-fade swal2-shown" id="testimony" style="overflow-y: auto; display:none;"><div role="dialog" aria-modal="true" aria-labelledby="swal2-title" aria-describedby="swal2-content" class="swal2-popup swal2-modal swal2-show" tabindex="-1" aria-live="assertive" style="width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; display: flex;"><ul class="swal2-progresssteps" style="display: none;"></ul><div class="swal2-icon swal2-error" style="display: none;"><span class="swal2-x-mark"><span class="swal2-x-mark-line-left"></span><span class="swal2-x-mark-line-right"></span></span></div><div class="swal2-icon swal2-question" style="display: none;">?</div><div class="swal2-icon swal2-warning" style="display: none;">!</div><div class="swal2-icon swal2-info" style="display: none;">i</div><div class="swal2-icon swal2-success" style="display: none;"><div class="swal2-success-circular-line-left" style="background: rgb(255, 255, 255) none repeat scroll 0% 0%;"></div><span class="swal2-success-line-tip"></span> <span class="swal2-success-line-long"></span><div class="swal2-success-ring"></div> <div class="swal2-success-fix" style="background: rgb(255, 255, 255) none repeat scroll 0% 0%;"></div><div class="swal2-success-circular-line-right" style="background: rgb(255, 255, 255) none repeat scroll 0% 0%;"></div></div><img class="swal2-image" src="{{asset('assets/wp-content/themes/maubelajarapa/assets/images/KampungCourseLogo.png')}}" alt="" style="display: block;"><div class="swal2-contentwrapper"><h2 class="swal2-title" id="swal2-title"></h2><div id="swal2-content" class="swal2-content" style="display: block;">Masukkan Testimoni Anda Dibawah, Kami akan menampilkannya pada testimoni kursus</div></div><textarea class="swal2-input" rows="5" style="display: block; height:max-content;" placeholder="" type="text"></textarea><input type="file" class="swal2-file" style="display: none;"><div class="swal2-range" style="display: none;"><output></output><input type="range"></div><select class="swal2-select" style="display: none;"></select><div class="swal2-radio" style="display: none;"></div><label for="swal2-checkbox" class="swal2-checkbox" style="display: none;"><input type="checkbox"></label><textarea class="swal2-textarea" style="display: none;"></textarea><div class="swal2-validationerror" id="swal2-validationerror" style="display: none;"></div><div class="swal2-buttonswrapper" style="display: flex;"><button type="button" class="swal2-confirm swal2-styled" aria-label="" style="background-color: rgb(48, 133, 214); border-left-color: rgb(48, 133, 214); border-right-color: rgb(48, 133, 214);">Submit</button><button type="button" class="swal2-cancel swal2-styled" style="display: inline-block; background-color: rgb(170, 170, 170);" aria-label="" onclick="document.getElementById('testimony').style.display = 'none';">Cancel</button></div><button type="button" class="swal2-close" style="display: none;">×</button></div></div>

    @endsection