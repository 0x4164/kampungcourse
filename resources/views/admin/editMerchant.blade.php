@extends('inc.layout')
@section('content')
<form method="post" action="/editLembaga" id="editForm" enctype="multipart/form-data">
<div id=content class=main-container>
        <div id=vendor-page>
            <section class=page-heading>
                <div onclick="document.getElementById('coverPic').click();" class="heading-img img-fit-wrapper">
                    <img src={{asset('images/'.$merchant->coverImage)}} alt="{{$merchant->name}}"></div>
                <nav class="vendor-masthead page-masthead">
                    <div class="vendor-header-content page-header-content">
                        <div class=container>
                            <div class=row>
                                <div class="col-sm-4 col-sm-push-4">
                                    <div class=vendor-pic-wrapper>
                                        <figure onclick="document.getElementById('profilePic').click();" class="vendor-pic img-fit-wrapper"><img src={{asset('images/'.$merchant->profileImage)}} alt="{{$merchant->name}}"></figure>
                                        <h2 class="vendor-name"><input name="name" value="{{$merchant->name}}"></h2>
                                        <i class=vendor-about><input name="categories" value="{{$merchant->categories}}"></i></div>
                                </div>
                                <div class="col-sm-4 col-sm-push-4">
                                        <button onclick="document.getElementById('editForm').submit();" style="background-color:green; color:white;" class="btn btn-transparent  navbar-btn form-report-vendor_open"><i class="fas fa-save"></i> Save</button>
                                </div>
                                <div class="col-sm-4 col-sm-pull-8">
                                <a @if(Auth::check()) href="/chat/{{$merchant->id}}" @else href="{{route('login')}}" @endif  style="background-color:blue; color:white;" class="btn btn-transparent  navbar-btn form-report-vendor_open"><i class="fas fa-envelope"></i> Konsultasi</a>
                            </div>
                        </div>
                    </div>
                    <div class="vendor-menu page-header-menu">
                        <div class=container>
                            <ul class="nav navbar-nav">
                            <li><a href="/merchant-dashboard">Dashboard</a></li>
                            <li class=active><a href="/showLembaga/{{$merchant->id}}">Profil</a></li>
                            <!--<li><a href="/editGaleri/">Galeri</a></li> -->
                            </ul>
                        </div>
                    </div>
                </nav>
            </section>
            <div class="container vendor-tab-content vendor-profile">
                
                    @csrf
                <input type="hidden" name="merchantId" value="{{$merchant->id}}">
                    <input type="file" style="display:none" name="profilePic" id="profilePic" onchange="document.getElementById('editForm').submit();">
                    <input type="file" style="display:none" name="coverPic" id="coverPic" onchange="document.getElementById('editForm').submit();">
                    <div class=vendor-location><strong>Email:</strong> <input name="email" value="{{$merchant->email}}"></div>
                <div class=vendor-location><strong>Kota:</strong> <input name="city" value="{{$merchant->city}}"></div>
                <div class=vendor-location><strong>Tipe Lembaga:</strong> <input name="businessType" value="{{$merchant->businessType}}"> </div>
                <div class=vendor-location><strong>Nomor HP:</strong> <input name="phone" value="{{$merchant->phone}}"> </div>
                <div class=vendor-location><strong>Alamat Lembaga:</strong> <input name="address" value="{{$merchant->address}}"></div>
                <div class=vendor-location><strong>Kode Pos:</strong> <input name="postCode" value="{{$merchant->postCode}}"></div>
                <p class=vendor-description>
                    Deskripsi :<br>
                    <textarea name="description">{{$merchant->description}}</textarea>
                </p>
                </form>
                <?php
                    $berkas = json_decode($merchant->berkas);
                    foreach ($berkas as $file) {?>
                        <img width="700" src="<?php echo asset('/images') ?>/<?php echo $file ?>" style="padding:30px;">
                        <hr>
                        <?php  }
                ?>
            </div>
        </div>
    </div>
    @endsection