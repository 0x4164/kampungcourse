<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\WishlistController;

Route::get('/', 'HomeController@index')->name('home');


Route::get('/main', function () {
    return view('home.layout');
});

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('learner-dashboard');


/*Route::get('/admin', function(){
    return view('home');
})->name('adminpage')->middleware('auth:merchant');*/
Route::post('image-upload', 'ImageUploadController@imageUploadPost')->name('image.upload.post');
Route::get('merchant-login','Auth\MerchantLoginController@showLoginForm')->name('merchant.login');
Route::post('merchant-login', ['as' => 'merchant-login', 'uses' => 'Auth\MerchantLoginController@login']);
Route::get('merchant-register','Auth\MerchantLoginController@showRegisterPage')->name('merchant-register');
Route::post('merchant-register', 'Auth\MerchantLoginController@register')->name('merchant.register');
Route::get('/account', function(){
    $data['title'] = "Dashboard";
    return view('dashboard.user')->with($data);
})->name('learner-dashboard')->middleware('auth');
Route::get('/admin-dashboard', function(){
    $data['title'] = "Dashboard";
    return view('dashboard.admin')->with($data);
})->name('adminpage')->middleware('auth:admin');
Route::get('/merchant-dashboard', function(){
    $data['title'] = "Dashboard";
    return view('dashboard.merchant')->with($data);
})->name('merchantpage')->middleware('auth:merchant');
Route::get('/aboutus', function(){
    $data['title'] = "Tentang Kami";
    return view('aboutus.aboutus')->with($data);
})->name('aboutus');
Route::get('/viewcourse', function(){
    $data['title'] = "Kursus";
    return view('course.view')->with($data);
})->name('viewcourse');
Route::get('/forgotpassword', function(){
    $data['title'] = "Lupa Password";
    return view('login.forgotPassword')->with($data);
})->name('forgotPassword');
Route::get('/invoice', "CourseController@invoice")->name('invoice');
Route::post('/uploadBuktiTransfer', "CourseController@uploadTransfer");
Route::post('/submitTestimony', "CourseController@submitTestimony");
Route::get('/verifPendaftaran/{id}', "CourseController@verifikasiPendaftaran");
Route::get('/history', 'CourseController@history')->name('history');
Route::get('/wishlist', "WishlistController@index")->name('wishlist');
Route::get('/verifyMerchant', function(){
    $data['title'] = "Verifikasi Lembaga";
    return view('admin.merchantVerify')->with($data);
})->name('verifyMerchant');
Route::get('/omzet', function(){
    $data['title'] = "Analisis Omzet";
    return view('admin.omzet')->with($data);
})->name('omzet');
Route::get('/verify', function(){
    $data['title'] = "Verifikasi Iklan";
    return view('admin.verify')->with($data);
})->name('verifikasi');
Route::get('/kelolaKursus', function(){
    $data['title'] = "Kelola Kursus";
    return view('merchant.kelolaKursus')->with($data);
})->name('kelolaKursus');
Route::get('admin-login','Auth\AdminLoginController@showLoginForm');
Route::post('admin-login', ['as' => 'admin-login', 'uses' => 'Auth\AdminLoginController@login']);
Route::get('admin-register','Auth\AdminLoginController@showRegisterPage');
Route::post('admin-register', 'Auth\AdminLoginController@register')->name('admin.register');
Route::get('/admin-verifikasi', 'CourseController@verifCourse' )->name('admin_verify');
Route::get('/admin-omzet', "CourseController@admin_omzet")->name('admin_omzet');
Route::post('/admin-omzet', "CourseController@count_admin_omzet");
Route::get('/admin-promotion', function(){
    $data['title'] = "Kelola Promosi";
    return view('dashboard.admin_promotion')->with($data);
})->name('admin_promotion');
Route::get('/admin-marketing', function(){
    $data['title'] = "Marketing";
    return view('dashboard.admin_marketing')->with($data);
})->name('admin_marketing');
Route::get('/admin-blog', 'BlogController@index')->name('admin_blog');
Route::get('/merchant-omzet', "CourseController@omzet")->name('merchant_omzet');
Route::post('/merchant-omzet', "CourseController@count_omzet")->name('merchant_omzet');
Route::get('/merchant-participant', function(){
    $data['title'] = "Data Peserta";
    return view('dashboard.merchant_participant')->with($data);
})->name('merchant_participant');
Route::get('/merchant-course', 'CourseController@merchantCourse')->name('merchant_course');
Route::get('/merchant-account', 'CourseController@redirectEditMerchant')->name('merchant_account');
Route::get('posts/{id}', 'BlogController@show')->name('blog.show');
Route::post('posts/{id}', 'BlogController@update')->name('blog.update');
Route::delete('posts/{id}', 'BlogController@destroy')->name('blog.delete');
Route::get('posts/{id}/edit', 'BlogController@edit')->name('blog.edit');
Route::get('post/create', 'BlogController@create')->name('blog.create');
Route::post('post/store', 'BlogController@store')->name('blog.store');

Route::resource('course', 'CourseController');
Route::get('/merchant-update', function(){
    return redirect('merchant-account')->with('success', 'Profil Berhasil Di Update');
})->name('merchant.update');
Route::get('/history-testimony', function(){
    return redirect('history')->with('success', 'Testimoni Telah Dikirim');
})->name('history.testimony');
Route::get('/edit-account', function(){
    $data['title'] = "Edit Akun";
    return view('dashboard.account')->with($data);
})->name('edit.account');
Route::post('/edit-account-save', 'Auth\RegisterController@updateProfile')->name('user.update');
Route::post('/merchant-omzet-filter', 'HomeController@countOmzet')->name('count.omzet');

Route::get('showposts/{id}', 'HomeController@showBlog')->name('post.show');
Route::get('category/english', 'CourseController@categoryEnglish')->name('course.english');
Route::get('category/arab', function(){
    $data['title'] = "Kategori Bahasa Arab";
    return view('course.category_arab')->with($data);
})->name('course.arab');
Route::get('category/mandarin', function(){
    $data['title'] = "Kategori Bahasa Mandarin";
    return view('course.category_mandarin')->with($data);
})->name('course.mandarin');
Route::get('course', 'CourseController@index')->name('course.index');
Route::get('registerCourse', 'CourseController@register')->name('course.register');
Route::post('registerCourse', 'CourseController@registerCourse')->name('register.course');

Route::post('/success', 'SendEmailController@sendInvoice')->name('regist.success');
Route::get('/blog', 'BlogController@blog')->name('blog');

Route::get('/lembaga', 'CourseController@lembaga')->name('merchant');

Route::get('search/{key}', 'CourseController@search')->name('search');

Route::get('chat', 'ChatController@index')->name('chat');

Route::get('transaction', function(){
    $data['title'] = "Transaksi";
    return view('dashboard.transaction')->with($data);
})->name('transaction');

Route::get('category/filter', 'CourseController@filter')->name('course.filter');
Route::get('lembaga-show', function(){
    $data['title'] = "Lembaga";
    return view('admin.merchantVerify')->with($data);
})->name('lembaga');
Route::get('lembaga-course', function(){
    $data['title'] = "Kursus Lembaga";
    return view('admin.course')->with($data);
})->name('lembaga.kursus');
Route::get('lembaga-galeri', function(){
    $data['title'] = "Galeri Lembaga";
    return view('admin.gallery')->with($data);
})->name('lembaga.gallery');
Route::get('lembaga-review', function(){
    $data['title'] = "Review Lembaga";
    return view('admin.review')->with($data);
})->name('lembaga.review');

Route::get('/showVerifyMerchant/{id}', 'CourseController@showVerifyMerchant');
Route::get('/showVerifyMerchantinf/{id}', 'CourseController@showVerifyMerchantinf');
Route::get('/showVerifyMerchantberkas/{id}', 'CourseController@showVerifyMerchantberkas');
Route::get('/verifyMerchant/{id}', 'CourseController@verifyMerchant');
Route::get('/verifyCourse/{id}', 'CourseController@verifyCourse');
Route::get('/unverifyMerchant/{id}', 'CourseController@unverifyMerchant');
Route::get('/deleteCourse/{id}', 'CourseController@delete');
Route::get('/deleteSlide/{id}', 'CourseController@deleteSlide');
Route::get('/deleteFlash/{id}', 'CourseController@deleteFlash');
Route::get('/deleteTop/{id}', 'CourseController@deleteTop');
Route::get('/deleteRecommended/{id}', 'CourseController@deleteRecommended');
Route::get('/addWishlist/{courseId}', 'WishlistController@addWishlist');
Route::get('/removeWishlist/{courseId}', 'WishlistController@removeWishlist');

Route::get('/editLembaga/{id}', 'CourseController@editLembaga');
Route::post('/editLembaga', 'CourseController@saveEditLembaga');
Route::get('/showLembaga/{id}', 'CourseController@showLembaga');
Route::get('/showLembagaKursus/{id}', 'CourseController@showLembagaKursus');
Route::get('/showLembagaGaleri/{id}', 'CourseController@showLembagaGaleri');
Route::get('/showLembagaReview/{id}', 'CourseController@showLembagaReview');

Route::get('/addPromotion/{id}', 'CourseController@promote');

Route::get('/chat/{id}', 'ChatController@chat');
Route::get('/chatWith/{id}', 'ChatController@chatWith');
Route::get('/chatFrom/{id}', 'ChatController@chatFrom');
Route::post('/sendChat', 'ChatController@sendChat');
Route::post('/getMessage', 'ChatController@getMessage');

Route::get('/kelolaSlider', 'CourseController@kelolaSlider');
Route::post('/promoteSlider', 'CourseController@promoteSlider');
Route::post('/promoteFlash', 'CourseController@promoteFlash');
Route::post('/promoteTop', 'CourseController@promoteTop');
Route::post('/promoteRecommended', 'CourseController@promoteRecommended');
Route::get('/kelolaTopKursus', 'CourseController@kelolaTopKursus');
Route::get('/kelolaRecommendedKursus', 'CourseController@kelolaRecommendedKursus');
Route::get('/verifikasiSlider/{id}', 'CourseController@verifikasiSlider');
Route::get('/verifikasiFlash/{id}', 'CourseController@verifikasiFlash');
Route::get('/verifikasiTop/{id}', 'CourseController@verifikasiTop');
Route::get('/verifikasiRecommended/{id}', 'CourseController@verifikasiRecommended');
Route::get('/flashsaleConfig', 'CourseController@flashsale');
Route::post('/flashsaleConfig', 'CourseController@flashsaleUpdate');


Route::post('/sendMail', 'SendEmailController@send');
Route::post('/sendNotification', 'NotificationController@store');

Route::get('showParticipant', function(){
    $data['title'] = "Daftar Peserta Kursus";
    return view('course.participant')->with($data);
})->name('showParticipant');

Route::get('generate-pdf','HomeController@generatePDF');
Route::post('/downloadInvoice','CourseController@downloadInvoice');
Route::get('/courseDate/{id}', 'CourseController@courseDate');
Route::post('/addCourseDate','CourseController@addCourseDate');

//tes
Route::get('/tes', 'HomeController@tes');
Route::get('/authBRI', 'HomeController@authBRI');
Route::get('/generateSignature', 'HomeController@generateSignature');
Route::get('/accountInfo', 'HomeController@accountInfo');

Route::get('/trfValidate', 'HomeController@trfValidate');
Route::get('/transfer', 'HomeController@transfer');
