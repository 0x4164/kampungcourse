<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $table = 'blogs';
    public $primaryKey = 'id';
    public $timestamps = true;

    public function admin()
    {
        return $this->belongsTo('App\Admin');
    }
}
