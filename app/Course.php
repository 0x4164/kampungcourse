<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table = 'courses';
    public $primaryKey = 'id';
    public $timestamps = true;
    public function merchant()
    {
        return $this->belongsTo('App\Merchant');
    }

    public function sliders()
    {
      return $this->hasMany('App\slider');
    }

    public function flashes()
    {
      return $this->hasMany('App\Flash');
    }

    public function tops()
    {
      return $this->hasMany('App\Top');
    }

    public function Recommended()
    {
      return $this->hasMany('App\Recommended');
    }

    public function wishlists()
    {
      return $this->hasMany('App\Wishlist');
    }

    public function invoices()
    {
      return $this->hasMany('App\Invoice');
    }

    public function coursedate()
    {
      return $this->hasMany('App\CourseDate');
    }
}
