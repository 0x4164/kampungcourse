<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Merchant extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','userId','phone','coverImage','profileImage','businessType','categories','description','maps','address','postCode','city','gallery','ktp','berkas','status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function courses()
    {
      return $this->hasMany('App\Course');
    }

    public function sliders()
    {
      return $this->hasMany('App\slider');
    }

    public function flashes()
    {
      return $this->hasMany('App\Flash');
    }

    public function tops()
    {
      return $this->hasMany('App\Top');
    }

    public function Recommended()
    {
      return $this->hasMany('App\Recommended');
    }

    public function invoices()
    {
      return $this->hasMany('App\Invoice');
    }
}
