<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class chatlist extends Model
{
    protected $table = 'chatlists';
    public $primaryKey = 'id';
    public $timestamps = true;
}
