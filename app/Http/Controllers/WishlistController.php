<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use App\Wishlist;

class WishlistController extends Controller
{
    public function addWishlist($courseId)
    {

        $wishlist = new Wishlist();
        $wishlist->user_id = auth()->user()->id;
        $wishlist->course_id = $courseId;
        $wishlist->save();
        return redirect('/wishlist')->with('success', 'Wishlist Berhasil Ditambahkan');
    }

    public function removeWishlist($courseId)
    {

        $wishlist =  Wishlist::destroy($courseId);
        if ($wishlist) {
            return redirect('/wishlist')->with('success', 'Wishlist Berhasil Dihapus');
        } else {
            return redirect('/wishlist')->with('error', 'Penghapusan Wishlist Gagal');
        }
    }

    public function index(Request $request)
    {
        $wishlist = Course::whereHas('wishlists', function($q){
            $q->where('user_id', '=', auth()->user()->id);
        })->with('merchant')->with('wishlists')->paginate(25);
        $kategori = $request->input('kategori');
        $metode = $request->input('metode');
        $periode = $request->input('periode');
        $searchKey = $request->input('searchKey');
        //$lokasi = $request->input('lokasi');
        //$promo = $request->input('promo');
        $data['searchKey'] = $searchKey;
        $data['kategori'] = $kategori;
        $data['metode'] = $metode;
        $data['periode'] = $periode;
        $data['wishlist'] = $wishlist;
        $data['title'] = "Wishlist";
        return view('course.wishlist')->with($data);
    }
}
