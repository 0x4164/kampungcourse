<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notifikasi;
use App\User;
use App\Merchant;
use App\Admin;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use AuthenticatesUsers;
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'pesan' => 'required',
            'link' => 'required',
            'to' => 'required'
        ]);
        switch($request->input('to'))
        {
            case 'peserta' :
                $dikirimke = User::all();
                foreach($dikirimke as $kirim)
                {
                    $notifikasi = new Notifikasi;
                    $notifikasi->message = $request->input('pesan');
                    $notifikasi->link = $request->input('link');
                    $notifikasi->user_id = $kirim->id;
                    $notifikasi->role = 'peserta';
                    $notifikasi->save();
                }
                break;
            case 'lembaga' :
                $dikirimke = Merchant::all();
                foreach($dikirimke as $kirim)
                {
                    $notifikasi = new Notifikasi;
                    $notifikasi->message = $request->input('pesan');
                    $notifikasi->link = $request->input('link');
                    $notifikasi->user_id = $kirim->id;
                    $notifikasi->role = 'lembaga';
                    $notifikasi->save();
                }
                break;
            case 'semua' :
                $dikirimke = Merchant::all();
                foreach($dikirimke as $kirim)
                {
                    $notifikasi = new Notifikasi;
                    $notifikasi->message = $request->input('pesan');
                    $notifikasi->link = $request->input('link');
                    $notifikasi->user_id = $kirim->id;
                    $notifikasi->role = 'lembaga';
                    $notifikasi->save();
                }
                $dikirimke2 = User::all();
                foreach($dikirimke2 as $kirim)
                {
                    $notifikasi = new Notifikasi;
                    $notifikasi->message = $request->input('pesan');
                    $notifikasi->link = $request->input('link');
                    $notifikasi->user_id = $kirim->id;
                    $notifikasi->role = 'peserta';
                    $notifikasi->save();
                }
                break;
        }
        
        return redirect('admin-marketing')->with('success', 'Notifikasi Berhasil Dikirim');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public static function show()
    {
        if(auth('merchant')->check()){
            $notif = Notifikasi::where('user_id', auth('merchant')->id())->where('role', 'lembaga')->get();
        }
        else if(auth()->check()){
            $notif = Notifikasi::where('user_id', auth()->id())->where('role', 'peserta')->get();
        }else{
            $notif = null;
        }
        return $notif;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
