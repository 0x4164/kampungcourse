<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Merchant;
use App\User;
use App\chatlist;
use App\chat;

class ChatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public static function read()
    {
        if (auth('merchant')->check()) {
            $chat = chatlist::where('to', auth('merchant')->id())->where('status2', 'unread')->get();
        } else if (auth()->check()) {
            $chat = chatlist::where('user_id', auth()->id())->where('status', 'unread')->get();
        } else {
            $chat = null;
        }
        return $chat;
    }

    public function chat($id)
    {
        $list = chatlist::where('to', $id)->where('user_id', auth()->id())->get();
        if (count($list) > 0) {
            $list[0]->touch();
            $list[0]->status = 'unread';
            $list[0]->status2 = 'unread';
        } else {
            $list = new chatlist();
            $list->user_id = auth()->id();
            $list->to = $id;
            $list->status = 'unread';
            $list->status2 = 'unread';
            $list->save();
        }
        return redirect('chat');
    }

    public static function getMerchantData($id)
    {
        $data = Merchant::find($id);
        return $data;
    }

    public static function getUserData($id)
    {
        $data = User::find($id);
        return $data;
    }

    public function chatWith($id)
    {
        $data['to'] = Merchant::find($id);
        $data['title'] = "Chat: " . $data['to']->name;
        $data['chat'] = chat::where('user_id', auth()->id())->where('merchant_id', $id)->orderBy('created_at', 'asc')->limit(20)->get();
        $chatlist = chatlist::where('user_id', auth()->id())->where('to', $id)->get();
        
        foreach ($chatlist as $list) {
            $list->status = 'read';
            $list->save();
        }

        return view('dashboard.chatroom')->with($data);
    }

    public function chatFrom($id)
    {
        $data['to'] = User::find($id);
        $data['title'] = "Chat: " . $data['to']->name;
        $data['chat'] = chat::where('user_id', $id)->where('merchant_id', auth('merchant')->id())->orderBy('created_at', 'asc')->limit(20)->get();
        $chatlist = chatlist::where('user_id', $id)->where('to', auth('merchant')->id())->get();
        
        foreach ($chatlist as $list) {
            $list->status2 = 'read';
            $list->save();
        }
        return view('dashboard.chatroom')->with($data);
    }

    public function sendChat(Request $request)
    {
        $list = chatlist::where('to', $request->input('merchant_id'))->where('user_id', $request->input('user_id'))->get();

        $chat = new chat();
        $chat->message = $request->input('message');
        $chat->merchant_id = $request->input('merchant_id');
        $chat->user_id = $request->input('user_id');
        if (auth()->check()) {
            $chat->dari = 'peserta';
            $list[0]->status2 = 'unread';
            $list[0]->save();
        }
        if (auth('merchant')->check()) {
            $chat->dari = 'lembaga';
            $list[0]->status = 'unread';
            $list[0]->save();
        }
        $chat->save();
        return response()->json([
            'success' => true
        ]);
    }

    public function getMessage(Request $request)
    {
        $date = $request->input('lastCheck');
        //$to = '2019-10-14 18:28:06';
        $chat = chat::where('created_at', '>=', $date)->get(); //chat::whereBetween('created_at', [$date, $to])->get(); //
        return response()->json([
            'success' => true,
            'chat' => json_encode($chat),
            'from' => $date,
        ]);
    }

    public function index()
    {
        if (auth()->check()) {
            $data['title'] = "Chat Room";
            $data['chatList'] = chatlist::where('user_id', auth()->id())->get();
            /*foreach ($data['chatList'] as $list) {
                $list->status = 'read';
                $list->save();
            }*/
            return view('dashboard.chat')->with($data);
        }
        if (auth('merchant')->check()) {
            $data['title'] = "Chat Room";
            $data['chatList'] = chatlist::where('to', auth('merchant')->id())->get();
            /*foreach ($data['chatList'] as $list) {
                $list->status2 = 'read';
                $list->save();
            }*/
            return view('dashboard.chat')->with($data);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
