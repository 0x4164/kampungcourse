<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use App\User;
use App\Merchant;
use App\Participant;
use App\Invoice;

class SendEmailController extends Controller
{
    function send(Request $request)
    {
        $this->validate($request, [
            'message' =>  'required'
        ]);

        $data = array(
            'name'      =>  'Kampung Course',
            'message'   =>   $request->message
        );
        switch ($request->input('recipient')) {
            case 'lembaga':
                $dikirimke = Merchant::all();
                foreach ($dikirimke as $kirim) {
                    Mail::to($kirim->email)->send(new SendMail($data));
                }
                break;
            case 'peserta':
                $dikirimke = User::all();
                foreach ($dikirimke as $kirim) {
                    Mail::to($kirim->email)->send(new SendMail($data));
                }
                break;
        }
        return back()->with('success', 'Email Telah Terkirim');
    }
    function sendInvoice(Request $request)
    {



        $this->validate($request, [
            'email' => 'required',
            'message' =>  'required'
        ]);

        $data = array(
            'name'      =>  'Kampung Course',
            'message'   =>   $request->message
        );
        Mail::to($request->input('email'))->send(new SendMail($data));

        $invoice = new Invoice();
        $invoice->course_id = $request->input("courseId");
        $invoice->user_id = auth()->user()->id;
        $invoice->merchant_id = $request->input("merchantId");
        $invoice->total = $request->input("total");
        $invoice->periode = $request->input("periode");
        $invoice->start_date = $request->input('startDate');
        $invoice->status = "Unverified";
        $invoice->bukti = "empty";
        $invoice->invoice_message = $request->input('message');
        $invoice->testimony = "empty";
        $invoice->save();
        $invoiceId = $invoice->id;

        $pesan = $request->input('sms');
        for ($i = 1; $i <= $request->input('quantity'); $i++) {
            $participant = new Participant();
            $participant->name = $request->input("nama_peserta$i");
            $participant->gender = $request->input("participant_gender$i");
            $participant->course_id = $request->input("courseId");
            $participant->merchant_id = $request->input("merchantId");
            $participant->start_date = $request->input('startDate');
            $participant->periode = $request->input("periode");
            $participant->invoice_id = $invoiceId;
            $participant->save();
            $data = array(
                'name'      =>  'Kampung Course',
                'message'   =>   $request->mailto
            );
            $recipient = $request->input("participant_email$i");

            Mail::to($recipient)->send(new SendMail($data));
            $phone = $request->input('phone_peserta' . $i);


            $url = 'https://reguler.zenziva.net/apps/smsapi.php';
            $query_array = array(
                'userkey' => 'l6ocki',
                'passkey' => 'ceabd0a34bde5902f2c8a2da',
                'nohp' => $phone,
                'pesan' => $pesan
            );

            $query = http_build_query($query_array);
            $result = file_get_contents($url . '?' . $query);
        }
        return redirect('/');
    }
}
