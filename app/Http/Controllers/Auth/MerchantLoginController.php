<?php
namespace App\Http\Controllers\Auth;
use App\Merchant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
class MerchantLoginController extends Controller
{
    use AuthenticatesUsers;
    protected $guard = 'merchant';
    protected $redirectTo = '/home';
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function showLoginForm()
    {
        $data['title'] = "Login";
        return view('login.educatorsLogin')->with($data);
    }
    public function guard()
    {
        return auth()->guard('merchant');
    }
    public function showRegisterPage()
    {
        $data['title'] = "Register";
        return view('login.educatorsRegister')->with($data);
    }
    public function register(Request $request)
    {
        $count = 0;
        $galery = array();
        $berkas = array();
        foreach($request->file('galery') as $image){
            $imageName = time().$count.'.'.$image->getClientOriginalExtension();
            $image->move(public_path('images'), $imageName);
            $galery[] = $imageName;  
            $count++;
        }
        foreach($request->file('berkas') as $image){
            $imageName = time().$count.'.'.$image->getClientOriginalExtension();
            $image->move(public_path('images'), $imageName);
            $berkas[] = $imageName;  
            $count++;
        }
        Merchant::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'coverImage' => $request->cover,
            'profileImage' => $request->logo,
            'businessType' => $request->businessType,
            'phone' => $request->phone,
            'categories' => $request->categories,
            'description' => $request->description,
            'maps' => $request->maps,
            'address' => $request->address,
            'postCode' => $request->postCode,
            'city' => $request->city,
            'ktp' => $request->id_card_url,
            'gallery' => json_encode($galery),
            'berkas' => json_encode($berkas),
            'status' => 'Unverified'
        ]);
        return response()->json([
            'success' => true
        ]);
    }
    public function login(Request $request)
    {
        if (auth()->guard('merchant')->attempt(['email' => $request->email, 'password' => $request->password ])) {
            return redirect()->route('merchantpage');
        }
        return back()->withErrors(['email' => 'Email atau Password yang anda masukkan salah']);
    }
}