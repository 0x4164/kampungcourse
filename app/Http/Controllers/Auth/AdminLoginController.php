<?php
namespace App\Http\Controllers\Auth;
use App\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
class AdminLoginController extends Controller
{
    use AuthenticatesUsers;
    protected $guard = 'admin';
    protected $redirectTo = '/home';
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function showLoginForm()
    {
        $data['title'] = "Login";
        return view('login.adminLogin')->with($data);
    }
    public function guard()
    {
        return auth()->guard('admin');
    }
    public function showRegisterPage()
    {
        $data['title'] = "Register";
        return view('login.educatorsRegister')->with($data);
    }
    public function register(Request $request)
    {
        $stringInput = 'Not Inserted';
        $request->validate([
            'name' => 'required|string|max:199',
            'email' => 'required|string|email|max:255|unique:merchants',
            'password' => 'required|string|min:6|confirmed'
        ]);
        Merchant::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'coverImage' => $stringInput,
            'profileImage' => $stringInput,
            'businessType' => $stringInput,
            'phone' => $stringInput,
            'categories' => $stringInput,
            'description' => $stringInput,
            'maps' => $stringInput,
            'address' => $stringInput,
            'postCode' => $stringInput,
            'city' => $stringInput,
        ]);
        return redirect()->route('admin-login')->with('success','Registration Success');
    }
    public function login(Request $request)
    {
        if (auth()->guard('admin')->attempt(['email' => $request->email, 'password' => $request->password ])) {
            return redirect()->route('adminpage');
        }
        return back()->withErrors(['email' => 'Email atau Password yang anda masukkan salah']);
    }
}