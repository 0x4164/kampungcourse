<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;
use App\Course;
use App\CourseDate;
use App\User;
use App\Merchant;
use App\slider;
use App\Flash;
use App\Top;
use App\Recommended;
use App\Invoice;
use App\Participant;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Hash;
use App\Services\BRIService as BRIService;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    { }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['title'] = "Beranda";
        $data['sliders'] = slider::all();
        $data['flashs'] = Course::with('flashes')->get();
        $data['tops'] = Course::with('tops')->get();
        $data['recommendeds'] = Course::with('recommended')->get();
        $data['newest'] = Course::where('status', 'terverifikasi')->orderBy('created_at', 'asc')->take(10)->get();
        $data['blog'] = Blog::with('admin')->orderBy('created_at', 'asc')->take(5)->get()->toArray();
        return view('home.layout')->with($data);
    }

    public function success()
    {
        return redirect('/')->with('success', 'Pendaftaran Kursus Berhasil');
    }

    public function countOmzet(Request $request)
    {
        $data['title'] = "Omzet";
        $data['bulan'] = $request->input('bulan');
        $data['tahun'] = $request->input('tahun');
        $data['periode'] = $request->input('periode');
        return view('dashboard.merchant_omzet')->with($data);
    }

    public function showBlog($id)
    {
        $data['post'] = Blog::find($id);
        $data['title'] = $data['post']->title;
        return view('blog.show')->with($data);
    }

    public function generatePDF()
    {
        $data = ['title' => 'Welcome to HDTuto.com'];
        $pdf = PDF::loadView('myPDF', $data);
  
        return $pdf->download('itsolutionstuff.pdf');
    }

    public function authBRI(){
        $bri=new BRIService();
        echo $bri->authBRI();
    }
    public function generateSignature(){
        $bri=new BRIService();
        echo $bri->generateSignature();
    }
    public function accountInfo(){
        $bri=new BRIService();
        echo $bri->accountInfo();
    }
    public function trfValidate(){
        $bri=new BRIService();
        echo $bri->trfValidate();
    }
    public function transfer(){
        $bri=new BRIService();
        echo $bri->transfer();
    }

    public function tes(){
        // $pass="12312312";
        // $p=Hash::make($pass);
        // echo $p;

        BRIService::d1();
    }

    /*
     path=/v1/inquiry/888801000157508&verb=GET&token=Bearer R04XSUbnm1GXNmDiXx9ysWMpFWBr&timestamp=2019-01-02T13:14:15.678Z&body=
     */
}
