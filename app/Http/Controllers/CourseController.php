<?php

namespace App\Http\Controllers;

use App\Course;
use App\CourseDate;
use App\User;
use App\Merchant;
use App\slider;
use App\Flash;
use App\Top;
use App\Recommended;
use App\Invoice;
use App\Participant;
use Barryvdh\DomPDF\Facade as PDF;


use Illuminate\Http\Request;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = "Daftar Kursus";
        $data['courses'] = Course::with('merchant')->get();
        return view('course.index')->with($data);
    }

    public function downloadInvoice(Request $request)
    {
        $data = ['message' => $request->input('message')];
        $pdf = PDF::loadView('myPDF', $data);

        return $pdf->download('invoice.pdf');
    }

    public function showVerifyMerchant($id)
    {
        $data['title'] = "Verifikasi Lembaga";
        $data['merchant'] = Merchant::find($id);
        return view('admin.verifyMerchant')->with($data);
    }

    public function showVerifyMerchantinf($id)
    {
        $data['title'] = "Verifikasi Lembaga";
        $data['merchant'] = Merchant::find($id);
        return view('admin.verifyMerchantinf')->with($data);
    }

    public function showVerifyMerchantberkas($id)
    {
        $data['title'] = "Verifikasi Lembaga";
        $data['merchant'] = Merchant::find($id);
        return view('admin.verifyMerchantberkas')->with($data);
    }

    public function categoryEnglish()
    {
        $data['title'] = "Kategori Bahasa Inggris";
        $data['courses'] = Course::with('merchant')->where('category', 'Bahasa Inggris')->get();
        return view('course.categoryEnglish')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function filter(Request $request)
    {

        $data['title'] = "Pencarian";
        $kategori = $request->input('kategori');
        $metode = $request->input('metode');
        $periode = $request->input('periode');
        $searchKey = $request->input('searchKey');
        //$lokasi = $request->input('lokasi');
        //$promo = $request->input('promo');
        $data['searchKey'] = $searchKey;
        $data['kategori'] = $kategori;
        $data['metode'] = $metode;
        $data['periode'] = $periode;

        $course = Course::query();
        $course->with('merchant');
        if ($kategori != 'Semua Bahasa') {
            $course = $course->where('category', 'like', '%' . $kategori . '%');
        }
        if ($metode != 'Semua Metode') {
            $course = $course->where('method', 'like', '%' . $metode . '%');
        }
        if ($periode != 'Semua Periode') {
            $course = $course->where('duration', 'like', '%' . $periode . '%');
        }
        if (!empty($searchKey)) {
            $course = $course->where('title', 'like', '%' . $searchKey . '%');
            $merchant = Course::whereHas('merchant', function ($query) use ($searchKey) {
                $query->where('name', 'like', '%' . $searchKey . '%');
            });
            $course->union($merchant);
            $data['search'] = true;
        }
        /*if($kategori != 'Semua Periode'){
            $course = $course->where('method','like', '%'.$periode.'%');
        }
        if($lokasi[0] != 'Semua Lokasi'){
            foreach($lokasi as $lok){
                $course = $course->where('method','like', '%'.$metode.'%');
            }
            
        }
        
        
        if(!empty($searchKey)){
            $data['searchKey'] = $searchKey;
            $data['search'] = true;
            $course->where('title','like', '%'.$searchKey.'%');
        }*/
        $data['courses'] = $course->paginate(25);

        return view('course.search')->with($data);
    }

    public function search($key)
    {
        $data['title'] = "Pencarian - " . $key;
        $data['courses'] = Course::with('merchant')->where('title', 'like', '%' . $key . '%')->get();
        $data['searchKey'] = $key;
        $data['search'] = true;
        return view('course.search')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'category' => 'required',
            'tag' => 'required',
            'metode' => 'required',
            'duration' => 'required',
            'total' => 'required',
            'kuota' => 'required',
            'level' => 'required',
            'penjelasan' => 'required',
            'jadwal' => 'required',
            'fasilitas' => 'required',
            'persyaratan' => 'required',
            'date' => 'required',
            'harga' => 'required',
            'cover_image' => 'required'
        ]);
        $count = 0;
        foreach ($request->file('cover_image') as $image) {
            $imageName = time() . $count . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images'), $imageName);
            $data[] = $imageName;
            $count++;
        }

        $course = new Course;
        $course->title = $request->input('title');
        $course->category = $request->input('category');
        $course->tag = $request->input('tag');
        $course->method = $request->input('metode');
        $course->duration = $request->input('duration');
        $course->capacity = $request->input('total');
        $course->quota = $request->input('kuota');
        $course->level = $request->input('level');
        $course->description = $request->input('penjelasan');
        $course->schedule = $request->input('jadwal');
        $course->facility = $request->input('fasilitas');
        $course->terms = $request->input('persyaratan');
        $course->start_date = $request->input('date');
        $course->price = $request->input('harga');
        $course->status = 'diajukan';
        $course->merchant_id = auth()->guard('merchant')->user()->id;
        $course->thumbnail = json_encode($data);
        $course->save();

        return redirect('merchant-course')->with('success', 'Kursus Baru Berhasil Diajukan Untuk Diverifikasi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $data['course'] =  Course::with('merchant')->with('courseDate')->find($id);
        $data['testimonies'] = $data['invoices'] = Course::with('invoices')->whereHas('invoices', function ($query) use ($id) {
            $query->where('course_id',  $id);
            $query->where('testimony', '!=',  'empty');
        })->paginate(10);
        $data['title'] = $data['course']->title;
        return view('course.view')->with($data);
    }

    public function register()
    {
        $data['title'] = 'Checkout';
        return view('course.checkout')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['course'] =  Course::with('merchant')->find($id);
        $data['title'] = $data['course']->title;
        return view('course.edit')->with($data);
    }

    public function verifCourse()
    {
        $data['unverifiedCourse'] = Course::with('merchant')->where('status', 'diajukan')->paginate(10);
        $data['unverifiedMerchant'] = Merchant::where('status', 'Unverified')->paginate(10);
        $data['unverifiedSlider'] = slider::with(['course', 'merchant'])->where('status', 'unverified')->paginate(10);
        $data['unverifiedFlash'] = Flash::with(['course', 'merchant'])->where('status', 'unverified')->paginate(10);
        $data['unverifiedTop'] = Top::with(['course', 'merchant'])->where('status', 'unverified')->paginate(10);
        $data['unverifiedRecommended'] = Recommended::with(['course', 'merchant'])->where('status', 'unverified')->paginate(10);
        $data['unverifiedParticipant'] = $data['invoices'] = Course::with('invoices')->whereHas('invoices', function ($query) {
            $query->where('status',  'Unverified');
        })->paginate(10);
        $data['title'] = "Verifikasi";
        return view('dashboard.admin_verify')->with($data);
    }


    public function promote($id)
    {
        $data['course'] =  Course::with('merchant')->find($id);
        $data['title'] = 'Ajukan Promosi';
        return view('dashboard.makePromotion')->with($data);
    }

    public function flashsale()
    {
        $data['title'] = 'Pengaturan Flashsale';
        $data['flashsale'] = Flash::with(['course', 'merchant'])->where('status', 'verified')->get();
        return view('dashboard.flashsaleConfig')->with($data);
    }

    public function kelolaTopKursus()
    {
        $data['title'] = 'Pengaturan Top Kursus';
        $data['topCourse'] = Top::with(['course', 'merchant'])->where('status', 'verified')->get();
        return view('dashboard.kelolaTopKursus')->with($data);
    }

    public function kelolaRecommendedKursus()
    {
        $data['title'] = 'Pengaturan Recommended Kursus';
        $data['recommendedCourse'] = Recommended::with(['course', 'merchant'])->where('status', 'verified')->get();
        return view('dashboard.kelolaRecommendedKursus')->with($data);
    }

    public function flashsaleUpdate(Request $request)
    {
        $update = $request->flashtime;
        config(['flashsale.flashdate' => $update]);
        $fp = fopen(base_path() . '/config/flashsale.php', 'w');
        fwrite($fp, '<?php return ' . var_export(config('flashsale'), true) . ';');
        fclose($fp);
        return redirect('flashsaleConfig')->with('success', 'Waktu flash sale telah diupdate');
    }

    public function promoteSlider(Request $request)
    {
        $slider = new slider();
        $imageName = time() . '.' . request()->image->getClientOriginalExtension();
        request()->image->move(public_path('images'), $imageName);
        $slider->course_id = $request->input('courseId');
        $slider->merchant_id = $request->input('merchantId');
        $slider->image = $imageName;
        $slider->status = 'unverified';
        $slider->save();
        return redirect('merchant-course')->with('success', 'Promosi Slider Berhasil Diajukan');
    }

    public function promoteFlash(Request $request)
    {
        $flash = new Flash();
        $flash->course_id = $request->input('courseId');
        $flash->merchant_id = $request->input('merchantId');
        $flash->flashPrice = $request->input('price');
        $flash->status = 'unverified';
        $flash->save();
        return redirect('merchant-course')->with('success', 'Promosi Flashsale Berhasil Diajukan');
    }

    public function promoteTop(Request $request)
    {
        $top = new Top();
        $top->course_id = $request->input('courseId');
        $top->merchant_id = $request->input('merchantId');
        $top->status = 'unverified';
        $top->save();
        return redirect('merchant-course')->with('success', 'Promosi Top Kursus Berhasil Diajukan');
    }

    public function promoteRecommended(Request $request)
    {
        $recommended = new Recommended();
        $recommended->course_id = $request->input('courseId');
        $recommended->merchant_id = $request->input('merchantId');
        $recommended->status = 'unverified';
        $recommended->save();
        return redirect('merchant-course')->with('success', 'Promosi Recommended Kursus Berhasil Diajukan');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $course = Course::find($id);
        $course->title = $request->input('title');
        $course->description = $request->input('description');
        $course->category = $request->input('category');
        $course->facility = $request->input('facility');
        $course->save();
        return redirect('merchant-course')->with('success', 'Kursus Berhasil Di Edit');
    }

    public function courseDate($id)
    {
        $data['courseDates'] = courseDate::where('course_id', $id)->get();
        $data['title'] = 'Pengaturan Tanggal Kursus';
        $data['courseId'] = $id;
        return view('course.date')->with($data);
    }

    public function addCourseDate(Request $request)
    {
        $dates = explode(',', $request->input('date'));
        foreach ($dates as $date) {
            $courseDate = new courseDate();
            $courseDate->course_id = $request->input('courseId');
            $courseDate->start_date = $date;
            $courseDate->save();
        }
        return redirect('courseDate/'.$request->input('courseId'))->with('success', 'Tanggal Berhasil Ditambahkan');
    }

    public function delete($id)
    {
        $course =  Course::destroy($id);
        if ($course) {
            return redirect('merchant-course')->with('success', 'Kursus Berhasil Dihapus');
        } else {
            return redirect('merchant-course')->with('error', 'Penghapusan Kursus Gagal');
        }
    }

    public function deleteSlide($id)
    {
        $course =  slider::destroy($id);
        if ($course) {
            return back()->with('success', 'Slide Berhasil Dihapus');
        } else {
            return back()->with('error', 'Penghapusan Slide Gagal');
        }
    }

    public function deleteFlash($id)
    {
        $course =  Flash::destroy($id);
        if ($course) {
            return back()->with('success', 'Flashsale Berhasil Dihapus');
        } else {
            return back()->with('error', 'Penghapusan Flashsale Gagal');
        }
    }

    public function deleteTop($id)
    {
        $course =  Top::destroy($id);
        if ($course) {
            return back()->with('success', 'Top Kursus Berhasil Dihapus');
        } else {
            return back()->with('error', 'Penghapusan Top Kursus Gagal');
        }
    }

    public function deleteRecommended($id)
    {
        $course =  Recommended::destroy($id);
        if ($course) {
            return back()->with('success', 'Recommended Kursus Berhasil Dihapus');
        } else {
            return back()->with('error', 'Penghapusan Recommended Kursus Gagal');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function uploadTransfer(Request $request)
    {


        $imageName = time() . '.' . request()->image->getClientOriginalExtension();

        request()->image->move(public_path('images'), $imageName);
        $invoice = Invoice::find($request->input('invoiceId'));
        $invoice->bukti = $imageName;
        $invoice->save();
        return redirect('/invoice')->with('success', 'Bukti Transfer Berhasil Di Unggah');
    }

    public function verifyMerchant($id)
    {
        $merchant = Merchant::find($id);
        $merchant->status = 'Verified';
        $merchant->save();
        return redirect('admin-verifikasi')->with('success', 'Lembaga Berhasil Diverifikasi');
    }

    public function verifikasiPendaftaran($id)
    {
        $merchant = Invoice::find($id);
        $merchant->status = 'Verified';
        $merchant->save();
        return redirect('admin-verifikasi')->with('success', 'Pendaftaran Berhasil Diverifikasi');
    }

    public function verifyCourse($id)
    {
        $merchant = Course::find($id);
        $merchant->status = 'terverifikasi';
        $merchant->save();
        return redirect('admin-verifikasi')->with('success', 'Kursus Berhasil Diverifikasi');
    }

    public function verifikasiSlider($id)
    {
        $merchant = slider::find($id);
        $merchant->status = 'verified';
        $merchant->save();
        return redirect('admin-verifikasi')->with('success', 'Iklan Berhasil Diverifikasi');
    }

    public function verifikasiFlash($id)
    {
        $merchant = Flash::find($id);
        $merchant->status = 'verified';
        $merchant->save();
        return redirect('admin-verifikasi')->with('success', 'Iklan Berhasil Diverifikasi');
    }

    public function verifikasiTop($id)
    {
        $merchant = Top::find($id);
        $merchant->status = 'verified';
        $merchant->save();
        return redirect('admin-verifikasi')->with('success', 'Iklan Berhasil Diverifikasi');
    }

    public function verifikasiRecommended($id)
    {
        $merchant = Recommended::find($id);
        $merchant->status = 'verified';
        $merchant->save();
        return redirect('admin-verifikasi')->with('success', 'Iklan Berhasil Diverifikasi');
    }

    public function unverifyMerchant($id)
    {
        $merchant = Merchant::find($id);
        $merchant->delete();
        return redirect('admin-verifikasi')->with('success', 'Lembaga Telah Ditolak dan Dihapus dari daftar lembaga');
    }

    public function merchantCourse()
    {
        $data['title'] = "Kelola Kursus";
        $data['courses'] = Course::where('merchant_id', auth('merchant')->id())->where('status', 'terverifikasi')->get();
        return view('dashboard.merchant_course')->with($data);
    }

    public function showLembaga($id)
    {
        $data['title'] = "Lembaga";
        $data['merchant'] = Merchant::find($id);
        $data['participant'] = Participant::where('merchant_id', $id)->count();
        return view('admin.merchantVerify')->with($data);
    }

    public function editLembaga($id)
    {
        $data['title'] = "Edit Lembaga";
        $data['merchant'] = Merchant::find($id);
        return view('admin.editMerchant')->with($data);
    }

    public function redirectEditMerchant()
    {
        $id = auth()->guard('merchant')->user()->id;
        return redirect('/editLembaga/' . $id);
    }

    public function saveEditLembaga(Request $request)
    {
        $id = $request->input('merchantId');
        if (null != request()->profilePic) {
            $imageName = time() . '.' . request()->profilePic->getClientOriginalExtension();
            request()->profilePic->move(public_path('images'), $imageName);
            $merchant = Merchant::find($id);
            $merchant->profileImage = $imageName;
            $merchant->save();
            return redirect('/editLembaga/' . $id);
        }
        if (null != request()->coverPic) {
            $imageName = time() . '.' . request()->coverPic->getClientOriginalExtension();
            request()->coverPic->move(public_path('images'), $imageName);
            $merchant = Merchant::find($id);
            $merchant->coverImage = $imageName;
            $merchant->save();
            return redirect('/editLembaga/' . $id);
        }
        $merchant = Merchant::find($id);
        $merchant->name = $request->input('name');
        $merchant->categories = $request->input('categories');
        $merchant->email = $request->input('email');
        $merchant->city = $request->input('city');
        $merchant->phone = $request->input('phone');
        $merchant->businessType = $request->input('businessType');
        $merchant->address = $request->input('address');
        $merchant->postCode = $request->input('postCode');
        $merchant->description = $request->input('description');
        $merchant->save();
        return redirect('/editLembaga/' . $id);
    }

    public function showLembagaKursus($id)
    {
        $data['title'] = "Lembaga";
        $data['merchant'] = Merchant::find($id);
        $data['participant'] = Participant::where('merchant_id', $id)->count();
        $data['courses'] = Course::where('merchant_id', $id)->get();
        return view('admin.merchantCourse')->with($data);
    }

    public function invoice()
    {
        $data['title'] = "Invoice";
        $data['invoices'] = Course::with('invoices')->whereHas('invoices', function ($query) {
        })->get();
        return view('dashboard.invoice')->with($data);
    }

    public function history()
    {
        $data['title'] = "Riwayat";
        $data['invoices'] = Course::with('invoices')->whereHas('invoices', function ($query) {
            $query->where('status',  'Verified');
        })->get();
        return view('dashboard.history')->with($data);
    }

    public function submitTestimony(Request $request)
    {

        $invoice = Invoice::find($request->input('invoiceId'));
        $invoice->testimony = $request->input('testimony');
        $invoice->rating = $request->input('rating' . $request->input('invoiceId'));
        $invoice->save();
        return redirect('/history')->with('success', 'Testimoni Telah Dikirim');
    }

    public static function getRating($merchantId)
    {
        $rating = Invoice::where('merchant_id', $merchantId)->avg('rating');
        return $rating;
    }

    public static function getCourseRating($courseId)
    {
        $rating = Invoice::where('course_id', $courseId)->avg('rating');
        return $rating;
    }

    public function showLembagaGaleri($id)
    {
        $data['title'] = "Lembaga";
        $data['merchant'] = Merchant::find($id);
        $data['participant'] = Participant::where('merchant_id', $id)->count();
        return view('admin.merchantGallery')->with($data);
    }

    public static function getUser($id)
    {
        return User::find($id);
    }

    public function lembaga()
    {
        $data['merchants'] = Merchant::all();
        $data['title'] = "Daftar Lembaga";
        return view('merchant')->with($data);
    }

    public function showLembagaReview($id)
    {
        $data['title'] = "Lembaga";
        $data['merchant'] = Merchant::find($id);
        $data['participant'] = Participant::where('merchant_id', $id)->count();
        $data['testimonies'] = $data['invoices'] = Course::with('invoices')->whereHas('invoices', function ($query) use ($id) {
            $query->where('merchant_id',  $id);
            $query->where('testimony', '!=',  'empty');
        })->paginate(10);
        return view('admin.merchantReview')->with($data);
    }

    public function registerCourse(Request $request)
    {
        $data['course'] = Course::with('merchant')->find($request->input('courseId'));
        $data['quantity'] = $request->input('quantity');
        $data['title'] = 'Checkout';
        return view('course.checkout')->with($data);
    }

    public function kelolaSlider()
    {
        $data['slider'] = slider::with(['course', 'merchant'])->where('status', 'verified')->get();
        $data['title'] = "Kelola Slider Promosi";
        //echo json_encode($data['slider']);
        return view('dashboard.admin_slider')->with($data);
    }

    public function admin_omzet()
    {
        $data['title'] = "Analisis Omzet & Peserta";
        $data['merchants'] = Merchant::all();
        return view('dashboard.admin_omzet')->with($data);
    }
    public function count_admin_omzet(Request $request)
    {
        $data['title'] = "Analisis Omzet & Peserta";
        $data['merchants'] = Merchant::all();
        $lembaga = $request->input('lembaga');
        $bulan = $request->input('bulan');
        $tahun = $request->input('tahun');
        $periode = $request->input('periode');
        $omzet = Invoice::where('merchant_id', $lembaga)->where('created_at', 'like', '%' . $bulan . '%')->where('created_at', 'like', '%' . $tahun . '%')->where('periode', 'like', '%' . $periode . '%')->sum('total');
        $participant = Participant::where('merchant_id', $lembaga)->where('created_at', 'like', '%' . $bulan . '%')->where('created_at', 'like', '%' . $tahun . '%')->where('periode', 'like', '%' . $periode . '%')->count();
        $data['omzet'] = $omzet;
        $data['participant'] = $participant;
        return view('dashboard.admin_omzet')->with($data);
    }

    public function omzet()
    {
        $data['title'] = "Omzet";
        return view('dashboard.merchant_omzet')->with($data);
    }

    public function count_omzet(Request $request)
    {
        $data['title'] = "Omzet";
        $lembaga = auth()->guard('merchant')->user()->id;
        $bulan = $request->input('bulan');
        $tahun = $request->input('tahun');
        $periode = $request->input('periode');
        $omzet = Invoice::where('merchant_id', $lembaga)->where('created_at', 'like', '%' . $bulan . '%')->where('created_at', 'like', '%' . $tahun . '%')->where('periode', 'like', '%' . $periode . '%')->sum('total');
        $participant = Participant::where('merchant_id', $lembaga)->where('created_at', 'like', '%' . $bulan . '%')->where('created_at', 'like', '%' . $tahun . '%')->where('periode', 'like', '%' . $periode . '%')->count();
        $data['omzet'] = $omzet;
        $data['participant'] = $participant;

        return view('dashboard.merchant_omzet')->with($data);
    }
}
