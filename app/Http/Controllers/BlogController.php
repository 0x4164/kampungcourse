<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;
use Illuminate\Foundation\Auth\AuthenticatesUsers;


class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use AuthenticatesUsers;
    public function index()
    {
        $data['title'] = "Blog";
        $data['posts'] = Blog::orderBy('created_at','asc')->paginate(5);
        return view('dashboard.admin_blog')->with($data);
    }

    public function blog()
    {
        $data['title'] = "Blog";
        $data['posts'] = Blog::with('admin')->orderBy('created_at','asc')->paginate(20);
        return view('blog.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['title'] = "Create New Blog Post";
        return view('dashboard.admin_create_post')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        $imageName = time().'.'.request()->image->getClientOriginalExtension();
        request()->image->move(public_path('images'), $imageName);
        $post = new Blog;
        $post->title = $request->input('title');
        $post->body = $request->input('body');
        $post->thumbnail = $imageName;
        $post->tags = $request->input('tags');
        $post->admin_id = auth()->guard('admin')->user()->id;
        $post->save();
        
        return redirect('admin-blog')->with('success', 'Post Baru Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['title'] = "Blog";
        $data['post'] = Blog::find($id);
        return view('dashboard.admin_show_post')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['title'] = "Edit Blog Post";
        $data['post'] = Blog::find($id);
        return view('dashboard.admin_edit_post')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required'
        ]);
        $post = Blog::find($id);
        if(isset(request()->image)){
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('images'), $imageName);
            $post->thumbnail = $imageName;
        }
        $post->title = $request->input('title');
        $post->body = $request->input('body');
        $post->tags = $request->input('tags');
        $post->save();

        return redirect('admin-blog')->with('success', 'Post Berhasil Di Edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Blog::find($id);
        $post->delete();
        return redirect('admin-blog')->with('success', 'Post Berhasil Di Hapus');
    }
}
