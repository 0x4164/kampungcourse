<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class slider extends Model
{
    public function course()
    {
        return $this->belongsTo('App\Course');
    }

    public function merchant()
    {
        return $this->belongsTo('App\Merchant');
    }
}
