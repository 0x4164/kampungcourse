<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class chat extends Model
{
    protected $table = 'chats';
    public $primaryKey = 'id';
    public $timestamps = true;
}
