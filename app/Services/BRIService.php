<?php

namespace App\Services;

class BRIService{
    public static function d1(){
        echo "d1";
    }

    public function authBRI(){
        //auth
        $url ="https://sandbox.partner.api.bri.co.id/oauth/client_credential/accesstoken?grant_type=client_credentials";
        $data = "client_id=".env('BRI_ID')."&client_secret=".env('BRI_SECRET');
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");  //for updating we have to use PUT method.
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch,CURLOPT_POSTFIELDS,$data);
        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $json = json_decode($result, true);
        $accesstoken = $json['access_token'];

        // $datas = json_encode($result, true);

        // echo "<br/> <br/>";
        // echo "resssss : ".$result;
        // echo "<br/> <br/>";

        // echo "accesstoken : ".$accesstoken;
        // echo "<br/> <br/>";
        return $accesstoken;
    }

    public function generateSignature($path,$verb,$token,$timestamp,$body,$secret){
        // change SECRET to your Consumer Secret value
        $secret = env('BRI_SECRET');
        // $path="";
        // $verb="";
        // $token=$this->authBRI();
        // $timestamp = gmdate("Y-m-d\TH:i:s.000\Z");
        // $body="";
        $payload = "path=$path&verb=$verb&token=Bearer $token&timestamp=$timestamp&body=$body";

        $signPayload = hash_hmac('sha256', $payload, $secret, true);
        $base64sign = base64_encode($signPayload);
        echo $base64sign." :::: ";
        return $base64sign;
    }

    public function accountInfo(){
        // https://partner.api.bri.co.id/sandbox/v2/inquiry/
        $NoRek = "888801000157508";
        $secret = env('BRI_SECRET');
        $timestamp = gmdate("Y-m-d\TH:i:s.000\Z");
        $token = $this->authBRI();
        $path = "/sandbox/v2/inquiry/".$NoRek;
        $verb = "GET";
        $body="";

        $base64sign = $this->generateSignature($path,$verb,$token,$timestamp,$body,$secret);

        $urlGet ="https://partner.api.bri.co.id/sandbox/v2/inquiry/".$NoRek;
        $chGet = curl_init();
        curl_setopt($chGet,CURLOPT_URL,$urlGet);

        $request_headers = array(
                            "Authorization:Bearer " . $token,
                            "BRI-Timestamp:" . $timestamp,
                            "BRI-Signature:" . $base64sign
                        );
        curl_setopt($chGet, CURLOPT_HTTPHEADER, $request_headers);
        curl_setopt($chGet, CURLINFO_HEADER_OUT, true);


        // curl_setopt($chGet, CURLOPT_CUSTOMREQUEST, "GET");  //for updating we have to use PUT method.
        curl_setopt($chGet, CURLOPT_RETURNTRANSFER, true);

        $resultGet = curl_exec($chGet);
        $httpCodeGet = curl_getinfo($chGet, CURLINFO_HTTP_CODE);
        // $info = curl_getinfo($chGet);
        // print_r($info);
        curl_close($chGet);

        $jsonGet = json_decode($resultGet, true);


        echo "<br/> <br/>";
        echo "Response Get : ".$resultGet;
        echo "<br/> <br/>";
    }

    public function trfValidate(){
        $NoRek = "888801000157508";
        $secret = env('BRI_SECRET');
        $timestamp = gmdate("Y-m-d\TH:i:s.000\Z");
        $token = $this->authBRI();
        $path = "/sandbox/v2/transfer/internal";
        $verb = "GET";
        $body="";

        $base64sign = $this->generateSignature($path,$verb,$token,$timestamp,$body,$secret);
        //https://partner.api.bri.co.id/sandbox/v2
        // $urlGet ="https://partner.api.bri.co.id/sandbox/v2/transfer/internal/accounts?sourceAccount=".$NoRek."&beneficiaryAccount=".$NoRek;
        // ."&beneficiaryaccount=".$NoRek
        $urlGet ="https://partner.api.bri.co.id/sandbox/v2/transfer/internal/accounts?sourceaccount=".$NoRek;
        // $urlGet ="https://partner.api.bri.co.id/sandbox/v2/transfer/internal/accounts?sourceAccount=".$NoRek;
        $chGet = curl_init();
        curl_setopt($chGet,CURLOPT_URL,$urlGet);

        $request_headers = array(
            "Authorization:Bearer " . $token,
            "BRI-Timestamp:" . $timestamp,
            "BRI-Signature:" . $base64sign
        );
        curl_setopt($chGet, CURLOPT_HTTPHEADER, $request_headers);
        curl_setopt($chGet, CURLINFO_HEADER_OUT, true);


        // curl_setopt($chGet, CURLOPT_CUSTOMREQUEST, "GET");  //for updating we have to use PUT method.
        curl_setopt($chGet, CURLOPT_RETURNTRANSFER, true);
        var_dump($chGet);

        $resultGet = curl_exec($chGet);
        $httpCodeGet = curl_getinfo($chGet, CURLINFO_HTTP_CODE);
        // $info = curl_getinfo($chGet);
        // print_r($info);
        curl_close($chGet);

        $jsonGet = json_decode($resultGet, true);

        echo "<br/> <br/>";
        echo "Response Get : ".$resultGet;
        echo "<br/> <br/>";
    }

    public function transfer(){
        $noReff = "12313221";
        $sourceAcc = "888801000157508";
        $benefAcc = "888801000003301";
        $amt="1000.00";
        $feeType="OUR";
        $trxDateTime="12-02-2019 15:08:00";
        $remark="REMARK TEST";

        $datas = array('NoReferral' => $noReff ,
        'sourceAccount' => $sourceAcc,
        'beneficiaryAccount' => $benefAcc,
        'Amount' => $amt,
        'FeeType' => $feeType,
        'transactionDateTime' => $trxDateTime,
        'remark' => $remark );

        $payload = json_encode($datas, true);

        $path = "/sandbox/v2/transfer/internal";
        $verb = "POST";
        $secret = env('BRI_SECRET');
        $timestamp = gmdate("Y-m-d\TH:i:s.000\Z");
        $token = $this->authBRI();
        $base64sign = $this->generateSignature($path,$verb,$token,$timestamp,$payload,$secret);

        $request_headers = array(
                            "Content-Type:"."application/json",
                            "Authorization:Bearer " . $token,
                            "BRI-Timestamp:" . $timestamp,
                            "BRI-Signature:" . $base64sign,
                        );

        $urlPost ="https://partner.api.bri.co.id/sandbox/v2/transfer/internal";
        $chPost = curl_init();
        curl_setopt($chPost, CURLOPT_URL,$urlPost);
        curl_setopt($chPost, CURLOPT_HTTPHEADER, $request_headers);
        curl_setopt($chPost, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($chPost, CURLINFO_HEADER_OUT, true);
        curl_setopt($chPost, CURLOPT_RETURNTRANSFER, true);
        $resultPost = curl_exec($chPost);
        $httpCodePost = curl_getinfo($chPost, CURLINFO_HTTP_CODE);
        curl_close($chPost);

        $jsonPost = json_decode($resultPost, true);

        echo "<br/> <br/>";
        echo "Response Post : ".$resultPost;
    }

    public function rtgsTrf(){
        $client_id = '...';
      $secret_id = '...';
      $timestamp = gmdate("Y-m-d\TH:i:s.000\Z");
      $secret = $secret_id;
      //generate token
        $token = generateToken($client_id,$secret_id);

        $FeeType = "SHA|1500";
        $NoReferral = "45641231237";
        $receiverSwiftCode = "CENAIDJA";
        $senderAccount = "020601000064301";
        $senderName = "KAHFI";
        $senderIdentity = "3175012411940006";
        $senderType = "1";
        $senderResidencyStatus = "1";
        $senderAddress = "JALAN";
        $senderPhone = "0214758684";
        $receiverAccount = "020601000006565";
        $receiverName = "RINE";
        $receiverIdentity = "1234567890";
        $receiverType = "1";
        $receiverResidencyStatus = "1";
        $receiverAddress = "JALAN1";
        $receiverPhone = "021";
        $amount = "25000000";
        $remark = "KLIRING 1 HARI RABU";
        $corporateType = "1";

        $datas = array('FeeType' => $FeeType,
            'NoReferral' => $NoReferral,
            'receiverSwiftCode'=> $receiverSwiftCode,
            'senderAccount' => $senderAccount,
            'senderName' => $senderName,
            'senderIdentity' => $senderIdentity,
            'senderType' => $senderType,
            'senderResidencyStatus' => $senderResidencyStatus,
            'senderAddress' => $senderAddress,
            'senderPhone' => $senderPhone,
            'receiverAccount' => $receiverAccount,
            'receiverName' => $receiverName,
            'receiverIdentity' => $receiverIdentity,
            'receiverType' => $receiverType,
            'receiverResidencyStatus' => $receiverResidencyStatus,
            'receiverAddress' => $receiverAddress,
            'receiverPhone' => $receiverPhone,
            'amount' => $amount,
            'remark' => $remark,
            'corporateType' => $corporateType);


            $payload = json_encode($datas, true);

            $path = "sandbox/v1/transfer/rtgs";
      $verb = "POST";
      $base64sign = generateSignature($path,$verb,$token,$timestamp,$payload,$secret);



      $request_headers = array(
                                "Content-Type:"."application/json",
                                "Authorization:Bearer " . $token,
                                "BRI-Timestamp:" . $timestamp,
                                "BRI-Signature:" . $base64sign,
                            );



            $urlPost ="https://partner.api.bri.co.id/sandbox/v1/transfer/rtgs";
            $chPost = curl_init();
            curl_setopt($chPost, CURLOPT_URL,$urlPost);
            curl_setopt($chPost, CURLOPT_HTTPHEADER, $request_headers);
            curl_setopt($chPost, CURLOPT_CUSTOMREQUEST, "POST"); 
            curl_setopt($chPost, CURLOPT_POSTFIELDS, $payload);
            curl_setopt($chPost, CURLINFO_HEADER_OUT, true);
            curl_setopt($chPost, CURLOPT_RETURNTRANSFER, true);
            $resultPost = curl_exec($chPost);
            $httpCodePost = curl_getinfo($chPost, CURLINFO_HTTP_CODE);
            curl_close($chPost);


            $jsonPost = json_decode($resultPost, true);


            echo "<br/> <br/>";
            echo "Response Post : ".$resultPost;
    }
}
